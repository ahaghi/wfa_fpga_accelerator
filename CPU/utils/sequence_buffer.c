/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 * DESCRIPTION: Simple linear vector for generic type elements
 */

#include "utils/sequence_buffer.h"

/*
 * Align memory
 */
void* sequence_buffer_align_mem(
    void* const memory,
    const uint64_t align_bytes) {
  if (align_bytes == 0) return memory;
  // Compute aligned memory
  void* memory_aligned = memory + (align_bytes-1);
  memory_aligned = memory_aligned - ((uintptr_t)memory_aligned % align_bytes);
  return memory_aligned;
}

/*
 * Setup
 */
sequence_buffer_t* sequence_buffer_new(
    const uint64_t num_sequences_hint,
    const uint64_t sequence_length_hint,
    const uint64_t buffer_align_bytes) {
  // Alloc
  sequence_buffer_t* const sequence_buffer = malloc(sizeof(sequence_buffer_t));
  // ID
  sequence_buffer->sequence_id = 1;
  // Initialize sequences
  sequence_buffer->offsets = malloc(num_sequences_hint*sizeof(sequence_offset_t));
  sequence_buffer->offsets_used = 0;
  sequence_buffer->offsets_allocated = num_sequences_hint;
  // Initialize buffer
  const uint64_t buffer_size = num_sequences_hint*sequence_length_hint;
  sequence_buffer->buffer_mem = malloc(buffer_size+buffer_align_bytes);
  sequence_buffer->buffer = sequence_buffer_align_mem(sequence_buffer->buffer_mem,buffer_align_bytes);
  sequence_buffer->buffer_used = 0;
  sequence_buffer->buffer_allocated = buffer_size;
  sequence_buffer->buffer_align_bytes = buffer_align_bytes;
  // Return
  return sequence_buffer;
}
void sequence_buffer_clear(
    sequence_buffer_t* const sequence_buffer) {
  sequence_buffer->sequence_id = 1;
  sequence_buffer->offsets_used = 0;
  sequence_buffer->buffer_used = 0;
}
void sequence_buffer_delete(
    sequence_buffer_t* const sequence_buffer) {
  free(sequence_buffer->buffer_mem);
  free(sequence_buffer->offsets);
  free(sequence_buffer);
}
/*
 * Accessors
 */
void sequence_buffer_add_offsets(
    sequence_buffer_t* const sequence_buffer,
    const uint64_t pattern_offset,
    const uint64_t pattern_length,
    const uint64_t text_offset,
    const uint64_t text_length) {
  // Check allocated memory
  if (sequence_buffer->offsets_used == sequence_buffer->offsets_allocated) {
    const uint64_t num_offsets = (float)(sequence_buffer->offsets_used+1) * (3.0/2.0);
    sequence_buffer->offsets = realloc(sequence_buffer->offsets,num_offsets*sizeof(sequence_offset_t));
    sequence_buffer->offsets_allocated = num_offsets;
  }
  // Add sequence pair
  sequence_offset_t* const current_offsets = sequence_buffer->offsets + sequence_buffer->offsets_used;
  current_offsets->pattern_offset = pattern_offset;
  current_offsets->pattern_length = pattern_length;
  current_offsets->text_offset = text_offset;
  current_offsets->text_length = text_length;
  sequence_buffer->offsets_used += 1;
}
void sequence_buffer_add_pair(
    sequence_buffer_t* const sequence_buffer,
    char* const pattern,
    const uint64_t pattern_length,
    char* const text,
    const uint64_t text_length,
    const uint64_t buffer_idx_bytes,
    const uint64_t buffer_length_bytes,
    const uint64_t buffer_pattern_bytes,
    const uint64_t buffer_text_bytes,
    const uint64_t buffer_padding_bytes) {
  // Allocate memory
  const uint64_t bytes_required =
      buffer_idx_bytes +
      2*buffer_length_bytes +
      buffer_pattern_bytes +
      buffer_text_bytes +
      buffer_padding_bytes;
  if (sequence_buffer->buffer_used+bytes_required > sequence_buffer->buffer_allocated) {
    const uint64_t buffer_size = (3*(sequence_buffer->buffer_used+bytes_required))/2;
    sequence_buffer->buffer_allocated = buffer_size;
    sequence_buffer->buffer_mem =
        realloc(sequence_buffer->buffer_mem,buffer_size+sequence_buffer->buffer_align_bytes);
    sequence_buffer->buffer = sequence_buffer_align_mem(
        sequence_buffer->buffer_mem,sequence_buffer->buffer_align_bytes);
  }
  // Copy ID
  void* mem_ptr = sequence_buffer->buffer + sequence_buffer->buffer_used;
  *((uint64_t*)mem_ptr) = (sequence_buffer->sequence_id)++;
  mem_ptr += buffer_idx_bytes;
  // Copy lengths
  *((uint32_t*)mem_ptr) = pattern_length;
  mem_ptr += buffer_length_bytes;
  *((uint32_t*)mem_ptr) = text_length;
  mem_ptr += buffer_length_bytes;
  // Copy sequences
  memcpy(mem_ptr,pattern,pattern_length);
  mem_ptr += buffer_pattern_bytes;
  memcpy(mem_ptr,text,text_length);
  mem_ptr += buffer_text_bytes;
  // Add sequence pair
  const uint64_t pattern_offset = sequence_buffer->buffer_used + buffer_idx_bytes + 2*buffer_length_bytes;
  const uint64_t text_offset = pattern_offset + buffer_pattern_bytes;
  sequence_buffer_add_offsets(
      sequence_buffer,pattern_offset,pattern_length,text_offset,text_length);
  // Update used
  sequence_buffer->buffer_used += bytes_required;
}

