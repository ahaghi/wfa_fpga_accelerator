/*
 *                             The MIT License
 *
 * Wavefront Alignments Algorithms
 * Copyright (c) 2017 by Santiago Marco-Sola  <santiagomsola@gmail.com>
 *
 * This file is part of Wavefront Alignments Algorithms.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * PROJECT: Wavefront Alignments Algorithms
 * AUTHOR(S): Santiago Marco-Sola <santiagomsola@gmail.com>
 * DESCRIPTION: Wavefront Alignments Algorithms benchmarking tool
 */

#include "utils/commons.h"
#include "utils/sequence_buffer.h"
#include "system/profiler_timer.h"

#include "edit/edit_table.h"
#include "edit/edit_dp.h"
#include "gap_lineal/nw.h"
#include "gap_affine/affine_wavefront.h"
#include "gap_affine/affine_wavefront_align.h"
#include "gap_affine/swg.h"
#include "fpga/Action_Call.h"


typedef struct {
  char operation;
  int inc_v;
  int inc_h;
} cigar_op_t;


void Recover_Cigar(
    char* pattern,
    char* text,
    uint32_t pattern_len,
    uint32_t text_len,
    uint64_t cigar_compacted,
    char* cigar){//,
    //uint64_t* cigar_len)
  // LUTs
  int cigar_len = 0;
  cigar_op_t cigar_op_lut[4] = {
      { .operation = 'X', .inc_v = 1, .inc_h = 1 },
      { .operation = 'I', .inc_v = 0, .inc_h = 1 },
      { .operation = 'D', .inc_v = 1, .inc_h = 0 },
      { .operation = 'E', .inc_v = 0, .inc_h = 0 }
  };
  //char matches_lut[8] = "MMMMMMMM";
  const uint64_t recover_matches_int[9] = {0, *((uint64_t*)"M"), *((uint64_t*)"MM"),*((uint64_t*)"MMM"),*((uint64_t*)"MMMM"),*((uint64_t*)"MMMMM"),*((uint64_t*)"MMMMMM"),*((uint64_t*)"MMMMMMM"),*((uint64_t*)"MMMMMMMM")};
  int last_opt = 0;
  // Parameters
  int v = 0, h = 0;
  //*cigar_len = (uint64_t) cigar;
  while (v < pattern_len && h < text_len) {
    // Fetch pattern/text blocks
    uint64_t* pattern_blocks = (uint64_t*)(pattern+v);
    uint64_t* text_blocks = (uint64_t*)(text+h);
    uint64_t pattern_block = *pattern_blocks;
    uint64_t text_block = *text_blocks;
    // Compare 64-bits blocks
    uint64_t cmp = pattern_block ^ text_block;
    while (cmp==0 && (v+8) < pattern_len && (h+8) < text_len) {
      // Increment offset
      v += 8;
      h += 8;
      // Dump matches in block
      *((uint64_t*)cigar) = recover_matches_int[8];
      cigar += 8;
      cigar_len += 8;
      // Next blocks
      ++pattern_blocks;
      ++text_blocks;
      // Fetch & Compare
      pattern_block = *pattern_blocks;
      text_block = *text_blocks;
      cmp = pattern_block ^ text_block;
    }
    // Count equal characters
    
   uint64_t num_matches_last = __builtin_ctzl(cmp)/8;
       
   if(v+num_matches_last < pattern_len && h+num_matches_last < text_len) {       
      ;
   }
   else{
      num_matches_last = MIN(pattern_len-v, text_len-h);
   }
     
   v += num_matches_last;
   h += num_matches_last;
   *((uint64_t*)cigar) = recover_matches_int[num_matches_last];
   cigar += num_matches_last;   
   cigar_len += num_matches_last;
    
    //int k;
    //for (k = 0; k < num_matches_last; k++){
    //   *(cigar++) = 'M';
    //}
    
    // Check boundaries
    if (v >= pattern_len || h >= text_len) break;
    // Fetch CIGAR operation
    do {
      const int cigar_op = (int) (cigar_compacted & 3);
       
      if (cigar_op == 1){
         last_opt = 1;
      }
      else if (cigar_op == 2){
         last_opt = 2;
      } 
      cigar_compacted = cigar_compacted >> 2;
      // Add operation using LUT
      if (cigar_op == 3){
         cigar_op_t* const op = cigar_op_lut + last_opt;
         *(cigar++) = op->operation;
         cigar_len++;
         v += op->inc_v;
         h += op->inc_h;
      }
      else{
         cigar_op_t* const op = cigar_op_lut + cigar_op;
         *(cigar++) = op->operation;
         cigar_len++;
         v += op->inc_v;
         h += op->inc_h;
      }

    } while ((int) (cigar_compacted & 3) == 3);
     
     
  }
   
  while (v < pattern_len) {
    *(cigar++) = 'D';
     cigar_len++;
    v++;
  }
  while (h < text_len) {
    *(cigar++) = 'I';
    cigar_len++;
    h++;
  }
  //*cigar_len = (uint64_t) cigar - *cigar_len; 
  *cigar = '\0';
   //printf("cigar len = %d \n", cigar_len);
}


void edit_cigar_print_long(
    edit_cigar_t* const edit_cigar,
    char* BT, int* BT_len) {
  *(BT+0) = edit_cigar->operations[edit_cigar->begin_offset];
  int i;
  *(BT_len) = edit_cigar->end_offset - edit_cigar->begin_offset;
   
  for (i=edit_cigar->begin_offset+1;i<edit_cigar->end_offset;++i) {
      //fprintf(stream,"%c",last_op);
      *(BT+i-edit_cigar->begin_offset) = edit_cigar->operations[i];
  }
   *(BT+i-edit_cigar->begin_offset) = '\0';
  //fprintf(stream,"%c\n",last_op);
}

void Original_Aligner(char* pattern, char* text, int pattern_len, int text_len, int64_t* score, char* BT_original, int* BT_original_len){
  mm_allocator_t* const mm_allocator = mm_allocator_new(BUFFER_SIZE_8M);
  // Set penalties
  affine_penalties_t affine_penalties = {
      .match = 0,
      .mismatch = 4,
      .gap_opening = 6,
      .gap_extension = 2,
  };
  // Init Affine-WFA
  affine_wavefronts_t* affine_wavefronts = affine_wavefronts_new_complete(
      pattern_len,text_len,&affine_penalties,NULL,mm_allocator);
  // Align
  affine_wavefronts_align(
      affine_wavefronts,pattern,pattern_len,text,text_len);
  // Display alignment
  *score = abs(edit_cigar_score_gap_affine(
      &affine_wavefronts->edit_cigar,&affine_penalties));

  edit_cigar_print_long(&affine_wavefronts->edit_cigar, BT_original, BT_original_len);
    
  // Free
  affine_wavefronts_delete(affine_wavefronts);
  mm_allocator_delete(mm_allocator);
}

int Aligner_comp(int score1, int score2, char* CIGAR1, char* CIGAR2){
   
   int _rc = 0;
   
   if(score1 == score2){
      _rc = strcmp (CIGAR1, CIGAR2);
   }
   else{
      _rc = 1;  
   }
      
 return(_rc);     
 }

void Snap_Size_Addaptor(uint64_t actual_size, int burst, int DW_byte, uint64_t *adapt_size){

   int reminder  = 0;
   int producet  = 0;
   reminder = actual_size % (burst*DW_byte);
   producet = actual_size / (burst*DW_byte);
   if(reminder == 0){
      *adapt_size = actual_size;
   }
   else{
      *adapt_size = (producet+1) * (burst*DW_byte);
   }
   
}

struct Align_Result_t{
  int k_max;
  int score;
  char Back_Trace[150+32];
};

/*
 * Algorithms
 */
typedef enum {
  alignment_gap_affine_swg,
  alignment_gap_affine_wavefront,
  alignment_gap_affine_wavefront_fpga
} alg_algorithm_type;

/*
 * Generic parameters
 */
typedef struct {
  // Input
  char *algorithm;
  char *input;
  int batch_size;
  // Penalties
  affine_penalties_t affine_penalties;
  // Profile
  profiler_timer_t timer_global;
  profiler_timer_t timer_parsing_input;
  profiler_timer_t timer_send_data;
  profiler_timer_t timer_align;
  profiler_timer_t timer_receive_results;
  // Misc
  int progress;
  int num_threads;
  int seq_len;
  bool check_correct;
  bool check_score;
  bool check_alignments;
  bool verbose;
} benchmark_args;
benchmark_args parameters = {
  // Input
  .algorithm = NULL,
  .input = NULL,
  .batch_size = INT32_MAX,
  // Penalties
  .affine_penalties = {
      .match = 0,
      .mismatch = 4,
      .gap_opening = 6,
      .gap_extension = 2,
  },
  // Misc
  .progress = 100000,
  .num_threads = 1,
  .seq_len = 150,
  .check_correct = false,
  .check_score = false,
  .check_alignments = false,
  .verbose = false
};
/*
 * Benchmark
 */
void benchmark_swg_parallel(
    sequence_buffer_t* const sequence_buffer,
    const int max_pattern_length,
    const int max_text_length) {
  // Parameters
  const int num_sequences = sequence_buffer->offsets_used;
  // Parallel section
  #pragma omp parallel num_threads(parameters.num_threads)
  {
    // Allocate
    mm_allocator_t* const mm_allocator = mm_allocator_new(BUFFER_SIZE_8M);
    affine_table_t affine_table;
    affine_table_allocate(
        &affine_table,max_pattern_length,
        max_text_length,mm_allocator);
    // Bach filter
    int i;
    #pragma omp master
    timer_start(&parameters.timer_align);
    #pragma omp for
    for (i=0;i<num_sequences;++i) {
      // Fetch sequence
      sequence_offset_t* const offset = sequence_buffer->offsets + i;
      char* const pattern = sequence_buffer->buffer + offset->pattern_offset;
      const int pattern_length = offset->pattern_length;
      char* const text = sequence_buffer->buffer + offset->text_offset;
      const int text_length = offset->text_length;
      // Align
      swg_compute(
          &affine_table,&(parameters.affine_penalties),
          pattern,pattern_length,text,text_length);
    }
    #pragma omp master
    timer_stop(&parameters.timer_align);
    #pragma omp critical
    affine_table_free(&affine_table,mm_allocator); // Free
    mm_allocator_delete(mm_allocator); // Free
  }
}
void benchmark_gap_affine_wavefront_parallel(
    sequence_buffer_t* const sequence_buffer,
    const int max_pattern_length,
    const int max_text_length) {
  // Parameters
  const int num_sequences = sequence_buffer->offsets_used;
  // Parallel section
  #pragma omp parallel num_threads(parameters.num_threads)
  {
    // Allocate
    mm_allocator_t* const mm_allocator = mm_allocator_new(BUFFER_SIZE_8M);
    affine_wavefronts_t* const affine_wavefronts =
        affine_wavefronts_new_complete(
            max_pattern_length,max_text_length,
            &parameters.affine_penalties,NULL,mm_allocator);
    // Bach filter
    int i;
    #pragma omp master
    timer_start(&parameters.timer_align);
    #pragma omp for
    for (i=0;i<num_sequences;++i) {
      // Fetch sequence
      sequence_offset_t* const offset = sequence_buffer->offsets + i;
      char* const pattern = sequence_buffer->buffer + offset->pattern_offset;
      const int pattern_length = offset->pattern_length;
      char* const text = sequence_buffer->buffer + offset->text_offset;
      const int text_length = offset->text_length;
      // Clear wavefront
      affine_wavefronts_clear(affine_wavefronts);
      // Align
      affine_wavefronts_align(affine_wavefronts,
          pattern,pattern_length,text,text_length);
       
    }
    #pragma omp master
    timer_stop(&parameters.timer_align);
    #pragma omp critical
    affine_wavefronts_delete(affine_wavefronts); // Free
    mm_allocator_delete(mm_allocator); // Free
  }
}

void benchmark_wavefront_fpga(
    sequence_buffer_t* const sequence_buffer,
    const int max_pattern_length,
    const int max_text_length) {
    
    
  // Parameters
  const uint64_t num_sequences = sequence_buffer->offsets_used;

  uint64_t  Actual_Src_Data_Size;
   
  if (parameters.seq_len == 150)
      Actual_Src_Data_Size = 320 * num_sequences;
  else if (parameters.seq_len == 300)
      Actual_Src_Data_Size = 640 * num_sequences;
  else //if (parameters.seq_len == 100) 
      Actual_Src_Data_Size = 256 * num_sequences;
   
  uint64_t  Adapt_Src_Data_Size;
  uint64_t  Actual_Dst_Data_Size = 16  * num_sequences; 
  uint64_t  Adapt_Dst_Data_Size;
   
  Snap_Size_Addaptor(Actual_Src_Data_Size, 64, 64, &Adapt_Src_Data_Size);   
  Snap_Size_Addaptor(Actual_Dst_Data_Size, 64, 64, &Adapt_Dst_Data_Size);

  // Comparing with CPU
  const int Max_seq_num = 10000000;
  int* tmp_arr   = (int*)malloc(Max_seq_num * sizeof(int)); 
  uint64_t in = 0;
  if (parameters.check_alignments){
      parameters.num_threads = 1;
      for(in = 1; in <= Max_seq_num; in++){
         *(tmp_arr+in) = 0;
      }
  }
   
  /*
   * Send data to FPGA (+ allocate memory in the device)
   */
  timer_start(&parameters.timer_send_data);

  void* Src_Addr = (char*) (sequence_buffer->buffer);
  void* Dst_Addr = (char*) snap_malloc(Adapt_Dst_Data_Size);
  uint32_t action_type = 0;
  uint64_t dst_data_size_return_local;

  timer_stop(&parameters.timer_send_data);
  /*
   * Align
   */
  timer_start(&parameters.timer_align);
    if (!snap_function(action_type, Adapt_Src_Data_Size, Adapt_Dst_Data_Size, &dst_data_size_return_local, Src_Addr, Dst_Addr, 64, 64)){
       printf("FPGA Failed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }
  printf("dst_data_size_return_local = %ld\n", dst_data_size_return_local*8);
   
  timer_stop(&parameters.timer_align);

  #pragma omp parallel num_threads(parameters.num_threads)
  {
  #pragma omp master
  timer_start(&parameters.timer_receive_results);
     
  int64_t tmp_ID, tmp_dist, score, tmp_ID_max = 0;
  uint8_t k_max;
  uint64_t itr;
  uint64_t BT = 0; 
  char ptr[320]  = {};
  char txt[320]  = {};
  int BT_original_len = 0;
  int rc = 0;
  int fail_num = 0;

  
  #pragma omp for 
  for (itr = 0; itr < dst_data_size_return_local*8*16; itr = itr+16){

     tmp_ID = 0;
     score = 0;
     tmp_dist = 0;
     k_max = 0;
     
     rc = 0;
     BT = 0;
     memcpy(&tmp_ID   , Dst_Addr+itr   , 4);
     memcpy(&tmp_dist , Dst_Addr+itr+4 , 3);
     memcpy(&k_max    , Dst_Addr+itr+7 , 1);
     memcpy(&BT       , Dst_Addr+itr+8 , 8);

     
     if (tmp_ID == 0 || tmp_ID > num_sequences) 
        continue;
     
     sequence_offset_t* const offset = sequence_buffer->offsets + (tmp_ID - 1);
     char* const    ptrn     = sequence_buffer->buffer + offset->pattern_offset;
     const uint32_t ptrn_len = offset->pattern_length;
     char* const    text     = sequence_buffer->buffer + offset->text_offset;
     const uint32_t text_len = offset->text_length;
     char BTF[400]  = {};
     
     if (tmp_dist == 0 && k_max > 0 && !parameters.check_alignments) //FPGA Fail to Calculate because K-max > FPGA_Design_K-max
         Original_Aligner(ptrn, text, ptrn_len, text_len, &tmp_dist, BTF, &BT_original_len);
     else
         Recover_Cigar(ptrn, text, ptrn_len, text_len, BT, BTF);
     
     if (parameters.check_alignments){
        *(tmp_arr+(tmp_ID)) = 1;
        tmp_ID_max = MAX(tmp_ID,tmp_ID_max);
        char BT_original[400]  = {};
        Original_Aligner(ptrn, text, ptrn_len, text_len, &score, BT_original, &BT_original_len);
        rc = Aligner_comp(tmp_dist, score, BTF, BT_original);    
     
        if (rc == 0)
           ;//continue;
        else{
           fail_num++;
           //memcpy(ptr, ptrn, ptrn_len);
           //memcpy(txt, text, text_len);
           //printf("== ID = %ld ======================================================================================================================================================\n", tmp_ID);
           //printf("   Score_FPGA = %ld ,   Score_CPU = %d,   k_max = %d\n", tmp_dist, score, k_max);
           //printf("   PTRN       = %s \n", ptr);
           //printf("   TEXT       = %s \n", txt);
           //printf("   CIGAR_FPGA = %s \n", BTF);
           //printf("   CIGAR_CPU  = %s \n", BT_original);
           //printf("   SHORT BT   = %ld \n", BT);
           //printf("   PTRN LEN   = %d \n", ptrn_len);
           //printf("   TEXT LEN   = %d \n", text_len);
           //printf("=================================================================================================================================================================\n\n"); 
           //break;
        }
     }

  }
  
  if (parameters.check_alignments){
      printf("Maximum Returned ID = %ld\n", tmp_ID_max);
      int lost_num = 0;
      for(in = 1; in <= num_sequences; in++){
         if( *(tmp_arr+in) == 0){
            lost_num++;
         }
      }   
      printf("%d IDs are LOST\n", lost_num);
         
      if (fail_num == 0)
        printf("Accurately DONE\n");
      else
        printf("%d of FPGA results doesn't match with CPU results\n", fail_num);
  }
     

  #pragma omp master
  timer_stop(&parameters.timer_receive_results);
  }
}

/*
 * Benchmark switch
 */
void align_benchmark(const alg_algorithm_type alg_algorithm) {
  // Parameters
  FILE *input_file = NULL;
  char *line1 = NULL, *line2 = NULL;
  int line1_length=0, line2_length=0;
  size_t line1_allocated=0, line2_allocated=0;
  // Init
  timer_reset(&(parameters.timer_parsing_input));
  timer_reset(&(parameters.timer_send_data));
  timer_reset(&(parameters.timer_receive_results));
  timer_restart(&(parameters.timer_global));
  input_file = fopen(parameters.input, "r");
  if (input_file==NULL) {
    fprintf(stderr,"Input file '%s' couldn't be opened\n",parameters.input);
    exit(1);
  }
  timer_reset(&parameters.timer_align);
  // Initizalize sequence-buffer
  unsigned int page_size = sysconf(_SC_PAGESIZE);
  const uint64_t buffer_aling_bytes = page_size; //4096; // Align to 4KB page size
  sequence_buffer_t* const sequence_buffer = sequence_buffer_new(2*10000000,150,buffer_aling_bytes);
  // Read-align loop
  bool pending_reads = true;
  int reads_processed = 0, progress = 0, batch_idx;
  int max_pattern_length = 0, max_text_length = 0;
  while (pending_reads) {
    // Read batch-input sequence-pair
    timer_start(&parameters.timer_parsing_input);
    sequence_buffer_clear(sequence_buffer);
    for (batch_idx=0;batch_idx<parameters.batch_size;++batch_idx) {
      // Read queries
      line1_length = getline(&line1,&line1_allocated,input_file);
      if (line1_length==-1) break;
      line2_length = getline(&line2,&line2_allocated,input_file);
      if (line1_length==-1) break;
      // Configure input
      char* const pattern = line1+1;
      const int pattern_length = line1_length-2;
      pattern[pattern_length] = '\0';
      char* const text = line2+1;
      const int text_length = line2_length-2;
      text[text_length] = '\0';
      max_pattern_length =  MAX(max_pattern_length,pattern_length);
      max_text_length =  MAX(max_text_length,text_length);
      // Add pair pattern-text
       
      if(parameters.seq_len == 300)
         sequence_buffer_add_pair(sequence_buffer,pattern,pattern_length,text,text_length,8,4,300,300,24);
      else if (parameters.seq_len == 150)
         sequence_buffer_add_pair(sequence_buffer,pattern,pattern_length,text,text_length,8,4,150,150,4);
      else // if (parameters.seq_len == 100) 
         sequence_buffer_add_pair(sequence_buffer,pattern,pattern_length,text,text_length,8,4,100,100,40);
       
    }
     
    timer_stop(&parameters.timer_parsing_input);
    if (batch_idx==0) break;
    // Align
    switch (alg_algorithm) {
      case alignment_gap_affine_swg:
        benchmark_swg_parallel(sequence_buffer,max_pattern_length,max_text_length);
        break;
      case alignment_gap_affine_wavefront:
        benchmark_gap_affine_wavefront_parallel(sequence_buffer,max_pattern_length,max_text_length);
        break;
      case alignment_gap_affine_wavefront_fpga:
        benchmark_wavefront_fpga(sequence_buffer,max_pattern_length,max_text_length);
        break;
      default:
        fprintf(stderr,"Algorithm unknown or not implemented\n");
        exit(1);
        break;
    }
    // Update Progress
    progress += batch_idx;
    reads_processed += batch_idx;
    if (progress >= parameters.progress) {
      progress = 0;
      // Compute speed
      const uint64_t time_elapsed_global = timer_elapsed_ns(&(parameters.timer_global));
      const float rate_global = (float)reads_processed/(float)TIMER_CONVERT_NS_TO_S(time_elapsed_global);
      const uint64_t time_elapsed_alg = timer_elapsed_ns(&(parameters.timer_align));
      const float rate_alg = (float)reads_processed/(float)TIMER_CONVERT_NS_TO_S(time_elapsed_alg);
      fprintf(stderr,"...processed %d reads (benchmark=%2.3f reads/s;alignment=%2.3f reads/s)\n",
          reads_processed,rate_global,rate_alg);
    }
  }
  timer_stop(&parameters.timer_global);
  // Print benchmark results
  fprintf(stderr,"[Benchmark]\n");
  fprintf(stderr,"=> Total.reads            %d\n",reads_processed);
  fprintf(stderr,"=> Time.Benchmark      ");
  timer_print(stderr,&parameters.timer_global,NULL);
  fprintf(stderr,"  => Time.Parsing.Input    ");
  timer_print(stderr,&parameters.timer_parsing_input,&parameters.timer_global);
  fprintf(stderr,"  => Time.Send.Data        ");
  timer_print(stderr,&parameters.timer_send_data,&parameters.timer_global);
  fprintf(stderr,"  => Time.Alignment        ");
  timer_print(stderr,&parameters.timer_align,&parameters.timer_global);
  fprintf(stderr,"  => Time.Receive.Results  ");
  timer_print(stderr,&parameters.timer_receive_results,&parameters.timer_global);
  // Free
  sequence_buffer_delete(sequence_buffer);
  fclose(input_file);
  free(line1);
  free(line2);
}
/*
 * Generic Menu
 */
void usage() {
  fprintf(stderr,
      "USE: ./align_benchmark -a algorithm -i input                         \n"
      "      Options::                                                      \n"
      "        [Input]                                                      \n"
      "          --algorithm|a <algorithm>                                  \n"
      "            [gap-affine]                                             \n"
      "              gap-affine-swg                                         \n"
      "              gap-affine-wfa                                         \n"
      "              gap-affine-fpga                                        \n"
      "          --input|i <File>                                           \n"
      "          --batch-size|b <Integer>                                   \n"
      "        [Penalties]                                                  \n"
      "          --affine-penalties|g M,X,O,E                               \n"
      "        [Misc]                                                       \n"
      "          --progress|P <integer>                                     \n"
      "          --num-threads|t <integer>                                  \n"
      "          --seq-len|t <integer>                                      \n"
      "          --check|c 'correct'|'score'|'alignment'                    \n"
      "          --help|h                                                   \n");
}
void parse_arguments(int argc,char** argv) {
  struct option long_options[] = {
    /* Input */
    { "algorithm", required_argument, 0, 'a' },
    { "input", required_argument, 0, 'i' },
    { "batch-size", required_argument, 0, 'b' },
    /* Penalties */
    { "affine-penalties", required_argument, 0, 'g' },
    /* Misc */
    { "progress", required_argument, 0, 'P' },
    { "num-threads", optional_argument, 0, 't' },
    { "seq-len", optional_argument, 0, 'l' },
    { "check", optional_argument, 0, 'c' },
    { "verbose", no_argument, 0, 'v' },
    { "help", no_argument, 0, 'h' },
    { 0, 0, 0, 0 } };
  int c,option_index;
  if (argc <= 1) {
    usage();
    exit(0);
  }
  while (1) {
    c=getopt_long(argc,argv,"a:i:b:g:P:t:l:c:vh",long_options,&option_index);
    if (c==-1) break;
    switch (c) {
    /*
     * Input
     */
    case 'a':
      parameters.algorithm = optarg;
      break;
    case 'i':
      parameters.input = optarg;
      break;
    case 'b':
      parameters.batch_size = atoi(optarg);
      break;
    /*
     * Penalties
     */
    case 'g': { // --affine-penalties
      char* sentinel = strtok(optarg,",");
      parameters.affine_penalties.match = atoi(sentinel);
      sentinel = strtok(NULL,",");
      parameters.affine_penalties.mismatch = atoi(sentinel);
      sentinel = strtok(NULL,",");
      parameters.affine_penalties.gap_opening = atoi(sentinel);
      sentinel = strtok(NULL,",");
      parameters.affine_penalties.gap_extension = atoi(sentinel);
      break;
    }
    /*
     * Misc
     */
    case 'P':
      parameters.progress = atoi(optarg);
      break;
    case 't':
      parameters.num_threads = atoi(optarg);
      printf("\n\nthread num = %d ================================================================================\n", parameters.num_threads);
      break;
    case 'l':
      parameters.seq_len = atoi(optarg);
      printf("Seq len = %d \n", parameters.seq_len);
      break;
    case 'c':
      if (optarg ==  NULL) { // default = score
        parameters.check_correct = true;
        parameters.check_score = true;
        parameters.check_alignments = false;
      } else if (strcasecmp(optarg,"correct")==0) {
        parameters.check_correct = true;
        parameters.check_score = false;
        parameters.check_alignments = false;
      } else if (strcasecmp(optarg,"score")==0) {
        parameters.check_correct = true;
        parameters.check_score = true;
        parameters.check_alignments = false;
      } else if (strcasecmp(optarg,"alignment")==0) {
        parameters.check_correct = true;
        parameters.check_score = true;
        parameters.check_alignments = true;
        printf("Compare Mode Enabled and Number of Threads is Forsed to 1\n");
      } else {
        fprintf(stderr,"Option '--check' must be in {'correct','score','alignment'}\n");
        exit(1);
      }
      break;
    case 'v':
      parameters.verbose = true;
      break;
    case 'h':
      usage();
      exit(1);
    // Other
    case '?': default:
      fprintf(stderr,"Option not recognized \n");
      exit(1);
    }
  }
  // Checks
  if (parameters.algorithm==NULL) {
    fprintf(stderr,"Option --algorithm is required \n");
    exit(1);
  }
  if (parameters.input==NULL) {
    fprintf(stderr,"Option --input is required \n");
    exit(1);
  }
}
int main(int argc,char* argv[]) {
  parse_arguments(argc,argv);
  // Select option
  if (strcmp(parameters.algorithm,"gap-affine-swg")==0) {
    align_benchmark(alignment_gap_affine_swg);
  } else if (strcmp(parameters.algorithm,"gap-affine-wfa")==0) {
    align_benchmark(alignment_gap_affine_wavefront);
    
  } else if (strcmp(parameters.algorithm,"gap-affine-fpga")==0) {
    align_benchmark(alignment_gap_affine_wavefront_fpga);
  } else {
    fprintf(stderr,"Algorithm '%s' not recognized\n",parameters.algorithm);
    exit(1);
  }
}
