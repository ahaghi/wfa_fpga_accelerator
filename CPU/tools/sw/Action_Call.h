#ifndef __ACTION_CALL__
#define __ACTION_CALL__

#ifdef __cplusplus
extern "C"
{
#endif
   
#include <stdbool.h>   
//#include <libsnap.h>  
//#include <snap_types.h>   
//#include <snap_tools.h>
//#include <snap_hls_if.h>
   
#include <osnap_types.h>   
#include <osnap_tools.h>
#include <libosnap.h>
#include <osnap_hls_if.h>
   
/* This number is unique and is declared in ~snap/ActionTypes.md */
#define ACTION_TYPE 0x1000000A


/* Data structure used to exchange information between action and application */
/* Size limit is 108 Bytes */
typedef struct action_job {
	struct snap_addr in;	    /* input data */
	struct snap_addr out;    /* offset table */
   
	uint32_t src_data_size_l;
	uint32_t src_data_size_h;
	uint32_t dst_data_size_l;
	uint32_t dst_data_size_h;
	uint32_t read_burst_num;
	uint32_t write_burst_num;
	uint32_t transfer_type;
} action_job_t;

   
//====================================== fills the MMIO registers / data structure =========================================//   

bool snap_function (uint32_t transfer_type, uint64_t Src_Data_Size, uint64_t Dst_Data_Size, uint64_t* dst_data_size_return, void* Src_Addr, void* Dst_Addr, uint32_t write_burst_num, uint32_t read_burst_num);
 
#ifdef __cplusplus
}
#endif

#endif	/* __ACTION_CALL__ */