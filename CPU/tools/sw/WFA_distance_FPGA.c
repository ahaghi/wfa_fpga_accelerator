
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/time.h>
#include "Action_Call.h"

void Recover_Cigar(char* ptrn, char* text, uint32_t ptrn_len, uint32_t text_len, uint64_t BT, char* BTF){
   int v = 0;
   int h = 0;
   int i = 0;
   uint64_t BT_tmp = BT;
   int      BT_Base;
   while (v < ptrn_len && h < text_len) {

      if (*(ptrn+v) == *(text+h)) {
         BTF[i] = 'M';
      }
      else { 
         BT_Base = BT_tmp & 3;
         BT_tmp = BT_tmp >> 2;

         switch (BT_Base) {
         case 2:
           BTF[i] = 'D';
           h--;
           break;
         case 1:
           BTF[i] = 'I';
           v--;    
           break;
         default: // 3
           BTF[i] = 'X';
         }
      }
      i++;
      v++;
      h++;
   }
   
   if (v < ptrn_len){
      while (v < ptrn_len){
         BTF[i] = 'D';
         v++;
         i++;
      }
   }
   
   else {
      while (h < text_len){
         BTF[i] = 'I';
         h++;
         i++;
      }
   }
}

static uint64_t get_usec(void)
{
        struct timeval t;

        gettimeofday(&t, NULL);
        return t.tv_sec * 1000000 + t.tv_usec;
}

int main(int argc,char* argv[]) {

   //const char* pattern_mem = "ACTTTACTGCGCGTTTGGAGAAATAAAATAGTTCTATACTGCGCGTTTGGAGAAATAAAATAGTTCTATACTGCGCGTTTGGAGAAGTTCTATACTGTAGTTCTATACTGCGCGTTTGGAGAAATAAAATGTTCTATACTGCGCGTTTGG";       
   //const char* text_mem    = "CTTTACTGCGCGTTTGGAAGAAATAAAATAGTTCTATCTGCGCGTTTGGAGAAAATAAAATAGTTCTATACTGCCGTTTGGAGAAGTTCTAATACTGTAGTTCTATACTGCCGTTTGGAGAAATAAAATAGTTCTATACTGCGCGTTTGG";
                              
   const char* pattern_mem = "GGTCTTCGAAACCCCTTGCATTTGCTGGGTTTGCTTTTCATGTAGGAACTACGTTATGTCGTAAAATAACATAAGTGGTACTTCAGTTAGGCGTCTTCCACG";
   const char* text_mem    = "GGTCTTCGAAACCCCTTGCATTTGCTGGGTTTGCAAAACATGTAGGAACTACGTTATGTCGTAAAATAACATAAGTGGTACTTCAGAAGCGTCTTCCACG";
   
   const uint32_t pattern_length = strlen(pattern_mem);
   const uint32_t text_length    = strlen(text_mem);
   
   uint64_t t_start;
   uint64_t t_end;
   uint64_t timer;
   uint32_t action_type = 0;
   uint64_t i;

   const uint64_t reps = 1024;
   uint64_t  Src_Data_Size = 5 * 64 * reps;
   uint64_t  Dst_Data_Size = 4 * 16 * reps; // 5 ID + 3 Distance 
   //uint64_t  Dst_Data_Size = 5 * 64 * reps; // ASCII  CIGAR
   //uint64_t  Dst_Data_Size = 1 * 64 * reps;   // Binary CIGAR
   uint64_t  dst_data_size_return_local;
	void* Src_Addr;
	void* Dst_Addr;
   
  // //FILE *fp;
   //char* samples;
   //fp = fopen("/home/ahaghi/Samples/dataset140_5.seq", "r");
   //fseek(fp, 0, SEEK_END); 
   //uint64_t size = ftell(fp); 
   //samples = (char *) malloc(size);
   //printf("size = %ld\n", size);
   //fseek(fp, 0, SEEK_SET); 
   //fread(samples, size, 1, fp);
   //fclose(fp);
   //printf("size = %d", size);
   
   Src_Addr = (char*) snap_malloc(Src_Data_Size);
	Dst_Addr = (char*) snap_malloc(Dst_Data_Size);
   char *ptr; 
   char *txt; 
   ptr = malloc(150);
   txt = malloc(150);
   uint32_t ptr_len;
   uint32_t txt_len;
   uint64_t offset = 0;
   uint64_t  ID = 1;
   int cmp_flag = 0;
   
   t_start = get_usec();
   i = 0;
   
   //while (offset < size){ // <size){
   //   if(*(samples+offset) == '>'){
   //      offset++;
   //      ptr_len = 0;
   //      while (*(samples+offset) != '\n'){
   //         *(ptr+ptr_len) = *(samples+offset);
   //         ptr_len++;
   //         offset++;
   //      }
   //      cmp_flag = 0;
   //      offset++;
   //   }
   //   else if (*(samples+offset) == '<') {
   //      offset++;
   //      txt_len = 0;
   //      while (*(samples+offset) != '\n'){
   //         *(txt+txt_len) = *(samples+offset);
   //         txt_len++;
   //         offset++;
   //      } 
   //      cmp_flag = 1;
   //      offset++;
   //   }
   //   else {
   //     cmp_flag = 0;
   //     offset++;
   //   }
   //   
   //   if (cmp_flag == 1){
   //      memcpy(Src_Addr+i          , &ID            , 8  );
   //      memcpy(Src_Addr+i+8        , &ptr_len       , 4  );
   //      memcpy(Src_Addr+i+8+4      , &txt_len       , 4  );
   //      memcpy(Src_Addr+i+8+4+4    , ptr            , 150);
   //      memcpy(Src_Addr+i+8+4+4+150, txt            , 150); 
   //      ID++;
   //      i = i+320;
   //   }
   //}
   
   for (i = 0; i < Src_Data_Size; i = i+320){
       memcpy(Src_Addr+i          , &ID               , 8  );
       memcpy(Src_Addr+i+8        , &pattern_length   , 4  );
       memcpy(Src_Addr+i+8+4      , &text_length      , 4  );
       memcpy(Src_Addr+i+8+4+4    , pattern_mem       , 150);
       memcpy(Src_Addr+i+8+4+4+150, text_mem          , 150); 
       ID++;
   }
   
   if (!snap_function(action_type, Src_Data_Size, Dst_Data_Size, &dst_data_size_return_local, Src_Addr, Dst_Addr, 64, 64)){
      printf("FPGA Failed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   }
        
   t_end = get_usec();
   timer = t_end - t_start;
   printf("distance Time = %ld\n", timer);   

   //// Binary CIGAR:
   //printf("dst_data_size_return_local = %ld\n", dst_data_size_return_local);
   //int64_t tmp_ID, tmp_dist;//, k_max;
   //uint8_t k_max, cigar_len;
   ////char BT[170] = {};
   //char PTRN[170] = {};
   //char TEXT[170] = {};
 
   //memcpy(PTRN, pattern_mem, 170);
   //memcpy(TEXT, text_mem   , 170); 
   //
   //for (i = 0; i < dst_data_size_return_local*64; i = i+(1*64)){
   //   tmp_ID = 0;
   //   tmp_dist = 0;
   //   k_max = 0;
   //   cigar_len = 0;
   //   char BT_binary[43] = {};
   //   char BT_ASCII[180] = {};
   //   char BT_ASCII2[180] = {};
   //   memcpy(&tmp_ID   , Dst_Addr+i   , 4);
   //   memcpy(&tmp_dist , Dst_Addr+i+4 , 3);
   //   memcpy(&k_max    , Dst_Addr+i+7 , 1);
   //   memcpy(&cigar_len, Dst_Addr+i+8 , 1);
   //   memcpy(BT_binary , Dst_Addr+i+9 , 43);
   //   int j;
   //   for (j = 0; j < 43 ; j++){
   //      int k;
   //      for (k = 0; k < 4 ; k++){
   //         switch (((BT_binary[j] >> (k*2)) & 3)){
   //            case 0:
   //               BT_ASCII[j*4+k] = 'M';
   //               break;
   //            case 1:
   //               BT_ASCII[j*4+k] = 'I';
   //               break;   
   //            case 2:
   //               BT_ASCII[j*4+k] = 'D';
   //               break;                       
   //            default:
   //               BT_ASCII[j*4+k] = 'X';
   //         } 
   //      }
   //   }
   //   memcpy(BT_ASCII2 , BT_ASCII+(170-cigar_len) , cigar_len);
   //   
   //   //if (tmp_ID < 1000){
   //    printf("== ID = %ld ================================================================\n", tmp_ID-1);
   //    printf("   Score = %ld ,   k_max = %d,   cigar_len = %d\n", tmp_dist, k_max, cigar_len);
   //      printf("   TEXT  = %s\n", TEXT);
   //      printf("   PTRN  = %s\n", PTRN);
   //      printf("   CIGAR = %s\n", BT_ASCII2);
   //      printf("============================================================================\n");
   //   //}
   //   
   //   //char test = " ";
   //   //int j;
   //   //for(j = 0; j < 150; j++){
   //   //   memcpy(&test, Dst_Addr+i+8+j , 1);
   //   //   printf("%c ", test);
   //   //}
   //}
   //
   //
   //
   
   /*
   printf("dst_data_size_return_local = %ld\n", dst_data_size_return_local/3);
   int64_t tmp_ID, tmp_dist;//, k_max;
   uint8_t k_max, cigar_len;
   //char BT[170] = {};
   char PTRN[170] = {};
   char TEXT[170] = {};
   
   memcpy(PTRN, pattern_mem, 170);
   memcpy(TEXT, text_mem   , 170); 
   int k = 0;
   for (i = 0; i < dst_data_size_return_local*64; i = i+(3*64)){
   //for (i = 0; i < 3*64; i = i+1){
      tmp_ID = 0;
      tmp_dist = 0;
      k_max = 0;
      cigar_len = 0;
      char BT[180] = {};
      memcpy(&k_max   , Dst_Addr+i   , 1);
      memcpy(&tmp_ID   , Dst_Addr+i   , 4);
      memcpy(&tmp_dist , Dst_Addr+i+4 , 3);
      memcpy(&k_max    , Dst_Addr+i+7 , 1);
      memcpy(&cigar_len    , Dst_Addr+i+8 , 1);
      memcpy(BT        , Dst_Addr+i+9 , 170);
      ////printf("%x", k_max);
      //if (k < 10 ){
      //   k++;
      //   printf("== ID = %ld ================================================================\n", tmp_ID-1);
      //   printf("   Score = %ld,   k_max = %d,   cigar_len = %d\n", tmp_dist, k_max, cigar_len);
      //   printf("   TEXT  = %s\n", TEXT);
      //   printf("   PTRN  = %s\n", PTRN);
      //   printf("   CIGAR = %s\n", BT);
      //   printf("\n============================================================================\n");
      //}
      
      //char test = " ";
      //int j;
      //for(j = 0; j < 150; j++){
      //   memcpy(&test, Dst_Addr+i+8+j , 1);
      //   printf("%c ", test);
      //}
   }
   */
   
   ///*
   printf("dst_data_size_return_local = %ld\n", dst_data_size_return_local*4);
   char ptr1[200]  = {};
   char txt1[200]  = {};
   int k = 0;
   for (i = 0; i < dst_data_size_return_local*4*16; i = i+16){
      uint64_t tmp_ID = 0;
      uint64_t tmp_dist = 0;
      uint8_t k_max = 0;
      uint64_t BT = 0;
      memcpy(&tmp_ID   , Dst_Addr+i   , 4);
      memcpy(&tmp_dist , Dst_Addr+i+4 , 3);
      memcpy(&k_max    , Dst_Addr+i+7 , 1);
      memcpy(&BT       , Dst_Addr+i+8 , 8);
      
     char BTF[200]  = {};
     Recover_Cigar(pattern_mem, text_mem, pattern_length, text_length, BT, BTF);
     memcpy(ptr1, pattern_mem, pattern_length);
     memcpy(txt1, text_mem, text_length);
     if (k < 5){
        k++;
        printf("== ID = %ld ================================================================\n", tmp_ID-1);
        printf("   Score = %ld,   k_max = %d\n", tmp_dist, k_max);
        printf("   TEXT  = %s\n", txt1);
        printf("   PTRN  = %s\n", ptr1);
        printf("   CIGAR = %s\n", BTF);
        printf("\n============================================================================\n");
     }      
        //// Printing N Data =====================================================
        //const int N = 10;
        //if(j < N){
        //   j++;
        ////if (tmp_dist == 0 && k_max > 0){
        //   memcpy(ptr, ptrn, ptrn_len);
        //   memcpy(txt, text, text_len);
        //   printf("== ID = %ld ======================================================================================================================================================\n", tmp_ID);
        //   printf("   Score = %ld ,   k_max = %d,   \n", tmp_dist, k_max);
        //   printf("   PTRN  = %s \n", ptr);
        //   printf("   TEXT  = %s \n", txt);
        //   printf("   CIGAR = %s \n", BTF);
        //   printf("=================================================================================================================================================================\n\n");  
        ////}
        //}
      
   }
   //*/
   /*
   uint64_t tmp_ID, tmp_dist, k_max;
   uint8_t UP_Code, State;
   int16_t offsetP, Pos1, Pos2, MatchNum, NewOffset, startpos;
   int j = 0;
   int k = 0;
   char umsg[]  = "          ";
   char umsg2[] = "          ";
      
   for (i = 0; i < 10*64; i = i+64){
      tmp_ID = 0;
      tmp_dist = 0;
      k_max = 0;
      UP_Code = 0;
      State = 0;
      
      memcpy(&tmp_ID   , Dst_Addr+i     , 5);
      memcpy(&UP_Code  , Dst_Addr+i+5   , 1);
      switch (UP_Code)
      {
         case 0:
            memcpy(umsg , "timer OVF ", 10);
            break;
      
         case 1:
            memcpy(umsg , "State Chng", 10);
            break;
                 
         case 2:
            memcpy(umsg , "Extnd Strt", 10);
            break;
                 
         default:
            memcpy(umsg , "Extnd End ", 10);
      }
      
      memcpy(&State    , Dst_Addr+i+6   , 1);
      switch (State)
      {
         case 0:
            memcpy(umsg2 , "Idle      ", 10);
            break;
      
         case 1:
            memcpy(umsg2 , "Extend    ", 10);
            break;
                 
         case 2:
            memcpy(umsg2 , "Compute   ", 10);
            break;
                 
         default:
            memcpy(umsg2 , "Finish    ", 10);
      }
      
      memcpy(&tmp_dist , Dst_Addr+i+7   , 3);
      memcpy(&k_max    , Dst_Addr+i+10  , 2);
      memcpy(&startpos , Dst_Addr+i+12  , 2);
      printf("ID = %ld --> UP_Code = %s,  State = %s,  score = %ld ,   k = %ld   startpos = %d\n", tmp_ID, umsg, umsg2, tmp_dist, k_max, startpos);
      k=0;
      
      for (j=0; j < 8; j++){
         offsetP = 0;
         memcpy(&offsetP    , Dst_Addr+i+14+k , 2);
         printf("k = %d - in_Offsets = %d \n", j, offsetP);
         k += 2;
      }
      
      Pos1     = 0;
      Pos2     = 0;
      MatchNum = 0;
      NewOffset= 0;

      memcpy(&NewOffset , Dst_Addr+i+14+k , 2);
      k += 2;
      memcpy(&MatchNum  , Dst_Addr+i+14+k , 2);
      k += 2;
      memcpy(&Pos2      , Dst_Addr+i+14+k , 2);
      k += 2;
      memcpy(&Pos1      , Dst_Addr+i+14+k , 2);
      k += 2;

      printf(" ==== Pos1 = %d,   Pos2 = %d,   MatchNum = %d,   NewOffset = %d\n", Pos1, Pos2,MatchNum,NewOffset);
      
      for (j=0; j < 8; j++){
         offsetP = 0;
         memcpy(&offsetP    , Dst_Addr+i+14+k , 2);
         printf("k = %d - out_Offsets = %d \n", j, offsetP);
         k += 2;
      }   
   }   
   */
   //int j = 0;
   //int k = 0;
   //
   //for (i = 0; i < 2*64; i = i+64){
   //   tmp_ID = 0;
   //   tmp_dist = 0;
   //   k_max = 0;
   //   memcpy(&tmp_ID   , Dst_Addr+i   , 5);
   //   memcpy(&tmp_dist , Dst_Addr+i+5 , 3);
   //   memcpy(&k_max    , Dst_Addr+i+8 , 2);
   //   printf("ID = %ld --> Dist = %ld ,   k_max = %ld\n", tmp_ID, tmp_dist, k_max);
   //   k=0;
   //   for (j=-32; j <= 32; j++){
   //      if ((j>-30 && j<-10) || (j>10 && j<30)){
   //         continue;
   //      }
   //      else{
   //         offsetP = 0;
   //         memcpy(&offsetP    , Dst_Addr+i+10+k , 2);
   //         printf("k = %d - Offset = %d \n", j, offsetP);
   //         k += 2;
   //      }
   //   }
   //   
   //}
   
     
}









