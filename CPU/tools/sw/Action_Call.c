
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <assert.h>
#include <getopt.h>
#include <ctype.h>

#include "Action_Call.h"

//====================================== fills the MMIO registers / data structure =========================================//   
static void snap_prepare_action(struct snap_job *cjob,
				 				struct action_job *mjob,
				 				//struct action_job *mjob_r,
				 				void *addr_in,
				 				uint32_t size_in,
				 				uint8_t type_in,
				 				void *addr_out,
				 				uint32_t size_out,
				 				uint8_t type_out,
                        uint64_t src_data_size,
                        uint64_t dst_data_size,
								uint32_t read_burst_num,
								uint32_t write_burst_num,
								uint32_t transfer_type)
{
	assert(sizeof(*mjob) <= SNAP_JOBSIZE);
	memset(mjob, 0, sizeof(*mjob));

	// Setting input params : where text is located in host memory
	snap_addr_set(&mjob->in, addr_in, size_in, type_in, SNAP_ADDRFLAG_ADDR | SNAP_ADDRFLAG_SRC);
	
	// Setting output params : where result will be written in host memory
	snap_addr_set(&mjob->out, addr_out, size_out, type_out, SNAP_ADDRFLAG_ADDR | SNAP_ADDRFLAG_DST | SNAP_ADDRFLAG_END);
	
   
	mjob->src_data_size_l = (uint32_t)src_data_size;
	mjob->src_data_size_h = (uint32_t)(src_data_size >> 32);
	mjob->dst_data_size_l = (uint32_t)dst_data_size;
	mjob->dst_data_size_h = (uint32_t)(dst_data_size >> 32);
   
   printf("src_data_size_l = %d , src_data_size_l = %d \n", mjob->src_data_size_l, mjob->src_data_size_h);
      
	mjob->read_burst_num = read_burst_num;
	mjob->write_burst_num = write_burst_num;
	mjob->transfer_type = transfer_type;
	
	//snap_job_set(cjob, mjob, sizeof(*mjob), mjob_r, sizeof(*mjob_r));
	snap_job_set(cjob, mjob, sizeof(*mjob), NULL, 0);
}

//============================================================================================================================================================
bool snap_function (uint32_t transfer_type, uint64_t Src_Data_Size, uint64_t Dst_Data_Size, uint64_t* dst_data_size_return, void* Src_Addr, void* Dst_Addr, uint32_t write_burst_num, uint32_t read_burst_num)//, struct snap_action *action)
{
	
	//int input_size = 0;
	//int src_size   = 0;
	//int dst_size   = 0;
	//long long  Data_Size = 0;
	
	uint8_t  type_in = SNAP_ADDRTYPE_HOST_DRAM;
	//uint64_t addr_in = 0x0ull;
	//char *   addr_in1;
	uint8_t  type_out = SNAP_ADDRTYPE_HOST_DRAM;
	//uint64_t addr_out = 0x0ull;
	//uint32_t read_burst_num = 16;
	//uint32_t write_burst_num = 16;
   
	//uint32_t transfer_type = 4;
	uint32_t dst_size_return_h = 0;
	uint32_t dst_size_return_l = 0;

	int rc = 0;
   uint32_t action_data = 0;
	int card_no = 7;
	struct snap_card *card = NULL;
	struct snap_action *action = NULL;
	char device[128];
	struct snap_job cjob;
	struct action_job mjob;
	//struct action_job mjob_r;
	unsigned long timeout = 60;
	struct timeval etime, stime1, etime3, stime3;//etime1, stime1, etime2, stime2, 

	snap_action_flag_t action_irq = 0; //(SNAP_ACTION_DONE_IRQ | SNAP_ATTACH_IRQ); //no irq for now; snap_action_flag_t is an enum defined in snaplib 
	

	//-- Offloading Action -----------------------------------------------------------------------------------------------
   printf("src_addr = %lx , src_addr = %lx, src_size = %ld \n", (uint64_t)Src_Addr, (uint64_t)Dst_Addr, Src_Data_Size);
	//-- Card Allocation --
   
	gettimeofday(&stime1, NULL);
	
    if (card_no == 0) {
        snprintf (device, sizeof (device) - 1, "IBM,oc-snap");
    } else {
        snprintf (device, sizeof (device) - 1, "/dev/ocxl/IBM,oc-snap.000%d:00:00.1.0", card_no);
    }
   
	//snprintf(device, sizeof(device)-1, "/dev/cxl/afu%d.0s", card_no);
	card = snap_card_alloc_dev(device, SNAP_VENDOR_ID_IBM, SNAP_DEVICE_ID_SNAP); //Constants defined in snaplib
	if (card == NULL) {
		fprintf(stderr, "err: failed to open card %u: %s\n", card_no, strerror(errno));
		goto out_error;
	}
	
	//gettimeofday(&etime1, NULL);
	
	printf("Card Allocated Successfully\n");
	
	//-- Attaching Action --
		
	//gettimeofday(&stime2, NULL);
	
	action = snap_attach_action(card, ACTION_TYPE, action_irq, 60);
	if (action == NULL) {
		fprintf(stderr, "err: failed to attach action %u: %s\n", card_no, strerror(errno));
		goto out_error1;
	}
	
	//gettimeofday(&etime2, NULL);
	
	printf("Action Attached Successfully\n");
   
	
	//-- Puting Data Addr and Size in cjob structure --
	
	//gettimeofday(&stime3, NULL);
	
	snap_prepare_action(&cjob, &mjob,  (void *)Src_Addr,  0, type_in, (void *)Dst_Addr, 0, type_out, Src_Data_Size, Dst_Data_Size, read_burst_num, write_burst_num, transfer_type);
	
	//gettimeofday(&etime3, NULL);
	
	//-- TimeStamp1 --
	//gettimeofday(&stime, NULL);

	//-- Set MMIO, Start Action, Wait for Idle --
	rc = snap_action_sync_execute_job(action, &cjob, timeout);

   snap_action_read32(card, 0x01B8, &dst_size_return_l);
   snap_action_read32(card, 0x01BC, &dst_size_return_h);
   snap_action_read32(card, ACTION_CONTROL, &action_data);
   snap_action_write32(card, ACTION_CONTROL, ACTION_CONTROL_RST);
	snap_action_write32(card, ACTION_CONTROL, action_data);
   
   *dst_data_size_return = ((((uint64_t)(dst_size_return_h)) << 32) | ((uint64_t) (dst_size_return_l))); 

	//-- TimeStamp2 --
	//gettimeofday(&etime, NULL);
	
	if (rc != 0) {
		fprintf(stderr, "err: job execution %d: %s!\n", rc, strerror(errno));
		goto out_error2;
      //return(false);
	}
	
	//-- test return code --
	if (cjob.retc == SNAP_RETC_SUCCESS) 
		fprintf(stdout, "SUCCESS\n");
	else
	{	
		fprintf(stdout, "FAILED\n");
		fprintf(stderr, "err: Unexpected RETC=%x!\n", cjob.retc);
		goto out_error2;
      //return(false);
	}
		
	//-- Detach action --
	snap_detach_action(action);
	
	//-- disallocate the card --
	snap_card_free(card);

	//exit(EXIT_SUCCESS);
	return(true);
	
	out_error2:
		snap_detach_action(action);
	out_error1:
		snap_card_free(card);
	out_error:
		//exit(EXIT_FAILURE);
		return(false);
      
	
} // main end

