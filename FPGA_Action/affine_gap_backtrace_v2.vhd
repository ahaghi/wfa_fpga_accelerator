library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
--use ieee.numeric_Std.all;
use work.WFA_PKG.all;

entity affine_gap_backtrace_v2 is
   port (
      i_clk             : in     std_logic; 
      i_rst_n           : in     std_logic; 
      i_start           : in     std_logic; 
      i_kmax            : in     integer range -c_k_range to c_k_range;
      i_kdits           : in     integer range -c_k_range to c_k_range;
      i_final_addr      : in     std_logic_vector( 7 downto 0);
      o_rd_addr_m       : out    std_logic_vector( 7 downto 0);
      o_rd_addr_IO      : out    std_logic_vector( 7 downto 0);
      o_rd_addr_DO      : out    std_logic_vector( 7 downto 0);
      o_rd_addr_IE      : out    std_logic_vector( 7 downto 0);
      o_rd_addr_DE      : out    std_logic_vector( 7 downto 0);
      i_rd_data_m       : in     std_logic_vector(23 downto 0); 
      i_rd_data_io      : in     std_logic_vector(23 downto 0);  
      i_rd_data_do      : in     std_logic_vector(23 downto 0);  
      i_rd_data_ie      : in     std_logic_vector( 7 downto 0);  
      i_rd_data_de      : in     std_logic_vector( 7 downto 0);
      o_Back_Trace      : out    std_logic_vector(63 downto 0);
      o_busy            : out    std_logic
   );
end affine_gap_backtrace_v2;

architecture affine_gap_backtrace_v2 of affine_gap_backtrace_v2 is

   function Addr_finder(dist, k : integer range -c_k_range to c_k_range) return std_logic_vector is
      variable sec                    : integer range -c_k_range to c_k_range := 0;
      variable sec_start_pos          : integer range -256 to 255             := 0;
      variable dist_start_pos         : integer range -256 to 255             := 0;
      variable final_mem_addr         : integer range -256 to 255             := 0;
      variable local_addr_k_in_dist   : integer range -c_k_range to c_k_range := 0;
      variable start_pos              : integer range -c_k_range to c_k_range := 0;
      variable indx                   : integer range 0 to 7                  := 0;
   begin
      sec                  := ((dist - 1) / 8) + 1;
      sec_start_pos        := ((sec - 1) * 8 + 1) * sec;
      dist_start_pos       := ((dist - ((sec - 1) * 8 + 1)) * 2 * sec) + sec_start_pos;
      local_addr_k_in_dist := (sec * 8 + k) / 8;
      final_mem_addr       := dist_start_pos + local_addr_k_in_dist;

   return (conv_std_logic_vector(final_mem_addr,8));
   end Addr_finder;

   constant c_mismatch  : integer := 4/2;--4-1;
   constant c_gap_open  : integer := 8/2;--8-1;
   constant c_gap_ext   : integer := 2/2;--2-1;

   type	states is ( st_idle,
                     st_BT,
                     st_Finish
                   );

   signal state                  : states;
   signal s_BT                   : std_logic_vector(63 downto 0);
   signal s_mval_sel             : std_logic_vector(1 downto 0);
   signal s_mval                 : std_logic_vector(2 downto 0);
   signal s_mval_m               : std_logic_vector(2 downto 0);
   signal s_mval_i               : std_logic_vector(2 downto 0);
   signal s_mval_d               : std_logic_vector(2 downto 0);
   signal s_ival                 : std_logic;
   signal s_dval                 : std_logic;
   signal s_rd_valid             : std_logic;
   signal s_rd_valid1            : std_logic;
   signal s_dist                 : integer range -c_k_range to c_k_range;
   signal s_dist_m               : integer range -c_k_range to c_k_range;
   signal s_dist_o               : integer range -c_k_range to c_k_range;
   signal s_dist_e               : integer range -c_k_range to c_k_range;
--   signal final_mem_addr_Indx_m  : std_logic_vector(10 downto 0);
--   signal final_mem_addr_Indx_IO : std_logic_vector(10 downto 0);
--   signal final_mem_addr_Indx_DO : std_logic_vector(10 downto 0); 
--   signal final_mem_addr_Indx_IE : std_logic_vector(10 downto 0); 
--   signal final_mem_addr_Indx_DE : std_logic_vector(10 downto 0);
   signal s_curr_k               : integer range -c_k_range to c_k_range;

   signal sec                    : integer range -c_k_range to c_k_range;
   signal start_pos              : integer range -c_k_range to c_k_range;
   signal indx                   : integer range 0 to 7;

   --signal sec0                   : integer range -c_k_range to c_k_range;
   --signal sec1                   : integer range -c_k_range to c_k_range;
   --signal sec3                   : integer range -c_k_range to c_k_range;
   --signal sec_start_pos0         : integer range -c_k_range to c_k_range;
   --signal sec_start_pos1         : integer range -c_k_range to c_k_range;
   --signal sec_start_pos3         : integer range -c_k_range to c_k_range;
   --signal dist_start_pos0        : integer range -256 to 255; 
   --signal dist_start_pos1        : integer range -256 to 255; 
   --signal dist_start_pos3        : integer range -256 to 255;             
   --signal local_addr_k_in_dist0  : integer range -c_k_range to c_k_range;
   --signal local_addr_k_in_dist1  : integer range -c_k_range to c_k_range;
   --signal local_addr_k_in_dist2  : integer range -c_k_range to c_k_range;
   --signal local_addr_k_in_dist3  : integer range -c_k_range to c_k_range;
   --signal local_addr_k_in_dist4  : integer range -c_k_range to c_k_range;
   --signal final_mem_addr0        : integer range -256 to 255;  
   --signal final_mem_addr1        : integer range -256 to 255;   
   --signal final_mem_addr2        : integer range -256 to 255;   
   --signal final_mem_addr3        : integer range -256 to 255;   
   --signal final_mem_addr4        : integer range -256 to 255;  

   signal s_table_type           : integer range 0 to 3;

begin

   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         
         o_busy       <= '0';  
         o_Back_Trace <= (others => '0');
         s_BT         <= (others => '0');
         state        <= st_idle;
         s_rd_valid1  <= '0';
         s_rd_valid   <= '0';   
         s_table_type <= 0;
         s_dist       <= 0;
         s_curr_k     <= 0;
         s_mval_sel   <= "00";

      elsif(rising_edge(i_clk)) then
         
         s_rd_valid1 <= '0';
         s_rd_valid  <= s_rd_valid1;

         if(s_rd_valid1 = '1') then
            s_dist       <= i_kmax;
         end if;
            
         case state is
         when st_idle =>
            if (i_start = '1') then
               s_BT         <= (others => '0');
               o_Back_Trace <= (others => '0');
               s_dist       <= i_kmax+c_mismatch;
               s_curr_k     <= i_kdits;
               --s_rd_addr   <= i_final_addr;
               s_rd_valid1  <= '1';
               state        <= st_BT;
               s_table_type <= 0;
               o_busy       <= '1';
               s_mval_sel   <= "00";
            end if;
               
         when st_BT =>
            if (s_rd_valid = '1') then
               s_rd_valid <= '1';
               case s_table_type is
               when 0 => -- M_wavefront
                  case s_mval is
                  when "100" => -- m_mismatch(-4), k  , Mismatch
                     s_BT           <= s_BT(61 downto 0) & "00"; 
                     s_table_type   <= 0;
                     s_dist         <= s_dist - c_mismatch;
                     if(s_dist = 2) then
                        s_dist      <= 1;
                     end if;
                     s_curr_k       <= s_curr_k;
                     s_mval_sel     <= "00";
                  when "101" => -- m_gapopen (-8), k-1, Insertion
                     s_BT           <= s_BT(61 downto 0) & "01"; 
                     s_table_type   <= 0;
                     s_dist         <= s_dist - c_gap_open;
                     if(s_dist = 4) then
                        s_dist      <= 1;
                     end if;
                     s_curr_k       <= s_curr_k-1;
                     s_mval_sel     <= "01";
                  when "110" => -- m_gapopen (-8), k+1, Deletion
                     s_BT           <= s_BT(61 downto 0) & "10"; 
                     s_table_type   <= 0;
                     s_dist         <= s_dist - c_gap_open;
                     if(s_dist = 4) then
                        s_dist      <= 1;
                     end if;
                     s_curr_k       <= s_curr_k+1;
                     s_mval_sel     <= "10";
                  when "010" => -- d_gapext  (-2), k+1, Deletion
                     s_BT           <= s_BT(61 downto 0) & "11"; 
                     s_table_type   <= 1;
                     s_dist         <= s_dist - c_gap_ext;
                     s_curr_k       <= s_curr_k+1;
                  when "001" => -- i_gapext  (-2), k-1, Insertion
                     s_BT           <= s_BT(61 downto 0) & "11"; 
                     s_table_type   <= 2;
                     s_dist         <= s_dist - c_gap_ext;
                     s_curr_k       <= s_curr_k-1;
                  when others => -- not valid, exit with error
                     null;
                  end case;
                     
               when 1 => -- D_wavefront
                  if (s_dval = '0') then -- m_gapopen (-8), k+1, Deletion
                     s_BT           <= s_BT(61 downto 0) & "10"; 
                     s_table_type   <= 0;
                     s_dist         <= s_dist - c_gap_open;
                     s_mval_sel     <= "10";
                     if(s_dist = 4) then
                        s_dist      <= 1;
                     end if;
                     s_curr_k       <= s_curr_k+1;
                  else                   -- d_gapext  (-2), k+1, Deletion
                     s_BT           <= s_BT(61 downto 0) & "11"; 
                     s_table_type   <= 1;
                     s_dist         <= s_dist - c_gap_ext;
                     s_curr_k       <= s_curr_k+1;
                  end if;
                     
               when others => -- 0 => -- I_wavefront
                  if (s_ival = '0') then -- m_gapopen (-8), k-1, Insertion
                     s_BT           <= s_BT(61 downto 0) & "01"; 
                     s_table_type   <= 0;
                     s_dist         <= s_dist - c_gap_open;
                     if(s_dist = 4) then
                        s_dist      <= 1;
                     end if;
                     s_curr_k       <= s_curr_k-1;
                     s_mval_sel     <= "01";
                  else -- i_gapext  (-2), k-1, Insertion
                     s_BT           <= s_BT(61 downto 0) & "11"; 
                     s_table_type   <= 2;
                     s_dist         <= s_dist - c_gap_ext;
                     s_curr_k       <= s_curr_k-1;
                  end if; 
                     
               end case;       
                  
            end if;    
               
            if (s_dist <= 0) then
               state <= st_Finish;
            else
               state <= st_BT;
            end if;
  
         when st_Finish =>
            o_busy       <= '0';  
            o_Back_Trace <= s_BT;
            state        <= st_idle;
         
         when others =>
            null;
                     
         end case;

      end if;
   end process; 
         
   --s_mval <= i_rd_data(indx*3+2 downto indx*3);                 
   --s_ival <= i_rd_data(indx+24+8);                
   --s_dval <= i_rd_data(indx+24);

   s_dist_m <= 1 when (s_dist = 2) else (s_dist-c_mismatch);
   s_dist_o <= 1 when (s_dist = 4) else (s_dist-c_gap_open);
   s_dist_e <= s_dist-c_gap_ext;
         
   -- Mismatch
   o_rd_addr_m    <= Addr_finder(s_dist_m, s_curr_k); 
   s_mval_m       <= i_rd_data_m( 2 downto  0) when(indx = 0) else
                     i_rd_data_m( 5 downto  3) when(indx = 1) else
                     i_rd_data_m( 8 downto  6) when(indx = 2) else
                     i_rd_data_m(11 downto  9) when(indx = 3) else
                     i_rd_data_m(14 downto 12) when(indx = 4) else
                     i_rd_data_m(17 downto 15) when(indx = 5) else
                     i_rd_data_m(20 downto 18) when(indx = 6) else
                     i_rd_data_m(23 downto 21);        

   -- Insertion Open             
   o_rd_addr_IO   <= Addr_finder(s_dist_o, s_curr_k-1);       
   s_mval_i       <= i_rd_data_io( 2 downto  0) when(indx = 0) else
                     i_rd_data_io( 5 downto  3) when(indx = 1) else
                     i_rd_data_io( 8 downto  6) when(indx = 2) else
                     i_rd_data_io(11 downto  9) when(indx = 3) else
                     i_rd_data_io(14 downto 12) when(indx = 4) else
                     i_rd_data_io(17 downto 15) when(indx = 5) else
                     i_rd_data_io(20 downto 18) when(indx = 6) else
                     i_rd_data_io(23 downto 21);                

   -- Deletion Open             
   o_rd_addr_DO   <= Addr_finder(s_dist_o, s_curr_k+1);                    
   s_mval_d       <= i_rd_data_do( 2 downto  0) when(indx = 0) else
                     i_rd_data_do( 5 downto  3) when(indx = 1) else
                     i_rd_data_do( 8 downto  6) when(indx = 2) else
                     i_rd_data_do(11 downto  9) when(indx = 3) else
                     i_rd_data_do(14 downto 12) when(indx = 4) else
                     i_rd_data_do(17 downto 15) when(indx = 5) else
                     i_rd_data_do(20 downto 18) when(indx = 6) else
                     i_rd_data_do(23 downto 21);
                                 
   -- Insertion Extend             
   o_rd_addr_IE   <= Addr_finder(s_dist_e, s_curr_k-1);                                
   s_ival         <= i_rd_data_ie(0) when(indx = 0) else
                     i_rd_data_ie(1) when(indx = 1) else
                     i_rd_data_ie(2) when(indx = 2) else
                     i_rd_data_ie(3) when(indx = 3) else
                     i_rd_data_ie(4) when(indx = 4) else
                     i_rd_data_ie(5) when(indx = 5) else
                     i_rd_data_ie(6) when(indx = 6) else
                     i_rd_data_ie(7);               
                
   -- Deletion Extend             
   o_rd_addr_DE   <= Addr_finder(s_dist_e, s_curr_k+1);                       
   s_dval         <= i_rd_data_de(0) when(indx = 0) else
                     i_rd_data_de(1) when(indx = 1) else
                     i_rd_data_de(2) when(indx = 2) else
                     i_rd_data_de(3) when(indx = 3) else
                     i_rd_data_de(4) when(indx = 4) else
                     i_rd_data_de(5) when(indx = 5) else
                     i_rd_data_de(6) when(indx = 6) else
                     i_rd_data_de(7);
   
   sec                  <= ((s_dist - 1) / 8) + 1;
   start_pos            <= ((sec * 8 + s_curr_k) / 8) * 8 - sec * 8;
   indx                 <= s_curr_k - start_pos;           
                  
   s_mval <= s_mval_d when(s_mval_sel = "10") else
             s_mval_i when(s_mval_sel = "01") else
             s_mval_m;
    
      
end affine_gap_backtrace_v2;         