library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity axi_master_wr_2 is
   generic (
      C_M_AXI_ADDR_WIDTH      : integer   := 64;      -- Width of Address Bus
      C_M_AXI_DATA_WIDTH      : integer   := 512;     -- Width of Data Bus
      C_M_AXI_ID_WIDTH        : integer   := 1;       -- Thread ID Width
      C_M_AXI_AWUSER_WIDTH    : integer   := 0;       -- Width of User Write Address Bus
      C_M_AXI_WUSER_WIDTH     : integer   := 0;       -- Width of User Write Data Bus
      C_M_AXI_BUSER_WIDTH     : integer   := 0        -- Width of User Response Bus
   );
   port (   
   
      i_num_of_burst_wr       : in      integer;  
      i_axi_write_start       : in      std_logic;                      
      o_axi_write_idle        : out     std_logic;                     
      o_axi_write_done        : out     std_logic;  
      i_dst_addr              : in      std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
      --i_fifo_addr_empty       : in      std_logic;          
      i_fifo_data_empty       : in      std_logic;
      i_fifo_data_out         : in      std_logic_vector(C_M_AXI_DATA_WIDTH -1  downto 0);      
      --i_fifo_addr_out         : in      std_logic_vector(C_M_AXI_ADDR_WIDTH -1  downto 0);      
      o_fifo_data_rd_rq       : out     std_logic; 
      --o_fifo_addr_rd_rq       : out     std_logic; 
      i_wr_context_id         : in      std_logic_vector(C_M_AXI_AWUSER_WIDTH - 1 downto 0);
      i_action_finished       : in      std_logic;
      o_state_is_idle         : out     std_logic;
      
      -- AXI Write Signals --------------------------------------------------------------                                                
      M_AXI_ACLK              : in     std_logic;
      M_AXI_ARESETN           : in     std_logic;
      M_AXI_AWID              : out    std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
      M_AXI_AWADDR            : out    std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_AWLEN             : out    std_logic_vector(7 downto 0);
      M_AXI_AWSIZE            : out    std_logic_vector(2 downto 0);
      M_AXI_AWBURST           : out    std_logic_vector(1 downto 0);
      M_AXI_AWLOCK            : out    std_logic_vector(1 downto 0);
      M_AXI_AWCACHE           : out    std_logic_vector(3 downto 0);
      M_AXI_AWPROT            : out    std_logic_vector(2 downto 0);
      M_AXI_AWQOS             : out    std_logic_vector(3 downto 0);
      M_AXI_AWUSER            : out    std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
      M_AXI_AWVALID           : out    std_logic;
      M_AXI_AWREADY           : in     std_logic;
      M_AXI_WDATA             : out    std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_WSTRB             : out    std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
      M_AXI_WLAST             : out    std_logic;
      M_AXI_WUSER             : out    std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
      M_AXI_WVALID            : out    std_logic;
      M_AXI_WREADY            : in     std_logic;
      M_AXI_BID               : in     std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
      M_AXI_BRESP             : in     std_logic_vector(1 downto 0);
      M_AXI_BUSER             : in     std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
      M_AXI_BVALID            : in     std_logic;
      M_AXI_BREADY            : out    std_logic
   );
end axi_master_wr_2;

architecture axi_master_wr_2 of axi_master_wr_2 is
    
   function AXI_PORT_WIDTH_to_ARSIZE (AXI_DATA_WIDTH : integer) return std_logic_vector is            
      variable v_axi_arsize  : std_logic_vector(2 downto 0) := "000";                                                                    
   begin                                                                   
      case AXI_DATA_WIDTH is
      
      when 8   =>  
         v_axi_arsize := "000";
         
      when 16  => 
         v_axi_arsize := "001";
         
      when 32  =>  
         v_axi_arsize := "010";
         
      when 64  =>
         v_axi_arsize := "011";       
         
      when 128 =>  
         v_axi_arsize := "100";       
         
      when 256 =>
         v_axi_arsize := "101";       
         
      when 512 =>
         v_axi_arsize := "110";       
         
      when others    => --1024
         v_axi_arsize := "111";       
      
      end case;      
      
      return(v_axi_arsize);                                                         
   end;

    function Bytes_per_burst_transfer (Num_of_Burst : integer; AXI_DATA_WIDTH : integer) return std_logic_vector is                               
       variable v_bytes_per_burst_transfer   : std_logic_vector(31 downto 0) := (others => '0');           
       variable v_axi_data_width             : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(AXI_DATA_WIDTH, 32));
       
    begin                                                                   
      case Num_of_Burst is
         
      when 2         =>  --2
         v_bytes_per_burst_transfer := "00"  & v_axi_data_width(31 downto 2);
         
      when 4         =>  --4
         v_bytes_per_burst_transfer := '0'   & v_axi_data_width(31 downto 1);
         
      when 8         =>  --8
         v_bytes_per_burst_transfer :=         v_axi_data_width(31 downto 0);
         
      when 16        => --16  
         v_bytes_per_burst_transfer :=         v_axi_data_width(30 downto 0) & '0';
         
      when 32        => --32
         v_bytes_per_burst_transfer :=         v_axi_data_width(29 downto 0) & "00";
         
      when 64        => --64
         v_bytes_per_burst_transfer :=         v_axi_data_width(28 downto 0) & "000";
         
      when others    => --128
         v_bytes_per_burst_transfer :=         v_axi_data_width(27 downto 0) & "0000";
      
      end case;

      return(v_bytes_per_burst_transfer);                                                                                                             
    end;

   type AXI_WRITE_STATES is(st_write_idle,                               
                            st_write_wait_for_data,
                            st_write_process   
                           );
                            
   signal AXI_WRITE_State              : AXI_WRITE_STATES;
       
   signal s_wr_addr_valid              : std_logic;                     
   signal s_wr_addr                    : std_logic_vector(C_M_AXI_ADDR_WIDTH - 1 downto 0);
   signal s_wr_addr2                   : std_logic_vector(C_M_AXI_ADDR_WIDTH - 1 downto 0);
   signal s_wr_len                     : std_logic_vector(7 downto 0);
   signal s_wr_addr_ready              : std_logic;
   signal s_wr_data                    : std_logic_vector(C_M_AXI_DATA_WIDTH -1  downto 0);                   
   signal s_wr_data_valid              : std_logic;
   signal s_wr_data_last               : std_logic;                     
   signal s_wr_ready                   : std_logic;                     
   signal s_wr_bready                  : std_logic;                     
   signal s_wr_done                    : std_logic;
   signal s_wr_data_byte_num           : std_logic_vector(63 downto 0);
   signal s_wr_data_word_num           : std_logic_vector( 7 downto 0);
   signal s_wr_addr_offset             : std_logic_vector(C_M_AXI_ADDR_WIDTH - 1 downto 0); 
   signal s_bytes_per_transfer         : std_logic_vector(31 downto 0);
   signal s_cntr                       : integer range 0 to 127;
   signal test_cntr_axi_wr             : integer;
   signal s_fifo_data_rd_rq            : std_logic;
   signal s_fifo_addr_rd_rq            : std_logic;
   signal s_addr_read_flag             : std_logic;


begin

   M_AXI_AWID        <= (others => '0');
   M_AXI_AWADDR      <= s_wr_addr;
   M_AXI_AWLEN       <= s_wr_len;
   M_AXI_AWSIZE      <= AXI_PORT_WIDTH_to_ARSIZE(C_M_AXI_DATA_WIDTH);
   M_AXI_AWBURST     <= "01";
   M_AXI_AWLOCK      <= (others => '0');
   M_AXI_AWCACHE     <= "0010";
   M_AXI_AWPROT      <= "000";
   M_AXI_AWQOS       <= x"0";
   M_AXI_AWUSER      <= i_wr_context_id;
   M_AXI_AWVALID     <= s_wr_addr_valid;
   M_AXI_WDATA       <= s_wr_data;
   M_AXI_WSTRB       <= (others => '1');
   M_AXI_WLAST       <= s_wr_data_last;
   M_AXI_WUSER       <= (others => '0');
   M_AXI_WVALID      <= s_wr_data_valid;
   M_AXI_BREADY      <= '1';--s_wr_bready; 
   
   s_wr_done         <= M_AXI_BVALID;
   s_wr_addr_ready   <= M_AXI_AWREADY;
   s_wr_ready        <= M_AXI_WREADY;

   process(M_AXI_ACLK, M_AXI_ARESETN) is
   begin    
      if(M_AXI_ARESETN = '0') then
         o_state_is_idle      <= '0';  
         o_axi_write_idle     <= '0';
         s_cntr               <=  0;
         s_fifo_data_rd_rq    <= '0';
         s_fifo_addr_rd_rq    <= '0'; 
         s_addr_read_flag     <= '0';
         s_wr_data_last       <= '0';
         test_cntr_axi_wr     <= 0;
         s_wr_len             <= (others => '0');
         s_wr_addr2           <= (others => '0');
         s_wr_addr_offset     <= (others => '0');
         s_bytes_per_transfer <= (others => '0');
         AXI_WRITE_State      <= st_write_idle;
         --o_fifo_data_rd_rq    <= '0';
         --o_fifo_addr_rd_rq    <= '0';
         o_axi_write_done     <= '0';
      elsif(rising_edge(M_AXI_ACLK)) then




           if (s_wr_data_valid = '1') then
              test_cntr_axi_wr <= test_cntr_axi_wr + 1;
           end if;    
           
         o_state_is_idle   <= '0';
         
         case AXI_WRITE_State is
         
         --==================================================================================
         when st_write_idle =>
            o_axi_write_idle     <= '1';
            o_state_is_idle      <= '1';
            s_cntr               <=  0 ;
            s_fifo_data_rd_rq    <= '0';
            s_fifo_addr_rd_rq    <= '0'; 
            s_addr_read_flag     <= '0';
            s_wr_data_last       <= '0';

            if(i_axi_write_start = '1') then
               AXI_WRITE_State         <= st_write_wait_for_data;
               o_axi_write_done        <= '0';
               o_axi_write_idle        <= '0';
               s_wr_addr_offset        <= (others => '0');
               s_bytes_per_transfer    <= Bytes_per_burst_transfer(i_num_of_burst_wr,C_M_AXI_DATA_WIDTH);
               s_wr_len                <= std_logic_vector(to_unsigned(i_num_of_burst_wr - 1 , 8)); 
            else
               AXI_WRITE_State         <= st_write_idle;
            end if;
               
         --==================================================================================
         when st_write_wait_for_data =>     
            if (i_fifo_data_empty = '0') then -- and i_fifo_data_empty
               s_cntr                  <=  i_num_of_burst_wr;
               s_fifo_data_rd_rq       <= '1';
               s_fifo_addr_rd_rq       <= '1'; 
               s_wr_addr2              <= i_dst_addr + s_wr_addr_offset;
               AXI_WRITE_State         <= st_write_process;
            elsif (i_action_finished = '1' and i_fifo_data_empty = '1') then 
               o_axi_write_done        <= '1';
               AXI_WRITE_State         <= st_write_idle;
            else
               AXI_WRITE_State         <= st_write_wait_for_data;
            end if;
               
         --==================================================================================
         when st_write_process =>  
            if (s_wr_addr_valid = '1') then
               s_addr_read_flag     <= '1';
               s_fifo_addr_rd_rq    <= '0';
            end if;
            if (s_cntr > 1) then
               if (s_wr_data_valid = '1') then
                  s_cntr               <= s_cntr - 1;
                  s_fifo_data_rd_rq    <= '1';
                  if (s_cntr = 2) then -- last kmer
                     s_wr_data_last    <= '1';
                     s_wr_addr_offset  <= s_bytes_per_transfer + s_wr_addr_offset;
                  end if;   
               end if;
            elsif (s_wr_data_valid = '1') then 
               s_addr_read_flag        <= '0';
               s_wr_data_last          <= '0';
               s_fifo_data_rd_rq       <= '0';
               if (i_fifo_data_empty = '0') then 
                  s_cntr               <=  i_num_of_burst_wr;
                  s_fifo_data_rd_rq    <= '1';
                  s_fifo_addr_rd_rq    <= '1';  
                  s_wr_addr2           <= i_dst_addr + s_wr_addr_offset;
               else
                  AXI_WRITE_State      <= st_write_wait_for_data;
               end if;
            end if;
               
         --==================================================================================
         when others =>  
            null;
                  
         end case;
               
      end if;              
   end process;

   s_wr_addr            <= s_wr_addr2;--i_fifo_addr_out;
   s_wr_addr_valid      <= s_fifo_addr_rd_rq and s_wr_addr_ready;
   --o_fifo_addr_rd_rq    <= s_fifo_addr_rd_rq and s_wr_addr_ready;

   s_wr_data            <= i_fifo_data_out when (i_action_finished = '0' or i_fifo_data_empty = '0') else (others => '0');
   --s_wr_data            <= i_fifo_data_out when (i_action_finished = '0' or i_fifo_data_empty = '0') else (0 => '1',64 => '1',128 => '1',192 => '1',256 => '1',320 => '1',384 => '1',448 => '1',  others => '0');
   s_wr_data_valid      <= s_fifo_data_rd_rq and s_wr_ready and (s_wr_addr_valid or s_addr_read_flag) and (not i_fifo_data_empty or i_action_finished);
   o_fifo_data_rd_rq    <= s_fifo_data_rd_rq and s_wr_ready and (s_wr_addr_valid or s_addr_read_flag) and (not i_fifo_data_empty or i_action_finished);

end axi_master_wr_2;
