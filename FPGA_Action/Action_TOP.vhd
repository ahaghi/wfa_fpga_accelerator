library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use work.WFA_PKG.all;

entity Action_TOP is
   generic (
      -- Parameters of Axi Master Bus Interface AXI_CARD_MEM0 ; to DDR memory
      C_AXI_CARD_MEM0_ID_WIDTH     : integer   := 2;
      C_AXI_CARD_MEM0_ADDR_WIDTH   : integer   := 33;
      C_AXI_CARD_MEM0_DATA_WIDTH   : integer   := 1024;
      C_AXI_CARD_MEM0_AWUSER_WIDTH : integer   := 1;
      C_AXI_CARD_MEM0_ARUSER_WIDTH : integer   := 1;
      C_AXI_CARD_MEM0_WUSER_WIDTH  : integer   := 1;
      C_AXI_CARD_MEM0_RUSER_WIDTH  : integer   := 1;
      C_AXI_CARD_MEM0_BUSER_WIDTH  : integer   := 1;

      -- Parameters of Axi Slave Bus Interface AXI_CTRL_REG
      C_AXI_CTRL_REG_DATA_WIDTH    : integer   := 32;
      C_AXI_CTRL_REG_ADDR_WIDTH    : integer   := 32;

      -- Parameters of Axi Master Bus Interface AXI_HOST_MEM ; to Host memory
      C_AXI_HOST_MEM_ID_WIDTH      : integer   := 2;
      C_AXI_HOST_MEM_ADDR_WIDTH    : integer   := 64;
      C_AXI_HOST_MEM_DATA_WIDTH    : integer   := 1024;
      C_AXI_HOST_MEM_AWUSER_WIDTH  : integer   := 1;
      C_AXI_HOST_MEM_ARUSER_WIDTH  : integer   := 1;
      C_AXI_HOST_MEM_WUSER_WIDTH   : integer   := 1;
      C_AXI_HOST_MEM_RUSER_WIDTH   : integer   := 1;
      C_AXI_HOST_MEM_BUSER_WIDTH   : integer   := 1;
      INT_BITS                     : integer   := 3;
      CONTEXT_BITS                 : integer   := 8
   );
   port (
      action_clk                   : in std_logic;
      action_rst_n                 : in std_logic;
      int_req_ack                  : in std_logic;
      int_req                      : out std_logic;
      int_src                      : out std_logic_vector(INT_BITS-1 DOWNTO 0);
      int_ctx                      : out std_logic_vector(CONTEXT_BITS-1 DOWNTO 0);
              
      -- Ports of Axi Slave Bus Interface AXI_CTRL_REG
      axi_ctrl_reg_awaddr          : in std_logic_vector(C_AXI_CTRL_REG_ADDR_WIDTH-1 downto 0);
      -- axi_ctrl_reg_awprot       : in std_logic_vector(2 downto 0);
      axi_ctrl_reg_awvalid         : in std_logic;
      axi_ctrl_reg_awready         : out std_logic;
      axi_ctrl_reg_wdata           : in std_logic_vector(C_AXI_CTRL_REG_DATA_WIDTH-1 downto 0);
      axi_ctrl_reg_wstrb           : in std_logic_vector((C_AXI_CTRL_REG_DATA_WIDTH/8)-1 downto 0);
      axi_ctrl_reg_wvalid          : in std_logic;
      axi_ctrl_reg_wready          : out std_logic;
      axi_ctrl_reg_bresp           : out std_logic_vector(1 downto 0);
      axi_ctrl_reg_bvalid          : out std_logic;
      axi_ctrl_reg_bready          : in std_logic;
      axi_ctrl_reg_araddr          : in std_logic_vector(C_AXI_CTRL_REG_ADDR_WIDTH-1 downto 0);
      -- axi_ctrl_reg_arprot       : in std_logic_vector(2 downto 0);
      axi_ctrl_reg_arvalid         : in std_logic;
      axi_ctrl_reg_arready         : out std_logic;
      axi_ctrl_reg_rdata           : out std_logic_vector(C_AXI_CTRL_REG_DATA_WIDTH-1 downto 0);
      axi_ctrl_reg_rresp           : out std_logic_vector(1 downto 0);
      axi_ctrl_reg_rvalid          : out std_logic;
      axi_ctrl_reg_rready          : in std_logic;

      -- Ports of Axi Master Bus Interface AXI_HOST_MEM
      -- to HOST memory
      axi_host_mem_awaddr          : out std_logic_vector(C_AXI_HOST_MEM_ADDR_WIDTH-1 downto 0);
      axi_host_mem_awlen           : out std_logic_vector(7 downto 0);
      axi_host_mem_awsize          : out std_logic_vector(2 downto 0);
      axi_host_mem_awburst         : out std_logic_vector(1 downto 0);
      axi_host_mem_awlock          : out std_logic_vector(1 downto 0);
      axi_host_mem_awcache         : out std_logic_vector(3 downto 0);
      axi_host_mem_awprot          : out std_logic_vector(2 downto 0);
      axi_host_mem_awregion        : out std_logic_vector(3 downto 0);
      axi_host_mem_awqos           : out std_logic_vector(3 downto 0);
      axi_host_mem_awvalid         : out std_logic;
      axi_host_mem_awready         : in std_logic;
      axi_host_mem_wdata           : out std_logic_vector(C_AXI_HOST_MEM_DATA_WIDTH-1 downto 0);
      axi_host_mem_wstrb           : out std_logic_vector(C_AXI_HOST_MEM_DATA_WIDTH/8-1 downto 0);
      axi_host_mem_wlast           : out std_logic;
      axi_host_mem_wvalid          : out std_logic;
      axi_host_mem_wready          : in std_logic;
      axi_host_mem_bresp           : in std_logic_vector(1 downto 0);
      axi_host_mem_bvalid          : in std_logic;
      axi_host_mem_bready          : out std_logic;
      axi_host_mem_araddr          : out std_logic_vector(C_AXI_HOST_MEM_ADDR_WIDTH-1 downto 0);
      axi_host_mem_arlen           : out std_logic_vector(7 downto 0);
      axi_host_mem_arsize          : out std_logic_vector(2 downto 0);
      axi_host_mem_arburst         : out std_logic_vector(1 downto 0);
      axi_host_mem_arlock          : out std_logic_vector(1 downto 0);
      axi_host_mem_arcache         : out std_logic_vector(3 downto 0);
      axi_host_mem_arprot          : out std_logic_vector(2 downto 0);
      axi_host_mem_arregion        : out std_logic_vector(3 downto 0);
      axi_host_mem_arqos           : out std_logic_vector(3 downto 0);
      axi_host_mem_arvalid         : out std_logic;
      axi_host_mem_arready         : in std_logic;
      axi_host_mem_rdata           : in std_logic_vector(C_AXI_HOST_MEM_DATA_WIDTH-1 downto 0);
      axi_host_mem_rresp           : in std_logic_vector(1 downto 0);
      axi_host_mem_rlast           : in std_logic;
      axi_host_mem_rvalid          : in std_logic;
      axi_host_mem_rready          : out std_logic;
      --axi_host_mem_error         : out std_logic;
      axi_host_mem_arid            : out std_logic_vector(C_AXI_HOST_MEM_ID_WIDTH-1 downto 0);
      axi_host_mem_aruser          : out std_logic_vector(C_AXI_HOST_MEM_ARUSER_WIDTH-1 downto 0);
      axi_host_mem_awid            : out std_logic_vector(C_AXI_HOST_MEM_ID_WIDTH-1 downto 0);
      axi_host_mem_awuser          : out std_logic_vector(C_AXI_HOST_MEM_AWUSER_WIDTH-1 downto 0);
      axi_host_mem_bid             : in std_logic_vector(C_AXI_HOST_MEM_ID_WIDTH-1 downto 0);
      axi_host_mem_buser           : in std_logic_vector(C_AXI_HOST_MEM_BUSER_WIDTH-1 downto 0);
      axi_host_mem_rid             : in std_logic_vector(C_AXI_HOST_MEM_ID_WIDTH-1 downto 0);
      axi_host_mem_ruser           : in std_logic_vector(C_AXI_HOST_MEM_RUSER_WIDTH-1 downto 0);
      axi_host_mem_wuser           : out std_logic_vector(C_AXI_HOST_MEM_WUSER_WIDTH-1 downto 0)
);
end Action_TOP;

architecture Action_TOP of Action_TOP is

   
   type ACTION_STATES is(st_Action_idle,
                         st_Action_read_start,
                         st_Action_Wait_Done
                         );                     
                                 

   signal Action_State                 : ACTION_STATES;

   signal s_dma_axi_write_start        : std_logic;
   signal s_dma_axi_write_idle         : std_logic; 
   signal s_dma_axi_write_done         : std_logic; 
   signal s_dma_axi_read_idle          : std_logic; 
   signal s_dma_axi_read_done          : std_logic; 
   signal s_dma_axi_read_start         : std_logic; 
   
   signal s_src_addr                   : std_logic_vector(63 downto 0);
   signal s_dst_addr                   : std_logic_vector(63 downto 0);
   signal s_src_addr_h                 : std_logic_vector(31 downto 0);
   signal s_src_addr_l                 : std_logic_vector(31 downto 0);
   signal s_dst_addr_h                 : std_logic_vector(31 downto 0);
   signal s_dst_addr_l                 : std_logic_vector(31 downto 0);

   signal s_src_data_size_h            : std_logic_vector(31 downto 0); 
   signal s_src_data_size_l            : std_logic_vector(31 downto 0); 
   signal s_dst_data_size_h            : std_logic_vector(31 downto 0); 
   signal s_dst_data_size_l            : std_logic_vector(31 downto 0);

   signal s_wr_data_size               : std_logic_vector(63 downto 0);
   signal s_rd_data_size               : std_logic_vector(63 downto 0);
   signal s_dst_size_KG                : std_logic_vector(63 downto 0);
   signal s_dst_size_HI                : std_logic_vector(63 downto 0);
   signal s_dst_size                   : std_logic_vector(63 downto 0);

   signal s_rd_burst_num               : std_logic_vector(31 downto 0);
   signal s_wr_burst_num               : std_logic_vector(31 downto 0);
   
   signal s_transfer_type              : std_logic_vector(31 downto 0);
   signal s_operation                  : std_logic_vector( 2 downto 0);
   
   signal s_input_fifo_Din             : std_logic_vector(1023 downto 0);   
   signal s_input_fifo_Dout            : std_logic_vector(1023 downto 0);      
   --signal s_input_fifo_Dout2           : std_logic_vector(511 downto 0);      
   signal s_output_fifo_Din            : std_logic_vector(1023 downto 0);   
   signal s_output_fifo_Din_KG         : std_logic_vector(511 downto 0);   
   signal s_output_fifo_Din_HI         : std_logic_vector(511 downto 0);   
   signal s_output_fifo_Din_HI_8       : std_logic_vector(511 downto 0);   
   signal s_output_fifo_Din_HI_64      : std_logic_vector(511 downto 0);   
   signal s_output_fifo_Dout           : std_logic_vector(1023 downto 0);   
   signal s_output_fifo_Data_cnt       : std_logic_vector(  8 downto 0);   
   signal s_input_fifo_wr_rq           : std_logic;  
   signal s_input_fifo_rd_rq           : std_logic;  
   signal s_input_fifo_rd_rq_KG        : std_logic;  
   signal s_input_fifo_rd_rq_HI        : std_logic;  
   signal s_input_fifo_wr_full         : std_logic;  
   signal s_input_fifo_rd_empty        : std_logic;  
   signal s_output_fifo_wr_rq          : std_logic;  
   signal s_output_fifo_wr_rq_KG       : std_logic;  
   signal s_output_fifo_wr_rq_HI       : std_logic;  
   signal s_output_fifo_rd_rq          : std_logic;  
   signal s_output_fifo_wr_full        : std_logic;  
   signal s_output_fifo_rd_empty       : std_logic;  
   signal s_end_of_reads               : std_logic;  
   signal s_start_action               : std_logic;  
   signal s_start_kmer_gen             : std_logic;  
   signal s_start_Histo                : std_logic;  
   signal s_action_finished            : std_logic;  
   signal s_kmers_finished             : std_logic;  
   signal s_Histo_finished             : std_logic;  
   signal s_fifo_rst                   : std_logic;   

   signal s_state_is_idle_kmer_gen     : std_logic; 
   signal s_state_is_idle_rd_provider  : std_logic; 
   signal s_state_is_idle_axi_wr       : std_logic; 
   signal s_state_is_idle_axi_rd       : std_logic; 
   signal s_status                     : std_logic_vector(15 downto 0); 

   signal app_start                    : std_logic;
   signal app_done                     : std_logic;
   signal app_ready                    : std_logic;
   signal app_idle                     : std_logic;   
   signal int_enable                   : std_logic;
   signal s_axi_ctrl_rst_n             : std_logic;
   signal s_rst_n                      : std_logic;
   signal action_clk_225               : std_logic;
   signal action_clk_200               : std_logic;
                   
   signal s_Context_ID                 : std_logic_vector(31 downto 0);
   signal test_cntr_wr                 : integer;
   signal test_cntr_rd                 : integer;

   signal s_text_seq                   : Array_Mers_k;   
   signal s_patrn_seq                  : Array_Mers_k;        
   signal s_patrn_len                  : std_logic_vector((8*4)-1 downto 0);               
   signal s_text_len                   : std_logic_vector((8*4)-1 downto 0);                                                  
   signal s_data_ID_in                 : std_logic_vector((8*8)-1 downto 0);                                         
   signal s_start_cmp                  : Array_1bit;                                      
   signal s_distance                   : Array_distance;                                 
   signal s_k_max                      : Array_distance;                                 
   signal s_m_wavefront_ext            : Array_Offset;                                 
   signal s_cmp_busy                   : Array_1bit;                                                                
   signal s_data_ID_out                : Array_ID;                                     
   signal s_comp_valid                 : Array_1bit;                               
   signal s_data_read                  : Array_1bit;                               
   signal s_Back_Trace                 : Array_64bit;                               
   signal Debug_s                      : Debug_t;           

begin

    int_ctx       <= s_Context_ID(CONTEXT_BITS - 1 downto 0);
    int_src       <= (others => '0');--"00";
    int_req       <= '0'; -- intrupt request
    s_rst_n       <= s_axi_ctrl_rst_n and action_rst_n; 
          
-- Instantiation of Axi Bus Interface AXI_CTRL_REG
action_axi_slave_inst : entity work.action_axi_slave
   generic map (
      C_S_AXI_DATA_WIDTH      => C_AXI_CTRL_REG_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH      => C_AXI_CTRL_REG_ADDR_WIDTH
   )
   port map (
      o_int_enable            => int_enable,
      i_Action_Type           => x"1000_000A",  -- action type
      i_Action_VER            => x"0000_0012",  -- action version
      o_Context_ID            => s_Context_ID,

      o_app_start             => app_start,
      i_app_done              => app_done,
      i_app_ready             => '0',           -- app_ready,
      i_app_idle              => app_idle,
      o_axi_ctrl_rst_n        => s_axi_ctrl_rst_n,
      i_status                => (others => '0'), --s_status,

      o_src_addr_h            => s_src_addr_h,
      o_src_addr_l            => s_src_addr_l,
      o_src_data_size_h       => s_src_data_size_h,
      o_src_data_size_l       => s_src_data_size_l,
      o_dst_addr_h            => s_dst_addr_h,
      o_dst_addr_l            => s_dst_addr_l,
      o_dst_data_size_h       => s_dst_data_size_h,
      o_dst_data_size_l       => s_dst_data_size_l,
      i_dst_data_size_h       => s_dst_size(63 downto 32),
      i_dst_data_size_l       => s_dst_size(31 downto 0),
      o_rd_burst_num          => s_rd_burst_num,    
      o_wr_burst_num          => s_wr_burst_num,    
      o_transfer_type         => s_transfer_type,   
      
      -- User ports ends
      S_AXI_ACLK              => action_clk,
      S_AXI_ARESETN           => action_rst_n,
      S_AXI_AWADDR            => axi_ctrl_reg_awaddr,
      -- S_AXI_AWPROT         => axi_ctrl_reg_awprot,
      S_AXI_AWVALID           => axi_ctrl_reg_awvalid,
      S_AXI_AWREADY           => axi_ctrl_reg_awready,
      S_AXI_WDATA             => axi_ctrl_reg_wdata,
      S_AXI_WSTRB             => axi_ctrl_reg_wstrb,
      S_AXI_WVALID            => axi_ctrl_reg_wvalid,
      S_AXI_WREADY            => axi_ctrl_reg_wready,
      S_AXI_BRESP             => axi_ctrl_reg_bresp,
      S_AXI_BVALID            => axi_ctrl_reg_bvalid,
      S_AXI_BREADY            => axi_ctrl_reg_bready,
      S_AXI_ARADDR            => axi_ctrl_reg_araddr,
      -- S_AXI_ARPROT         => axi_ctrl_reg_arprot,
      S_AXI_ARVALID           => axi_ctrl_reg_arvalid,
      S_AXI_ARREADY           => axi_ctrl_reg_arready,
      S_AXI_RDATA             => axi_ctrl_reg_rdata,
      S_AXI_RRESP             => axi_ctrl_reg_rresp,
      S_AXI_RVALID            => axi_ctrl_reg_rvalid,
      S_AXI_RREADY            => axi_ctrl_reg_rready
   );

-- Instantiation of Axi Bus Interface AXI_HOST_MEM
action_dma_axi_master_rd_inst : entity work.axi_master_rd
   generic map (
      C_M_AXI_ID_WIDTH        => C_AXI_HOST_MEM_ID_WIDTH,
      C_M_AXI_ADDR_WIDTH      => C_AXI_HOST_MEM_ADDR_WIDTH,
      C_M_AXI_DATA_WIDTH      => C_AXI_HOST_MEM_DATA_WIDTH,
      C_M_AXI_ARUSER_WIDTH    => C_AXI_HOST_MEM_ARUSER_WIDTH,
      C_M_AXI_RUSER_WIDTH     => C_AXI_HOST_MEM_RUSER_WIDTH
   )
   port map (

      i_num_of_burst_rd       => to_integer(unsigned(s_rd_burst_num)),
      i_axi_read_start        => s_dma_axi_read_start, 
      o_axi_read_idle         => s_dma_axi_read_idle, 
      o_axi_read_done         => s_dma_axi_read_done, 
      i_src_addr              => s_src_addr, 
      i_data_size             => s_rd_data_size, 
      o_fifo_data_in          => s_input_fifo_Din, 
      o_fifo_wr_rq            => s_input_fifo_wr_rq, 
      i_fifo_wr_full          => s_input_fifo_wr_full,
      i_rd_context_id         => s_Context_ID(C_AXI_HOST_MEM_ARUSER_WIDTH - 1 downto 0),
      o_state_is_idle         => s_state_is_idle_axi_rd,
      
      M_AXI_ACLK              => action_clk,
      M_AXI_ARESETN           => s_rst_n,--action_rst_n,
      M_AXI_ARID              => axi_host_mem_arid,
      M_AXI_ARADDR            => axi_host_mem_araddr,
      M_AXI_ARLEN             => axi_host_mem_arlen,
      M_AXI_ARSIZE            => axi_host_mem_arsize,
      M_AXI_ARBURST           => axi_host_mem_arburst,
      M_AXI_ARLOCK            => axi_host_mem_arlock,
      M_AXI_ARCACHE           => axi_host_mem_arcache,
      M_AXI_ARPROT            => axi_host_mem_arprot,
      M_AXI_ARQOS             => axi_host_mem_arqos,
      M_AXI_ARUSER            => axi_host_mem_aruser,
      M_AXI_ARVALID           => axi_host_mem_arvalid,
      M_AXI_ARREADY           => axi_host_mem_arready,
      M_AXI_RID               => axi_host_mem_rid,
      M_AXI_RDATA             => axi_host_mem_rdata,
      M_AXI_RRESP             => axi_host_mem_rresp,
      M_AXI_RLAST             => axi_host_mem_rlast,
      M_AXI_RUSER             => axi_host_mem_ruser,
      M_AXI_RVALID            => axi_host_mem_rvalid,
      M_AXI_RREADY            => axi_host_mem_rready
   );

action_dma_axi_master_wr_inst : entity work.axi_master_wr_2
   generic map (
      C_M_AXI_ID_WIDTH        => C_AXI_HOST_MEM_ID_WIDTH,
      C_M_AXI_ADDR_WIDTH      => C_AXI_HOST_MEM_ADDR_WIDTH,
      C_M_AXI_DATA_WIDTH      => C_AXI_HOST_MEM_DATA_WIDTH,
      C_M_AXI_AWUSER_WIDTH    => C_AXI_HOST_MEM_AWUSER_WIDTH,
      C_M_AXI_WUSER_WIDTH     => C_AXI_HOST_MEM_WUSER_WIDTH,
      C_M_AXI_BUSER_WIDTH     => C_AXI_HOST_MEM_BUSER_WIDTH
   )
   port map (
   
      i_num_of_burst_wr       => to_integer(unsigned(s_wr_burst_num)),
      i_axi_write_start       => s_dma_axi_write_start, 
      o_axi_write_idle        => s_dma_axi_write_idle, 
      o_axi_write_done        => s_dma_axi_write_done, 
      i_dst_addr              => s_dst_addr, 
      i_fifo_data_out         => s_output_fifo_Dout, 
      i_fifo_data_empty       => s_output_fifo_rd_empty,
      o_fifo_data_rd_rq       => s_output_fifo_rd_rq,
      i_wr_context_id         => s_Context_ID(C_AXI_HOST_MEM_AWUSER_WIDTH - 1 downto 0),
      i_action_finished       => s_action_finished,
      o_state_is_idle         => s_state_is_idle_axi_wr,
      
      M_AXI_ACLK              => action_clk,
      M_AXI_ARESETN           => s_rst_n, --action_rst_n,
      M_AXI_AWID              => axi_host_mem_awid,
      M_AXI_AWADDR            => axi_host_mem_awaddr,
      M_AXI_AWLEN             => axi_host_mem_awlen,
      M_AXI_AWSIZE            => axi_host_mem_awsize,
      M_AXI_AWBURST           => axi_host_mem_awburst,
      M_AXI_AWLOCK            => axi_host_mem_awlock,
      M_AXI_AWCACHE           => axi_host_mem_awcache,
      M_AXI_AWPROT            => axi_host_mem_awprot,
      M_AXI_AWQOS             => axi_host_mem_awqos,
      M_AXI_AWUSER            => axi_host_mem_awuser,
      M_AXI_AWVALID           => axi_host_mem_awvalid,
      M_AXI_AWREADY           => axi_host_mem_awready,
      M_AXI_WDATA             => axi_host_mem_wdata,
      M_AXI_WSTRB             => axi_host_mem_wstrb,
      M_AXI_WLAST             => axi_host_mem_wlast,
      M_AXI_WUSER             => axi_host_mem_wuser,
      M_AXI_WVALID            => axi_host_mem_wvalid,
      M_AXI_WREADY            => axi_host_mem_wready,
      M_AXI_BID               => axi_host_mem_bid,
      M_AXI_BRESP             => axi_host_mem_bresp,
      M_AXI_BUSER             => axi_host_mem_buser,
      M_AXI_BVALID            => axi_host_mem_bvalid,
      M_AXI_BREADY            => axi_host_mem_bready
   );


   s_fifo_rst <= not s_rst_n;

   process(action_clk, s_rst_n) is
   begin
      if(s_rst_n = '0') then
      
         app_done                <= '0';
         app_idle                <= '0'; 
         Action_State            <= st_Action_idle;
         s_src_addr              <= (others => '0');
         s_dst_addr              <= (others => '0');
         s_rd_data_size          <= (others => '0');
         s_wr_data_size          <= (others => '0');
         s_operation             <= (others => '0');
         s_dma_axi_read_start    <= '0';
         s_dma_axi_write_start   <= '0';
         s_start_action          <= '0';
                  
      elsif(rising_edge(action_clk)) then
         
         app_done    <= '0';

         
         case Action_State is
         
         --========================================================
         when st_Action_idle =>
            app_idle            <= '1'; 

            if (app_start = '1') then 
               app_done         <= '0';        
               app_idle         <= '0';  
               Action_State     <= st_Action_read_start;
               s_src_addr       <= s_src_addr_h & s_src_addr_l;
               s_dst_addr       <= s_dst_addr_h & s_dst_addr_l;
               s_rd_data_size   <= s_src_data_size_h & s_src_data_size_l;
               s_wr_data_size   <= s_dst_data_size_h & s_dst_data_size_l;
               s_operation      <= s_transfer_type(2 downto 0);
               s_start_action   <= '0';
            else
               Action_State     <= st_Action_idle;
            end if;
         
         --========================================================  
         when st_Action_read_start =>
            s_dma_axi_read_start       <= '1';
            if (s_dma_axi_read_idle = '0') then
               s_dma_axi_read_start    <= '0';
               s_start_action          <= '1';
               s_dma_axi_write_start   <= '1';
               Action_State            <= st_Action_Wait_Done;
            else
               Action_State            <= st_Action_read_start;
            end if;   

         --========================================================  
         when st_Action_Wait_Done =>
            if (s_dma_axi_write_idle = '0') then
               s_dma_axi_write_start <= '0';
            end if;   
            if (s_dma_axi_read_done = '1' and s_dma_axi_write_done = '1' and s_action_finished = '1') then
               app_done          <= '1';
               Action_State      <= st_Action_idle;
            else
               Action_State      <= st_Action_Wait_Done;
            end if;
         --========================================================  
         when others =>
            null;
            
         end case;   
         
      end if;
   end process;   


   --u_fifo_512x64_Input : entity work.fifo_512x64
   --  PORT MAP (
   --    CLK                       => action_clk,
   --    SRST                      => s_fifo_rst,
   --    DIN                       => s_input_fifo_Din,
   --    WR_EN                     => s_input_fifo_wr_rq,
   --    RD_EN                     => s_input_fifo_rd_rq,
   --    DOUT                      => s_input_fifo_Dout,
   --    FULL                      => s_input_fifo_wr_full,
   --    EMPTY                     => s_input_fifo_rd_empty,
   --    DATA_COUNT                => open
   --  );

   u_XPM_FIFO_1024x256_Input: ENTITY work.XPM_FIFO_1024x256
      GENERIC MAP( G_FIFO_MEMORY_TYPE => "block")
      PORT MAP(
         CLK                       => action_clk,
         SRST                      => s_fifo_rst,
         DIN                       => s_input_fifo_Din,
         WR_EN                     => s_input_fifo_wr_rq,
         RD_EN                     => s_input_fifo_rd_rq,
         DOUT                      => s_input_fifo_Dout,
         FULL                      => s_input_fifo_wr_full,
         EMPTY                     => s_input_fifo_rd_empty,
         DATA_COUNT                => open
      );
      
   --process(s_input_fifo_Dout) -- This process reveses the order of bytes.
   --begin        
   --   for i in 0 to 63 loop
   --   	 s_input_fifo_Dout2((512-(i*8)-1) downto 512-(i*8)-8)  <= s_input_fifo_Dout(((i*8)+(8-1)) downto i*8);
   --   end loop;
   --end process; 
      
      
   u_Data_Extractor: entity work.Data_Extractor_v2 
      port map (
         i_clk                   => action_clk,
         i_rst_n                 => s_rst_n,
         i_fifo_empty            => s_input_fifo_rd_empty,
         o_fifo_rd_rq            => s_input_fifo_rd_rq,
         i_fifo_Dout             => s_input_fifo_Dout,
         i_start                 => s_start_action,
         i_read_done             => s_dma_axi_read_done,
         o_end_of_file           => s_kmers_finished,
         o_state_is_idle         => open,
         i_cmp_busy              => s_cmp_busy,
         o_data_ID               => s_data_ID_in,
         o_patrn_len             => s_patrn_len,    
         o_text_len              => s_text_len,    
         o_patrn_seq             => s_patrn_seq, 
         o_text_seq              => s_text_seq,
         o_start_cmp             => s_start_cmp,
         i_output_fifo_Data_cnt  => s_output_fifo_Data_cnt
      );

   GEN_Aligner: 
   for i in 0 to c_cores_num-1 generate
      u_affine_gap_align: entity work.affine_gap_align 
         port map (
            i_clk                => action_clk,
            i_rst_n              => s_rst_n,
            i_Seq1_Mers          => s_text_seq,  -- Horizontal -> Text
            i_Seq2_Mers          => s_patrn_seq, -- Vertical   -> Pattern
            i_patrn_len          => s_patrn_len,
            i_text_len           => s_text_len,
            i_seq_ID             => s_data_ID_in,
            i_start              => s_start_cmp(i),
            o_distance           => s_distance(i),
            o_k_max              => s_k_max(i),
            --o_m_wavefront_ext    => s_m_wavefront_ext,
            --Debug_s              => Debug_s,
            i_data_read          => s_data_read(i),
            o_Dist_busy          => s_cmp_busy(i),
            o_seq_ID             => s_data_ID_out(i),
            o_Dist_valid         => s_comp_valid(i),
            o_Back_Trace         => s_Back_Trace(i)
         );
   end generate GEN_Aligner; 

   u_Controller: entity work.Controller_v2 
      port map (
         i_clk                   => action_clk,
         i_rst_n                 => s_rst_n,
         i_busy                  => s_cmp_busy,
         i_kmers_finished        => s_kmers_finished,
         i_distance              => s_distance,
         i_k_max                 => s_k_max,
         o_data_read             => s_data_read,
         --i_m_wavefront_ext       => s_m_wavefront_ext,
         i_dist_valid            => s_comp_valid,
         i_Back_Trace            => s_Back_Trace,
         i_seq_ID                => s_data_ID_out,
         i_fifo_near_full        => '0',
         o_fifo_wr_rq            => s_output_fifo_wr_rq,
         o_fifo_Din              => s_output_fifo_Din, 
         o_finished              => s_action_finished,
         o_return_data_size      => s_dst_size
      );  
      
   --u_Controller: entity work.Controller_dbg --_v2 
   --   port map (
   --      i_clk                   => action_clk,
   --      i_rst_n                 => s_rst_n,
   --      i_busy                  => s_cmp_busy,
   --      i_kmers_finished        => s_kmers_finished,
   --      i_distance              => s_distance,
   --      i_k_max                 => s_k_max,
   --      Debug_s                 => Debug_s,
   --      --i_m_wavefront_ext       => s_m_wavefront_ext,
   --      i_dist_valid            => s_comp_valid,
   --      i_seq_ID                => s_data_ID_out,
   --      i_fifo_near_full        => '0',
   --      o_fifo_wr_rq            => s_output_fifo_wr_rq,
   --      o_fifo_Din              => s_output_fifo_Din, 
   --      o_finished              => s_action_finished,
   --      o_return_data_size      => s_dst_size
   --   );      
         
   --u_fifo_512x64_Output : entity work.fifo_512x64
   --   PORT MAP (
   --      CLK                     => action_clk,
   --      SRST                    => s_fifo_rst,
   --      DIN                     => s_output_fifo_Din,
   --      WR_EN                   => s_output_fifo_wr_rq,
   --      RD_EN                   => s_output_fifo_rd_rq,
   --      DOUT                    => s_output_fifo_Dout,
   --      FULL                    => s_output_fifo_wr_full,
   --      EMPTY                   => s_output_fifo_rd_empty,
   --      DATA_COUNT              => s_output_fifo_Data_cnt
   --  );  
      
   u_XPM_FIFO_1024x256_Output: ENTITY work.XPM_FIFO_1024x256
      GENERIC MAP( G_FIFO_MEMORY_TYPE => "block")
      PORT MAP(
         CLK                     => action_clk,
         SRST                    => s_fifo_rst,
         DIN                     => s_output_fifo_Din,
         WR_EN                   => s_output_fifo_wr_rq,
         RD_EN                   => s_output_fifo_rd_rq,
         DOUT                    => s_output_fifo_Dout,
         FULL                    => s_output_fifo_wr_full,
         EMPTY                   => s_output_fifo_rd_empty,
         DATA_COUNT              => s_output_fifo_Data_cnt
      );
            
end Action_TOP;
