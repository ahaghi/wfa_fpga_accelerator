library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.WFA_PKG.all;

entity Data_Extractor_v2 is
   port (
      i_clk                   : in        std_logic; 
      i_rst_n                 : in        std_logic; 
      i_fifo_empty            : in        std_logic; 
      o_fifo_rd_rq            : buffer    std_logic; 
      i_fifo_Dout             : in        std_logic_vector(1023 downto 0); 
      i_start                 : in        std_logic; 
      i_read_done             : in        std_logic;
      o_end_of_file           : out       std_logic;
      o_state_is_idle         : out       std_logic;
      i_cmp_busy              : in        Array_1bit;     
      o_data_ID               : out       std_logic_vector((8*8)-1 downto 0); 
      o_patrn_len             : out       std_logic_vector((8*4)-1 downto 0);      
      o_text_len              : out       std_logic_vector((8*4)-1 downto 0);      
      o_patrn_seq             : out       Array_Mers_k;      
      o_text_seq              : out       Array_Mers_k;      
      o_start_cmp             : out       Array_1bit;
      i_output_fifo_Data_cnt  : in        std_logic_vector(8 downto 0)
   );
end Data_Extractor_v2;

architecture Data_Extractor_v2 of Data_Extractor_v2 is

   function MAPPER (Seq : std_logic_vector) return Array_Mers is
      variable Seq_Mers : Array_Mers := (others => (others => '0'));
   begin
      for i in 0 to ((Seq'length)/8)-1 loop
         case Seq((i+1)*8-1 downto (i+1)*8-8) is
         when X"41" => --65   => -- A
            Seq_Mers(i) := "00";
         when X"43" => --67   => -- C
            Seq_Mers(i) := "01";
         when X"47" => --71   => -- G
            Seq_Mers(i) := "10"; 
         when others => -- 84 => -- T
            Seq_Mers(i) := "11";
         end case; 
      end loop; 
   return Seq_Mers;
   end MAPPER;

   function MAPPER2 (Seq_Mers : Array_Mers) return Array_Mers_k is
      variable Seq_Mers_k : Array_Mers_k := (others => (others => '0'));
   begin
      for i in 0 to c_k_num loop
            Seq_Mers_k(i) := Seq_Mers(i*c_xor_len+0) & Seq_Mers(i*c_xor_len+1) & Seq_Mers(i*c_xor_len+2) & Seq_Mers(i*c_xor_len+3) & Seq_Mers(i*c_xor_len+4) & Seq_Mers(i*c_xor_len+5) & Seq_Mers(i*c_xor_len+6) & Seq_Mers(i*c_xor_len+7);
      end loop; 
   return Seq_Mers_k;
   end MAPPER2;
                      
	type	States is(st_idle,
                   st_process,
                   st_finish,
                   st_assigner
                  );

   signal state            : States;                       
   signal s_fifo_rd_rq     : std_logic;                                          
   signal s_data_cntr      : integer range 0 to 7;
   signal s_priority       : integer range 0 to 7;
   signal s_start          : std_logic;
   signal s_data_ID        : std_logic_vector(8*8-1   downto 0);
   signal s_patrn_len      : std_logic_vector(8*4-1   downto 0);
   signal s_text_len       : std_logic_vector(8*4-1   downto 0);
   signal s_patrn_seq      : std_logic_vector(8*150-1 downto 0);
   signal s_text_seq       : std_logic_vector(8*150-1 downto 0);
   signal s_next_data      : std_logic_vector(8*(64)-1 downto 0);
   
begin
 
   o_fifo_rd_rq   <= s_fifo_rd_rq and not i_fifo_empty; 
           
   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         
         s_data_cntr       <= 0;
         s_priority        <= 0;
         state             <= st_idle;
         o_state_is_idle   <= '1';  
         s_start           <= '0';
         s_fifo_rd_rq      <= '0';
         o_end_of_file     <= '0';

         s_data_ID         <= (others => '0'); 
         s_patrn_len       <= (others => '0');
         s_text_len        <= (others => '0');
         s_patrn_seq       <= (others => '0');
         s_text_seq        <= (others => '0');
         s_next_data       <= (others => '0');

         o_data_ID         <= (others => '0');
         o_patrn_len       <= (others => '0');
         o_text_len        <= (others => '0');
         o_patrn_seq       <= (others => (others => '0'));
         o_text_seq        <= (others => (others => '0'));
         o_start_cmp       <= (others => '0');

      elsif(rising_edge(i_clk)) then
         
         o_start_cmp  <= (others => '0');
         s_start      <= i_start;
            
         case state is
            
         when st_idle =>  
            o_state_is_idle      <= '1';  
            if (i_start = '1' and s_start = '0') then
               state             <= st_process;
               s_fifo_rd_rq      <= '1';      
               o_end_of_file     <= '0';
               s_data_cntr       <=  0 ;
               s_priority        <=  0 ;
               o_state_is_idle   <= '0';
            end if;
               
         --============================================================================================================================================================================== 
         when st_process =>
            
            s_fifo_rd_rq   <= '1';      
                               
            if (o_fifo_rd_rq = '1') then 
               case s_data_cntr is
               when 0      =>
                  s_data_cntr <= 1;
                  s_data_ID   <= i_fifo_Dout(8*8-1 downto 0);
                  s_patrn_len <= i_fifo_Dout(8*(8+4)-1 downto 8*8);
                  s_text_len  <= i_fifo_Dout(8*(8+4+4)-1 downto 8*(8+4));
                  s_patrn_seq(8*(112)-1 downto 0) <= i_fifo_Dout(8*(128)-1 downto 8*(8+4+4));
               when 1      =>
                  s_data_cntr <= 2;
                  s_patrn_seq(8*(38+112)-1 downto 8*(112))  <= i_fifo_Dout(8*(38)-1 downto 0);
                  s_text_seq(8*(90)-1 downto 0)             <= i_fifo_Dout(8*(128)-1 downto 8*(38));
               when 2      =>
                  s_data_cntr  <= 3;
                  s_text_seq(8*(60+90)-1 downto 8*(90))     <= i_fifo_Dout(8*(60)-1 downto 0);
                  s_next_data                               <= i_fifo_Dout(8*(128)-1 downto 8*(64));
                  s_fifo_rd_rq <= '0';      
                  state        <= st_assigner;
               when 3      =>
                  s_data_cntr <= 4;
                  s_data_ID   <= s_next_data(8*8-1 downto 0);
                  s_patrn_len <= s_next_data(8*(8+4)-1 downto 8*8);
                  s_text_len  <= s_next_data(8*(8+4+4)-1 downto 8*(8+4));
                  s_patrn_seq(8*(150)-1 downto 0) <= i_fifo_Dout(8*(102)-1 downto 0) & s_next_data(8*(64)-1 downto 8*(8+4+4));
                  s_text_seq(8*(26)-1 downto 0)   <= i_fifo_Dout(8*(128)-1 downto 8*(102));
               when others => --4      =>
                  s_data_cntr <= 0;
                  s_text_seq(8*(124+26)-1 downto 8*(26))  <= i_fifo_Dout(8*(124)-1 downto 0);
                  s_fifo_rd_rq <= '0';      
                  state        <= st_assigner;
               end case;
            end if;
               
            if (i_fifo_empty = '1' and i_read_done = '1') then   
               state    <= st_finish;
            end if;

         --==============================================================================================================================================================================          
         when st_assigner =>
               
            o_patrn_len  <= s_patrn_len;                
            o_text_len   <= s_text_len;                 
            o_patrn_seq  <= MAPPER2(MAPPER(s_patrn_seq));                
            o_text_seq   <= MAPPER2(MAPPER(s_text_seq));
            o_data_ID    <= s_data_ID;

            if (i_output_fifo_Data_cnt < 186) then
               state              <= st_process;
               s_fifo_rd_rq       <= '1';
               case s_priority is
               when 0 =>
                  s_priority <= 1;
                  if (i_cmp_busy(0) = '0') then
                     o_start_cmp(0)   <= '1';
                  elsif (i_cmp_busy(1) = '0') then
                     o_start_cmp(1)   <= '1'; 
                  elsif (i_cmp_busy(2) = '0') then
                     o_start_cmp(2)   <= '1'; 
                  elsif (i_cmp_busy(3) = '0') then
                     o_start_cmp(3)   <= '1'; 
                  elsif (i_cmp_busy(4) = '0') then
                     o_start_cmp(4)   <= '1'; 
                  elsif (i_cmp_busy(5) = '0') then
                     o_start_cmp(5)   <= '1'; 
                  elsif (i_cmp_busy(6) = '0') then
                     o_start_cmp(6)   <= '1'; 
                  elsif (i_cmp_busy(7) = '0') then
                     o_start_cmp(7)   <= '1'; 
                  elsif (i_cmp_busy(8) = '0') then
                     o_start_cmp(8)   <= '1'; 
                  elsif (i_cmp_busy(9) = '0') then
                     o_start_cmp(9)   <= '1';  
                  else
                     state            <= st_assigner;
                     s_fifo_rd_rq     <= '0'; 
                  end if;
               when 1 =>
                  s_priority <= 2;
                  if (i_cmp_busy(10) = '0') then
                     o_start_cmp(10)  <= '1';
                  elsif (i_cmp_busy(11) = '0') then
                     o_start_cmp(11)  <= '1'; 
                  elsif (i_cmp_busy(12) = '0') then
                     o_start_cmp(12)   <= '1'; 
                  elsif (i_cmp_busy(13) = '0') then
                     o_start_cmp(13)   <= '1'; 
                  elsif (i_cmp_busy(14) = '0') then
                     o_start_cmp(14)   <= '1'; 
                  elsif (i_cmp_busy(15) = '0') then
                     o_start_cmp(15)   <= '1'; 
                  elsif (i_cmp_busy(16) = '0') then
                     o_start_cmp(16)   <= '1'; 
                  elsif (i_cmp_busy(17) = '0') then
                     o_start_cmp(17)   <= '1'; 
                  elsif (i_cmp_busy(18) = '0') then
                     o_start_cmp(18)   <= '1'; 
                  elsif (i_cmp_busy(19) = '0') then
                     o_start_cmp(19)   <= '1'; 
                  else
                     state             <= st_assigner;
                     s_fifo_rd_rq      <= '0';   
                  end if;
               when 2 =>
                  s_priority <= 3;
                  if (i_cmp_busy(20) = '0') then
                     o_start_cmp(20)   <= '1'; 
                  elsif (i_cmp_busy(21) = '0') then
                     o_start_cmp(21)   <= '1'; 
                  elsif (i_cmp_busy(22) = '0') then
                     o_start_cmp(22)   <= '1'; 
                  elsif (i_cmp_busy(23) = '0') then
                     o_start_cmp(23)   <= '1'; 
                  elsif (i_cmp_busy(24) = '0') then
                     o_start_cmp(24)   <= '1'; 
                  elsif (i_cmp_busy(25) = '0') then
                     o_start_cmp(25)   <= '1'; 
                  elsif (i_cmp_busy(26) = '0') then
                     o_start_cmp(26)   <= '1'; 
                  elsif (i_cmp_busy(27) = '0') then
                     o_start_cmp(27)   <= '1'; 
                  elsif (i_cmp_busy(28) = '0') then
                     o_start_cmp(28)   <= '1'; 
                  elsif (i_cmp_busy(29) = '0') then
                     o_start_cmp(29)   <= '1'; 
                  elsif (i_cmp_busy(30) = '0') then
                     o_start_cmp(30)   <= '1'; 
                  else
                     state             <= st_assigner;
                     s_fifo_rd_rq      <= '0';    
                  end if;
               when 3 =>
                  s_priority <= 4;
                  if (i_cmp_busy(31) = '0') then
                     o_start_cmp(31)   <= '1'; 
                  elsif (i_cmp_busy(32) = '0') then
                     o_start_cmp(32)   <= '1'; 
                  elsif (i_cmp_busy(33) = '0') then
                     o_start_cmp(33)   <= '1'; 
                  elsif (i_cmp_busy(34) = '0') then
                     o_start_cmp(34)   <= '1'; 
                  elsif (i_cmp_busy(35) = '0') then
                     o_start_cmp(35)   <= '1'; 
                  elsif (i_cmp_busy(36) = '0') then
                     o_start_cmp(36)   <= '1'; 
                  elsif (i_cmp_busy(37) = '0') then
                     o_start_cmp(37)   <= '1'; 
                  elsif (i_cmp_busy(38) = '0') then
                     o_start_cmp(38)   <= '1'; 
                  elsif (i_cmp_busy(39) = '0') then
                     o_start_cmp(39)   <= '1'; 
                  elsif (i_cmp_busy(40) = '0') then
                     o_start_cmp(40)   <= '1'; 
                  elsif (i_cmp_busy(41) = '0') then
                     o_start_cmp(41)   <= '1'; 
                  else
                     state             <= st_assigner;
                     s_fifo_rd_rq      <= '0';   
                  end if;
               when 4 =>
                  s_priority <= 5;
                  if (i_cmp_busy(42) = '0') then
                     o_start_cmp(42)   <= '1'; 
                  elsif (i_cmp_busy(43) = '0') then
                     o_start_cmp(43)   <= '1'; 
                  elsif (i_cmp_busy(44) = '0') then
                     o_start_cmp(44)   <= '1'; 
                  elsif (i_cmp_busy(45) = '0') then
                     o_start_cmp(45)   <= '1'; 
                  elsif (i_cmp_busy(46) = '0') then
                     o_start_cmp(46)   <= '1'; 
                  elsif (i_cmp_busy(47) = '0') then
                     o_start_cmp(47)   <= '1'; 
                  elsif (i_cmp_busy(48) = '0') then
                     o_start_cmp(48)   <= '1'; 
                  elsif (i_cmp_busy(49) = '0') then
                     o_start_cmp(49)   <= '1'; 
                  elsif (i_cmp_busy(50) = '0') then
                     o_start_cmp(50)   <= '1'; 
                  elsif (i_cmp_busy(51) = '0') then
                     o_start_cmp(51)   <= '1'; 
                  elsif (i_cmp_busy(52) = '0') then
                     o_start_cmp(52)   <= '1'; 
                  else
                     state             <= st_assigner;
                     s_fifo_rd_rq      <= '0';  
                  end if;
               when others => --5 =>
                  s_priority <= 0;
                  if (i_cmp_busy(53) = '0') then
                     o_start_cmp(53)   <= '1'; 
                  elsif (i_cmp_busy(54) = '0') then
                     o_start_cmp(54)   <= '1'; 
                  elsif (i_cmp_busy(55) = '0') then
                     o_start_cmp(55)   <= '1'; 
                  elsif (i_cmp_busy(56) = '0') then
                     o_start_cmp(56)   <= '1'; 
                  elsif (i_cmp_busy(57) = '0') then
                     o_start_cmp(57)   <= '1'; 
                  elsif (i_cmp_busy(58) = '0') then
                     o_start_cmp(58)   <= '1'; 
                  elsif (i_cmp_busy(59) = '0') then
                     o_start_cmp(59)   <= '1'; 
                  elsif (i_cmp_busy(60) = '0') then
                     o_start_cmp(60)   <= '1'; 
                  elsif (i_cmp_busy(61) = '0') then
                     o_start_cmp(61)   <= '1'; 
                  elsif (i_cmp_busy(62) = '0') then
                     o_start_cmp(62)   <= '1'; 
                  elsif (i_cmp_busy(63) = '0') then
                     o_start_cmp(63)   <= '1'; 
                  else
                     state             <= st_assigner;
                     s_fifo_rd_rq      <= '0';   
                  end if;
               end case;          
            else
               state              <= st_assigner;
               s_fifo_rd_rq       <= '0';
            end if;
               
         --==============================================================================================================================================================================        
         when st_finish => 
               o_end_of_file      <= '1'; 
               s_fifo_rd_rq       <= '0';      
               state              <= st_idle;

         --==============================================================================================================================================================================          
         when others =>
            null;

         end case;

      end if;
   end process; 
     
      
end Data_Extractor_v2;         
