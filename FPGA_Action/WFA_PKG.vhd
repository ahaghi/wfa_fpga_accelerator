library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package WFA_PKG is

   constant c_max_seq_len     : integer := 150;
   constant c_xor_len         : integer := 8;
   constant c_k_num           : integer := c_max_seq_len/c_xor_len;
   constant c_cores_num       : integer := 64;
   constant c_k_range         : integer := 32;
   constant c_comp_num        : integer := 8;
   constant c_idx_0_lcl_pos   : integer := -(((c_k_range/c_comp_num)*c_comp_num)-c_k_range);

   type Array_Mers         is array (0 to (c_max_seq_len-1)+c_xor_len) of std_logic_vector(1 downto 0);
   type Array_Mers_k       is array (0 to  c_k_num+3) of std_logic_vector(c_xor_len*2-1 downto 0);

   type Array_Mers_core_k  is array (0 to (c_cores_num-1)) of Array_Mers_k;
   type Array_Mers_core    is array (0 to (c_cores_num-1)) of Array_Mers;
   type Array_1bit         is array (0 to (c_cores_num-1)) of std_logic;
   type Array_Seq_len      is array (0 to (c_cores_num-1)) of std_logic_vector((8*4)-1 downto 0);
   type Array_ID           is array (0 to (c_cores_num-1)) of std_logic_vector((8*8)-1 downto 0);
   type Array_distance     is array (0 to (c_cores_num-1)) of integer range 0 to c_max_seq_len;
   type Array_512bit       is array (0 to (c_cores_num-1)) of std_logic_vector(511 downto 0);
   type Array_1024bit      is array (0 to (c_cores_num-1)) of std_logic_vector(1023 downto 0);
   type Array_128bit       is array (0 to (c_cores_num-1)) of std_logic_vector(127 downto 0);
   type Array_cntr7        is array (0 to (c_cores_num-1)) of integer range 0 to 7;
   type Array_64bit        is array (0 to (c_cores_num-1)) of std_logic_vector(63 downto 0);

   type Array_Offset_comp  is array (0 to c_comp_num-1) of integer range -c_max_seq_len to c_max_seq_len;
   type Array2D_Offset     is array (0 to 5) of Array_Offset_comp;
   type Array_Offset_comp2 is array (0 to c_comp_num+1) of integer range -c_max_seq_len to c_max_seq_len;
   type Array_Offset       is array (-c_k_range-1 to c_k_range+1) of integer range -c_max_seq_len to c_max_seq_len;
   --type Array_Offset       is array (-c_k_range to c_k_range) of integer range 0 to c_max_seq_len;
   type Array_busy         is array (-c_k_range to c_k_range) of std_logic;
   type Array_busy_comp    is array (0 to c_comp_num-1) of std_logic;
   type Array_bt_type      is array (0 to c_comp_num-1) of std_logic_vector(63 downto 0);
   type Array_bt_type3     is array (0 to c_comp_num+1) of std_logic_vector(63 downto 0);
   type Array_bt_type2     is array (-c_k_range-1 to c_k_range+1) of std_logic_vector(63 downto 0);

   type Wavefront_t is record
     isNull     : std_logic;              
     --k_min      : integer range -63 to  0;            
     --k_max      : integer range   0 to 63;            
     offset     : Array_Offset;
   end record Wavefront_t;

   constant mismatch_penalty : integer := 4;
   constant gap_open_penalty : integer := 6;
   constant gap_ext_penalty  : integer := 2;

   type mbt is array (0 to (gap_open_penalty + gap_ext_penalty - 1)) of Array_bt_type2;
   type ibt is array (0 to (gap_ext_penalty - 1)) of Array_bt_type2;
   type dbt is array (0 to (gap_ext_penalty - 1)) of Array_bt_type2;
   type mwavefront is array (0 to (gap_open_penalty + gap_ext_penalty - 1)) of Wavefront_t;
   type iwavefront is array (0 to (gap_ext_penalty - 1))                    of Wavefront_t;
   type dwavefront is array (0 to (gap_ext_penalty - 1))                    of Wavefront_t;
   
   type WavefrontBKT_t is array (0 to 64) of std_logic_vector(64*6 downto 0);

   type Dbg2_t is record
      pos1              : integer range -c_max_seq_len to c_max_seq_len;
      pos2              : integer range -c_max_seq_len to c_max_seq_len;
      Match_Num         : integer range 0 to c_max_seq_len;
      New_Offset        : integer range -2*c_max_seq_len to 2*c_max_seq_len;
   end record Dbg2_t;   
   
   type Dbg2_t_Array is array (0 to (c_comp_num - 1)) of Dbg2_t;

   type Debug_t is record
      update_flag       : std_logic;  
      k_max             : integer range -c_k_range to c_k_range;     
      score             : integer range 0 to c_max_seq_len;
      state             : std_logic_vector(1 downto 0);   
      update_code       : std_logic_vector(1 downto 0);   
      m_wavefront_exti  : Array_Offset_comp;
      m_wavefront_exto  : Array_Offset_comp;
      startpos          : integer range -c_k_range to c_k_range;     
      Dbg2              : Dbg2_t;
   end record Debug_t;

end WFA_PKG;