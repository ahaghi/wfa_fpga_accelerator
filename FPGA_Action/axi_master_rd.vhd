library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity axi_master_rd is
   generic (
      C_M_AXI_ADDR_WIDTH      : integer   := 64;      -- Width of Address Bus
      C_M_AXI_DATA_WIDTH      : integer   := 512;     -- Width of Data Bus
      C_M_AXI_ID_WIDTH        : integer   := 1;       -- Thread ID Width
      C_M_AXI_ARUSER_WIDTH    : integer   := 0;       -- Width of User Read Address Bus
      C_M_AXI_RUSER_WIDTH     : integer   := 0        -- Width of User Read Data Bus
   );
   port (

      i_num_of_burst_rd       : in     integer;  
      i_axi_read_start        : in     std_logic;                      
      o_axi_read_idle         : out    std_logic;                     
      o_axi_read_done         : out    std_logic;                     
      i_src_addr              : in     std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
      i_data_size             : in     std_logic_vector(63 downto 0);         
      o_fifo_data_in          : out    std_logic_vector(C_M_AXI_DATA_WIDTH-1  downto 0);      
      o_fifo_wr_rq            : out    std_logic; 
      i_fifo_wr_full          : in     std_logic; 
      i_rd_context_id         : in     std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
      o_state_is_idle         : out    std_logic;
      
      -- AXI Read Signals ---------------------------------------------------------------      
      M_AXI_ACLK              : in     std_logic;
      M_AXI_ARESETN           : in     std_logic;      
      M_AXI_ARUSER            : out    std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
      M_AXI_ARID              : out    std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
      M_AXI_ARADDR            : out    std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
      M_AXI_ARLEN             : out    std_logic_vector(7 downto 0);
      M_AXI_ARSIZE            : out    std_logic_vector(2 downto 0);
      M_AXI_ARBURST           : out    std_logic_vector(1 downto 0);
      M_AXI_ARLOCK            : out    std_logic_vector(1 downto 0);
      M_AXI_ARCACHE           : out    std_logic_vector(3 downto 0);
      M_AXI_ARPROT            : out    std_logic_vector(2 downto 0);
      M_AXI_ARQOS             : out    std_logic_vector(3 downto 0);
      M_AXI_ARVALID           : out    std_logic;
      M_AXI_ARREADY           : in     std_logic;
      M_AXI_RID               : in     std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
      M_AXI_RDATA             : in     std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
      M_AXI_RRESP             : in     std_logic_vector(1 downto 0);
      M_AXI_RLAST             : in     std_logic;
      M_AXI_RUSER             : in     std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
      M_AXI_RVALID            : in     std_logic;
      M_AXI_RREADY            : out    std_logic
   );
end axi_master_rd;

architecture axi_master_rd of axi_master_rd is


   -- function called clogb2 that returns an integer which has the
   --value of the ceiling of the log base 2

--    function clogb2 (bit_depth : integer) return integer is            
--        variable depth  : integer := bit_depth;                               
--        variable count  : integer := 1;                                       
--     begin                                                                   
--         for clogb2 in 1 to bit_depth loop  -- Works for up to 32 bit integers
--          if (bit_depth <= 2) then                                           
--            count := 1;                                                      
--          else                                                               
--            if(depth <= 1) then                                              
--               count := count;                                                
--             else                                                             
--               depth := depth / 2;                                            
--              count := count + 1;                                            
--             end if;                                                          
--           end if;                                                            
--       end loop;                                                             
--       return(count);                                                         
--     end;

    
    
   function AXI_PORT_WIDTH_to_ARSIZE (AXI_DATA_WIDTH : integer) return std_logic_vector is            
      variable v_axi_arsize  : std_logic_vector(2 downto 0) := "000";                                                                    
   begin                                                                   
      case AXI_DATA_WIDTH is
      
      when 8   =>  
         v_axi_arsize := "000";
         
      when 16  => 
         v_axi_arsize := "001";
         
      when 32  =>  
         v_axi_arsize := "010";
         
      when 64  =>
         v_axi_arsize := "011";       
         
      when 128 =>  
         v_axi_arsize := "100";       
         
      when 256 =>
         v_axi_arsize := "101";       
         
      when 512 =>
         v_axi_arsize := "110";       
         
      when others    => --1024
         v_axi_arsize := "111";       
      
      end case;      
      
      return(v_axi_arsize);                                                         
   end;
    

    function Bytes_per_burst_transfer (Num_of_Burst : integer; AXI_DATA_WIDTH : integer; AXI_ADDR_WIDTH: integer) return std_logic_vector is                               
       variable v_bytes_per_burst_transfer   : std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0) := (others => '0');           
       variable v_axi_data_width             : std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0) := std_logic_vector(to_unsigned(AXI_DATA_WIDTH, AXI_ADDR_WIDTH));
       
    begin                                                                   
      case Num_of_Burst is
      
      when 2         =>  --2
         v_bytes_per_burst_transfer := "00"  & v_axi_data_width(AXI_ADDR_WIDTH - 1 downto 2);
         
      when 4         =>  --4
         v_bytes_per_burst_transfer := '0'   & v_axi_data_width(AXI_ADDR_WIDTH - 1 downto 1);
         
      when 8         =>  --8
         v_bytes_per_burst_transfer :=         v_axi_data_width(AXI_ADDR_WIDTH - 1 downto 0);
         
      when 16        => --16  
         v_bytes_per_burst_transfer :=         v_axi_data_width(AXI_ADDR_WIDTH - 2 downto 0) & '0';
         
      when 32        => --32
         v_bytes_per_burst_transfer :=         v_axi_data_width(AXI_ADDR_WIDTH - 3 downto 0) & "00";
         
      when 64        => --64
         v_bytes_per_burst_transfer :=         v_axi_data_width(AXI_ADDR_WIDTH - 4 downto 0) & "000";
         
      when others    => --128
         v_bytes_per_burst_transfer :=         v_axi_data_width(AXI_ADDR_WIDTH - 5 downto 0) & "0000";
      
      end case;

      return(v_bytes_per_burst_transfer);                                                                                                             
    end;
    

--    constant C_DMA_MAX_RD_BURST_TRANSFERS_INT       : integer := 32;--64;
--    constant C_NUM_OF_BYTES_PER_RD_TRANSFER_INT     : integer := C_DMA_MAX_RD_BURST_TRANSFERS_INT*(C_M_AXI_DATA_WIDTH/8); --X"00_00_10_00"; -- 64x64 = 4096  
-- 
--    constant C_DMA_MAX_RD_BURST_TRANSFERS           : std_logic_vector( 7 downto 0) := std_logic_vector(to_unsigned(C_DMA_MAX_RD_BURST_TRANSFERS_INT  , 8));-- X"40" -- 64
--    constant C_NUM_OF_BYTES_PER_RD_TRANSFER         : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(C_NUM_OF_BYTES_PER_RD_TRANSFER_INT,32));
--    constant C_DMA_RD_ADDR_OFFSET                   : std_logic_vector(31 downto 0) := C_NUM_OF_BYTES_PER_RD_TRANSFER;
   

   type     AXI_READ_STATES is(st_read_idle,
                               st_read_p1,
                               st_read_p2,
                               st_wait_read_data_done
                              );
                           
   signal   AXI_READ_State       :  AXI_READ_STATES;
   
   signal   s_rd_addr_valid      :  std_logic;                    
   signal   s_rd_addr            :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1  downto 0);       
   signal   s_rd_len             :  std_logic_vector(7 downto 0);       
   signal   s_rd_addr_ready      :  std_logic;       
   signal   s_rd_data            :  std_logic_vector(C_M_AXI_DATA_WIDTH - 1 downto 0);       
   signal   s_rd_data_valid      :  std_logic;                            
   signal   s_rd_data_last       :  std_logic;                            
   signal   s_rd_data_processing :  std_logic; 
   signal   s_rd_data_ready      :  std_logic;    
   signal   s_rd_data_byte_num   :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1 downto 0);
   signal   s_rd_addr_offset     :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1 downto 0);
   signal   s_bytes_per_transfer :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1 downto 0);
   signal   s_src_addr           :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1 downto 0);
   signal   s_data_size          :  std_logic_vector(63 downto 0); 
   signal   s_rd_addr_limit      :  std_logic_vector(C_M_AXI_ADDR_WIDTH -1 downto 0); 
   
begin


   M_AXI_ARID        <= (others => '0');
   M_AXI_ARADDR      <= s_rd_addr;
   M_AXI_ARLEN       <= s_rd_len;
   M_AXI_ARSIZE      <= AXI_PORT_WIDTH_to_ARSIZE(C_M_AXI_DATA_WIDTH);
   M_AXI_ARBURST     <= "01";
   M_AXI_ARLOCK      <= (others => '0');
   M_AXI_ARCACHE     <= "0010";
   M_AXI_ARPROT      <= "000";
   M_AXI_ARQOS       <= x"0";
   M_AXI_ARUSER      <= i_rd_context_id;
   M_AXI_ARVALID     <= s_rd_addr_valid;
   M_AXI_RREADY      <= s_rd_data_ready; 

   s_rd_data_ready   <= s_rd_data_processing and (not i_fifo_wr_full);
   o_fifo_wr_rq      <= s_rd_data_valid and s_rd_data_ready;
   o_fifo_data_in    <= s_rd_data;
   
   s_rd_data_last    <= M_AXI_RLAST;
   s_rd_data_valid   <= M_AXI_RVALID;
   s_rd_data         <= M_AXI_RDATA;
   s_rd_addr_ready   <= M_AXI_ARREADY;  
   
   --== DMA AXI READ Process =================================================================================
   process(M_AXI_ACLK, M_AXI_ARESETN) is
   begin
      if(M_AXI_ARESETN = '0') then
         
         AXI_READ_State       <= st_read_idle;
         o_axi_read_idle      <= '0';
         o_axi_read_done      <= '0';
         s_rd_data_byte_num   <= (others => '0');
         s_rd_addr            <= (others => '0');
         s_rd_addr_offset     <= (others => '0');
         s_rd_len             <= (others => '0');
         s_rd_addr_valid      <= '0';
         s_rd_data_processing <= '0';
         s_bytes_per_transfer <= (others => '0');
         s_src_addr           <= (others => '0');
         s_data_size          <= (others => '0');
         s_rd_addr_limit      <= (others => '0');
         o_state_is_idle      <= '0';

      elsif(rising_edge(M_AXI_ACLK)) then
         
         o_state_is_idle      <= '0';

         case AXI_READ_State is
         
         --==================================================================================
         when st_read_idle =>
            s_rd_addr_valid         <= '0';
            o_axi_read_idle         <= '1';
            s_rd_data_processing    <= '0';
            o_state_is_idle         <= '1';
            if(i_axi_read_start = '1') then
               AXI_READ_State       <= st_read_p1;
               o_axi_read_done      <= '0';
               s_rd_data_byte_num   <= (others => '0');
               s_rd_addr_offset     <= (others => '0');
               o_axi_read_idle      <= '0';
               s_rd_len             <= std_logic_vector(to_unsigned(i_num_of_burst_rd - 1 , 8));
               s_bytes_per_transfer <= Bytes_per_burst_transfer(i_num_of_burst_rd, C_M_AXI_DATA_WIDTH, C_M_AXI_ADDR_WIDTH);
               s_src_addr           <= i_src_addr; 
               s_data_size          <= i_data_size;
            else                    
               AXI_READ_State       <= st_read_idle;
            end if;
         
         --==================================================================================  
         when st_read_p1 =>
               s_rd_addr            <= s_src_addr;
               s_rd_addr_offset     <= s_bytes_per_transfer;
               s_rd_addr_valid      <= '1';
               AXI_READ_State       <= st_read_p2;
               s_rd_addr_limit      <= (s_data_size + s_src_addr) - s_bytes_per_transfer;
                
         --==================================================================================  
         when st_read_p2 =>
            if(s_rd_addr_ready = '1') then
               s_rd_data_processing    <= '1';
               if (s_rd_addr < s_rd_addr_limit) then
                  s_rd_addr            <= s_src_addr + s_rd_addr_offset;
                  s_rd_addr_offset     <= s_bytes_per_transfer + s_rd_addr_offset;
                  s_rd_addr_valid      <= '1';
                  AXI_READ_State       <= st_read_p2;
               else
                  AXI_READ_State       <= st_wait_read_data_done;
                  s_rd_addr_valid      <= '0';
               end if;   
            end if;
                           
         --==================================================================================  
         when st_wait_read_data_done =>
            if (s_rd_data_byte_num < s_data_size) then
               AXI_READ_State       <= st_wait_read_data_done;
            else
               AXI_READ_State       <= st_read_idle;
               s_rd_data_processing <= '0';
               o_axi_read_done      <= '1';
            end if;
            
         --=================================================================================
         when others =>
            null;
                     
         end case;   

         if (s_rd_data_last = '1' and s_rd_data_valid = '1' and s_rd_data_ready = '1') then
             s_rd_data_byte_num   <= s_bytes_per_transfer + s_rd_data_byte_num;
         end if;         
                
         
      end if;
   end process;      

end axi_master_rd;
