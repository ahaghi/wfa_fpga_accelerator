LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

Library xpm;
use xpm.vcomponents.all;

ENTITY XPM_FIFO_1024x256 IS
   GENERIC (
      G_FIFO_MEMORY_TYPE : String
    );
   PORT (
      clk           : IN STD_LOGIC;
      srst          : IN STD_LOGIC;
      din           : IN STD_LOGIC_VECTOR(1023 DOWNTO 0);
      wr_en         : IN STD_LOGIC;
      rd_en         : IN STD_LOGIC;
      dout          : OUT STD_LOGIC_VECTOR(1023 DOWNTO 0);
      full          : OUT STD_LOGIC;
      empty         : OUT STD_LOGIC;
      data_count    : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
      wr_rst_busy   : OUT STD_LOGIC;
      rd_rst_busy   : OUT STD_LOGIC
   );
END XPM_FIFO_1024x256;

ARCHITECTURE XPM_FIFO_1024x256_arch OF XPM_FIFO_1024x256 IS   

  COMPONENT xpm_fifo_sync IS
    GENERIC (
      DOUT_RESET_VALUE        : String;
      ECC_MODE                : String;
      FIFO_MEMORY_TYPE        : String;      -- block, ultra
      FIFO_READ_LATENCY       : INTEGER;
      FIFO_WRITE_DEPTH        : INTEGER;
      FULL_RESET_VALUE        : INTEGER;
      PROG_EMPTY_THRESH       : INTEGER;
      PROG_FULL_THRESH        : INTEGER;
      RD_DATA_COUNT_WIDTH     : INTEGER;
      READ_DATA_WIDTH         : INTEGER;
      READ_MODE               : String;
      USE_ADV_FEATURES        : String;
      WAKEUP_TIME             : INTEGER;
      WRITE_DATA_WIDTH        : INTEGER;
      WR_DATA_COUNT_WIDTH     : INTEGER
    );
    PORT (
      data_valid     : out std_logic;                                            -- 1-bit output: Read Data Valid: When asserted, this signal indicates
      dout           : out std_logic_vector(READ_DATA_WIDTH-1 downto 0);         -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven
      empty          : out std_logic;                                            -- 1-bit output: Empty Flag: When asserted, this signal indicates that
      full           : out std_logic;                                            -- 1-bit output: Full Flag: When asserted, this signal indicates that the
      overflow       : out std_logic;                                            -- 1-bit output: Overflow: This signal indicates that a write request
      prog_empty     : out std_logic;                                            -- 1-bit output: Programmable Empty: This signal is asserted when the
      prog_full      : out std_logic;                                            -- 1-bit output: Programmable Full: This signal is asserted when the
      rd_data_count  : out std_logic_vector(RD_DATA_COUNT_WIDTH-1 downto 0);     -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates
      rd_rst_busy    : out std_logic;                                            -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO
      sbiterr        : out std_logic;                                            -- 1-bit output: Single Bit Error: Indicates that the ECC decoder
      underflow      : out std_logic;                                            -- 1-bit output: Underflow: Indicates that the read request (rd_en)
      wr_ack         : out std_logic;                                            -- 1-bit output: Write Acknowledge: This signal indicates that a write
      wr_data_count  : out std_logic_vector(WR_DATA_COUNT_WIDTH-1 downto 0);     -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates
      wr_rst_busy    : out std_logic;                                            -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO
      din            : in std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);         -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when
      injectdbiterr  : in std_logic;                                             -- 1-bit input: Double Bit Error Injection: Injects a double bit error if
      injectsbiterr  : in std_logic;                                             -- 1-bit input: Single Bit Error Injection: Injects a single bit error if
      rd_en          : in std_logic;                                             -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this
      rst            : in std_logic;                                             -- 1-bit input: Reset: Must be synchronous to wr_clk. Must be applied
      sleep          : in std_logic;                                             -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo
      wr_clk         : in std_logic;                                             -- 1-bit input: Write clock: Used for write operation. wr_clk must be a
      wr_en          : in std_logic                                              -- 1-bit input: Write Enable: If the FIFO is not full, asserting this
    );
  END COMPONENT xpm_fifo_sync;
   
BEGIN

   xpm_fifo_sync_inst : xpm_fifo_sync
   generic map (
      DOUT_RESET_VALUE        => "0",        -- String
      ECC_MODE                => "no_ecc",   -- String
      FIFO_MEMORY_TYPE        => G_FIFO_MEMORY_TYPE,     -- String      block, ultra
      FIFO_READ_LATENCY       => 0,          -- DECIMAL
      FIFO_WRITE_DEPTH        => 256,        -- DECIMAL
      FULL_RESET_VALUE        => 0,          -- DECIMAL
      PROG_EMPTY_THRESH       => 5,          -- DECIMAL
      PROG_FULL_THRESH        => 224,        -- DECIMAL
      RD_DATA_COUNT_WIDTH     => 9,          -- DECIMAL
      READ_DATA_WIDTH         => 1024,       -- DECIMAL
      READ_MODE               => "fwft",     -- String
      USE_ADV_FEATURES        => "0404",     -- String
      WAKEUP_TIME             => 0,          -- DECIMAL
      WRITE_DATA_WIDTH        => 1024,       -- DECIMAL
      WR_DATA_COUNT_WIDTH     => 9           -- DECIMAL
   )
   port map (
      --data_valid      => data_valid,         
      dout              => dout,                     
      empty             => empty,                   
      full              => full,                     
      --overflow        => overflow,           
      --prog_empty      => prog_empty,       
      --prog_full       => prog_full,        
      rd_data_count     => data_count,   
      rd_rst_busy       => rd_rst_busy,       
      --sbiterr         => sbiterr,             
      --underflow       => underflow,         
      --wr_ack          => wr_ack,               
      --wr_data_count   => wr_data_count,   
      wr_rst_busy       => wr_rst_busy,       
      din               => din,                       
      injectdbiterr     => '0',             
      injectsbiterr     => '0',             
      rd_en             => rd_en,                  
      rst               => srst,                      
      sleep             => '0',                    
      wr_clk            => clk,                 
      wr_en             => wr_en                   
   );

   
END XPM_FIFO_1024x256_arch;