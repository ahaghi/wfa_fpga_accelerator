library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.WFA_PKG.all;

entity affine_gap_extend_v3 is
   generic (G_ID : integer range -c_k_range to c_k_range);
   port (
      i_clk                : in        std_logic; 
      i_rst_n              : in        std_logic; 
      i_Seq1_Mers          : in        Array_Mers_k;
      i_Seq2_Mers          : in        Array_Mers_k;
      i_text_len           : in        integer range 0 to c_max_seq_len;
      i_pattern_len        : in        integer range 0 to c_max_seq_len;
      i_start_pos          : in        integer range -c_k_range to c_k_range;
      i_valid              : in        std_logic;
      i_offset             : in        integer range -c_max_seq_len to c_max_seq_len;
      o_New_Offset         : out       integer range -c_max_seq_len to c_max_seq_len;
      o_busy               : out       std_logic;
      o_valid              : out       std_logic
   );
end affine_gap_extend_v3;

architecture affine_gap_extend_v3 of affine_gap_extend_v3 is
   
   type	States is(st_idle,
                   st_compare
                  );
                  

   signal state            : States;
   
   signal s_comp           : std_logic_vector((c_xor_len*2)-1 downto 0);    
   signal s_comp_in1       : std_logic_vector((c_xor_len*2)-1 downto 0);    
   signal s_comp_in2       : std_logic_vector((c_xor_len*2)-1 downto 0);    
   signal s_cntr           : integer range 0 to c_max_seq_len+c_xor_len;
   signal s_indx           : integer range 0 to c_k_num;
   signal s_busy           : std_logic;
   signal s_old_offset     : integer range -c_max_seq_len to c_max_seq_len;
   signal s_max_match      : integer range 0 to c_max_seq_len;
   signal s_pos1           : integer range -c_max_seq_len to c_max_seq_len;
   signal s_pos2           : integer range -c_max_seq_len to c_max_seq_len;
   signal s_pos1_c         : integer range 0 to c_k_num;
   signal s_pos1_f         : integer range 0 to c_xor_len-1;
   signal s_pos2_c         : integer range 0 to c_k_num;
   signal s_pos2_f         : integer range 0 to c_xor_len-1;
   signal s_Match_Num      : integer range 0 to c_max_seq_len;
   signal s_match          : integer range 0 to 15;

begin

     s_pos1   <= i_offset;                                                                
     s_Pos2   <= i_offset - (G_ID + i_start_pos);
   
   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         s_cntr          <= 0;
         s_indx          <= 1;
         s_max_match     <= 0;
         s_old_offset    <= 0;
         s_pos1_c        <= 0;
         s_pos1_f        <= 0;
         s_pos2_c        <= 0;
         s_pos2_f        <= 0;
         s_Match_Num     <= 0;
         o_New_Offset    <= 0;
         o_busy          <= '0';
         o_valid         <= '0';
         state           <= st_idle;

      elsif(rising_edge(i_clk)) then
         
         o_valid  <= '0';

         case state is
         when st_idle =>
            state <= st_idle;
            if (i_valid = '1') then
               if (s_pos1 >= 0 and s_pos2 >= 0 and s_pos1 < i_text_len and s_pos2 < i_pattern_len) then
                  s_old_offset <= i_offset;
                  s_pos1_c     <= s_pos1/8;
                  s_pos1_f     <= s_pos1 - (s_pos1/8) * 8;
                  s_pos2_c     <= s_pos2/8;
                  s_pos2_f     <= s_pos2 - (s_pos2/8) * 8;
                  s_indx       <=  0 ; 
                  o_busy       <= '1';
                  if(i_text_len - s_pos1 < i_pattern_len - s_pos2) then
                     s_max_match  <= i_text_len - s_pos1;
                  else
                     s_max_match  <= i_pattern_len - s_pos2;
                  end if;
                  state <= st_compare;
               else
                  s_max_match  <= 0;
                  o_New_Offset <= i_offset;
                  o_valid      <= '1';
                  o_busy       <= '0';
                  s_cntr       <= 0;
                  s_indx       <= 1;
                  s_pos1_c     <= 0;
                  s_pos1_f     <= 0;
                  s_pos2_c     <= 0;
                  s_pos2_f     <= 0;
               end if;
            end if;
               
         when st_compare =>
            state   <= st_idle;
            o_valid <= '1';
            o_busy  <= '0';
            s_cntr  <=  0 ;
            s_indx  <=  1 ;
            if (s_match + s_cntr <= s_max_match) then
               o_New_Offset <= s_match + s_cntr + s_old_offset;
            else
               o_New_Offset <= s_max_match + s_old_offset;
            end if;
               
            if (s_match = 8 and s_cntr < s_max_match) then
               s_cntr       <= s_cntr + 8;
               s_indx       <= s_indx + 1;
               state        <= st_compare;
               o_valid      <= '0';
               o_busy       <= '1';
            end if;
               
         when others =>
            null;
         end case;
         
      end if;    
   end process; 
 
   s_comp   <= (s_comp_in1 xor s_comp_in2);  
            
   s_match  <= 0 when (s_comp >= X"40_00") else      
               1 when (s_comp >= X"10_00") else
               2 when (s_comp >= X"04_00") else
               3 when (s_comp >= X"01_00") else 
               4 when (s_comp >= X"00_40") else
               5 when (s_comp >= X"00_10") else
               6 when (s_comp >= X"00_04") else
               7 when (s_comp >= X"00_01") else
               8;     
            
      
   process (s_indx) is
   begin
      case s_pos1_f is
      when 0 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)(15 downto 0));
                          
      when 1 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)(13 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto 14));
          
      when 2 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)(11 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto 12));
          
      when 3 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)( 9 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto 10));
          
      when 4 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)( 7 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto  8));
          
      when 5 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)( 5 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto  6));
                          
      when 6 =>
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)( 3 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto  4));
          
      when others => -- 7
         s_comp_in1     <= (i_Seq1_Mers(s_pos1_c+s_indx)( 1 downto 0) & i_Seq1_Mers(s_pos1_c+s_indx+1)(15 downto  2));
      
      end case;
   
      case s_pos2_f is
      when 0 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)(15 downto 0));
       
      when 1 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)(13 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto 14));
       
      when 2 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)(11 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto 12));
       
      when 3 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)( 9 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto 10));
       
      when 4 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)( 7 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto  8));
       
      when 5 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)( 5 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto  6));
       
      when 6 =>
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)( 3 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto  4));
       
      when others => -- 7
         s_comp_in2     <= (i_Seq2_Mers(s_pos2_c+s_indx)( 1 downto 0) & i_Seq2_Mers(s_pos2_c+s_indx+1)(15 downto  2));
                       
      end case;
   end process;
                  

                               
end affine_gap_extend_v3;         