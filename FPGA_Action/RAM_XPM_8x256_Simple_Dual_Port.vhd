LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

Library xpm;
use xpm.vcomponents.all;

ENTITY RAM_XPM_8x256_Simple_Dual_Port IS
  GENERIC (G_MEMORY_PRIMITIVE : STRING);
  PORT (
    clka    : IN STD_LOGIC;
    ena     : IN STD_LOGIC;
    wea     : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina    : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    clkb    : IN STD_LOGIC;
    enb     : IN STD_LOGIC;
    addrb   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    doutb   : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END RAM_XPM_8x256_Simple_Dual_Port;

ARCHITECTURE RAM_XPM_8x256_Simple_Dual_Port_arch OF RAM_XPM_8x256_Simple_Dual_Port IS

   COMPONENT xpm_memory_sdpram IS
      GENERIC (
         ADDR_WIDTH_A            :  INTEGER;
         ADDR_WIDTH_B            :  INTEGER;       -- DECIMAL
         AUTO_SLEEP_TIME         :  INTEGER;       -- DECIMAL
         BYTE_WRITE_WIDTH_A      :  INTEGER;       -- DECIMAL
         CLOCKING_MODE           :  STRING;        -- String
         ECC_MODE                :  STRING;        -- String
         MEMORY_INIT_FILE        :  STRING;        -- String
         MEMORY_INIT_PARAM       :  STRING;        -- String
         MEMORY_OPTIMIZATION     :  STRING;        -- String
         MEMORY_PRIMITIVE        :  STRING;        -- String
         MEMORY_SIZE             :  INTEGER;       -- DECIMAL
         MESSAGE_CONTROL         :  INTEGER;       -- DECIMAL
         READ_DATA_WIDTH_B       :  INTEGER;       -- DECIMAL
         READ_LATENCY_B          :  INTEGER;       -- DECIMAL
         READ_RESET_VALUE_B      :  STRING;        -- String
         USE_EMBEDDED_CONSTRAINT :  INTEGER;       -- DECIMAL
         USE_MEM_INIT            :  INTEGER;       -- DECIMAL
         WAKEUP_TIME             :  STRING;        -- String
         WRITE_DATA_WIDTH_A      :  INTEGER;       -- DECIMAL
         WRITE_MODE_B            :  STRING         -- String
      );
   PORT (
      dbiterrb       : out std_logic;                                         -- 1-bit output: Status signal to indicate double bit error occurrence
      doutb          : out std_logic_vector(READ_DATA_WIDTH_B-1 downto 0);    -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
      sbiterrb       : out std_logic;                                         -- 1-bit output: Status signal to indicate single bit error occurrence
      addra          : in std_logic_vector(ADDR_WIDTH_A-1 downto 0);          -- ADDR_WIDTH_A-bit input: Address for port A write operations.
      addrb          : in std_logic_vector(ADDR_WIDTH_B-1 downto 0);          -- ADDR_WIDTH_B-bit input: Address for port B read operations.
      clka           : in std_logic;                                          -- 1-bit input: Clock signal for port A. Also clocks port B when
      clkb           : in std_logic;                                          -- 1-bit input: Clock signal for port B when parameter CLOCKING_MODE is
      dina           : in std_logic_vector(WRITE_DATA_WIDTH_A-1 downto 0);    -- WRITE_DATA_WIDTH_A-bit input: Data input for port A write operations.
      ena            : in std_logic;                                          -- 1-bit input: Memory enable signal for port A. Must be high on clock
      enb            : in std_logic;                                          -- 1-bit input: Memory enable signal for port B. Must be high on clock
      injectdbiterra : in std_logic;                                          -- 1-bit input: Controls double bit error injection on input data when
      injectsbiterra : in std_logic;                                          -- 1-bit input: Controls single bit error injection on input data when
      regceb         : in std_logic;                                          -- 1-bit input: Clock Enable for the last register stage on the output
      rstb           : in std_logic;                                          -- 1-bit input: Reset signal for the final port B output register
      sleep          : in std_logic;                                          -- 1-bit input: sleep signal to enable the dynamic power saving feature.
      wea            : in std_logic_vector(0 downto 0)                        -- WRITE_DATA_WIDTH_A-bit input: Write enable vector for port A input
   );
   END COMPONENT xpm_memory_sdpram;

BEGIN

   xpm_memory_sdpram_inst : xpm_memory_sdpram
   generic map (
      ADDR_WIDTH_A => 8,             -- DECIMAL
      ADDR_WIDTH_B => 8,             -- DECIMAL
      AUTO_SLEEP_TIME => 0,            -- DECIMAL
      BYTE_WRITE_WIDTH_A => 8,       -- DECIMAL
      CLOCKING_MODE => "common_clock", -- String
      ECC_MODE => "no_ecc",            -- String
      MEMORY_INIT_FILE => "none",      -- String
      MEMORY_INIT_PARAM => "0",        -- String
      MEMORY_OPTIMIZATION => "true",   -- String
      MEMORY_PRIMITIVE => G_MEMORY_PRIMITIVE,      -- String
      MEMORY_SIZE => 2048,           -- DECIMAL
      MESSAGE_CONTROL => 0,            -- DECIMAL
      READ_DATA_WIDTH_B => 8,        -- DECIMAL
      READ_LATENCY_B => 1,             -- DECIMAL
      READ_RESET_VALUE_B => "0",       -- String
      USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
      USE_MEM_INIT => 0,               -- DECIMAL
      WAKEUP_TIME => "disable_sleep",  -- String
      WRITE_DATA_WIDTH_A => 8,       -- DECIMAL
      WRITE_MODE_B => "read_first"      -- String
   )
   port map (
      --dbiterrb => open,
      doutb          => doutb,                
      --sbiterrb => open,
      addra          => addra,                  
      addrb          => addrb,               
      clka           => clka, 
      clkb           => clkb,
      dina           => dina,               
      ena            => ena,
      enb            => enb,                       
      injectdbiterra => '0', 
      injectsbiterra => '0',
      regceb         => '1',
      rstb           => '0',
      sleep          => '0',                 
      wea            => wea
   );
END RAM_XPM_8x256_Simple_Dual_Port_arch;
