library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_Std.all;
use work.WFA_PKG.all;

entity affine_gap_compute_v2 is
   port (
      i_clk             : in     std_logic; 
      i_rst_n           : in     std_logic; 
      s_k               : in     integer range -c_k_range to c_k_range;
      s_m_sub_Null      : in     std_logic; 
      s_m_gap_Null      : in     std_logic; 
      s_i_ext_Null      : in     std_logic; 
      s_d_ext_Null      : in     std_logic; 
      i_m_wavefront_g   : in     Array_Offset_comp2;
      i_m_wavefront_m   : in     Array_Offset_comp2;
      i_d_wavefront     : in     Array_Offset_comp2;
      i_i_wavefront     : in     Array_Offset_comp2;
      o_m_wavefront     : out    Array_Offset_comp ;
      o_d_wavefront     : out    Array_Offset_comp ;
      o_i_wavefront     : out    Array_Offset_comp ;
      o_m_bt            : out    std_logic_vector(39 downto 0)
   );
end affine_gap_compute_v2;

architecture affine_gap_compute_v2 of affine_gap_compute_v2 is
   
begin

   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         
         o_m_wavefront     <= (others => -c_max_seq_len); 
         o_i_wavefront     <= (others => -c_max_seq_len ); 
         o_d_wavefront     <= (others => -c_max_seq_len ); 
         o_m_bt            <= ((others => '0'));
   
      elsif(rising_edge(i_clk)) then
         o_i_wavefront     <= (others => -c_max_seq_len); 
         o_d_wavefront     <= (others => -c_max_seq_len); 
         
         for k in 1 to c_comp_num loop

            --Compute D-WaveFront
            if (s_m_gap_Null = '0' or s_d_ext_Null = '0') then
               if (i_m_wavefront_g(k+1) > i_d_wavefront(k+1)) then
                  o_d_wavefront(k-1) <= i_m_wavefront_g(k+1); 
                  o_m_bt((k-1)+24)   <=  '0';
               else
                  o_d_wavefront(k-1) <= i_d_wavefront(k+1);
                  o_m_bt((k-1)+24)   <=  '1';
               end if;
            end if;
   
            --Compute I-WaveFront
            if (s_m_gap_Null = '0' or s_i_ext_Null = '0') then 
               if (i_m_wavefront_g(k-1) > i_i_wavefront(k-1)) then
                  o_i_wavefront(k-1) <= i_m_wavefront_g(k-1) + 1;   
                  o_m_bt((k-1)+32)   <=  '0';
               else
                  o_i_wavefront(k-1) <= i_i_wavefront(k-1) + 1; 
                  o_m_bt((k-1)+32)   <=  '1';
               end if;
            end if;

            --Compute M-WaveFront
            if (s_m_sub_Null = '0' or s_m_gap_Null = '0' or s_i_ext_Null = '0' or s_d_ext_Null = '0') then                 
               if    (i_m_wavefront_m(k) + 1   > i_m_wavefront_g(k-1) + 1 and
                      i_m_wavefront_m(k) + 1   > i_i_wavefront(k-1)   + 1 and
                      i_m_wavefront_m(k) + 1   > i_m_wavefront_g(k+1)     and
                      i_m_wavefront_m(k) + 1   > i_d_wavefront(k+1)     ) then
                  
                  o_m_wavefront(k-1) <= i_m_wavefront_m(k) + 1;  
                  o_m_bt(((k-1)*3)+2 downto ((k-1)*3)) <= "100";
                  
               elsif (i_m_wavefront_g(k-1) + 1 > i_i_wavefront(k-1) + 1   and
                      i_m_wavefront_g(k-1) + 1 > i_m_wavefront_g(k+1)     and
                      i_m_wavefront_g(k-1) + 1 > i_d_wavefront(k+1)     
                     ) then
                                     
                  o_m_wavefront(k-1) <= i_m_wavefront_g(k-1) + 1;
                  o_m_bt(((k-1)*3)+2 downto ((k-1)*3)) <=  "101";
                  
               elsif (i_i_wavefront(k-1) + 1 > i_m_wavefront_g(k+1)       and
                      i_i_wavefront(k-1) + 1 > i_d_wavefront(k+1)       
                     ) then
               
                  o_m_wavefront(k-1) <= i_i_wavefront(k-1) + 1;
                  o_m_bt(((k-1)*3)+2 downto ((k-1)*3)) <=  "001";
               
               elsif (i_m_wavefront_g(k+1)   > i_d_wavefront(k+1)       ) then
                  
                  o_m_wavefront(k-1) <= i_m_wavefront_g(k+1);
                  o_m_bt(((k-1)*3)+2 downto ((k-1)*3)) <=  "110";
               else
                  o_m_wavefront(k-1) <= i_d_wavefront(k+1);
                  o_m_bt(((k-1)*3)+2 downto ((k-1)*3)) <=  "010";
               end if;
                  
            end if;

         end loop;
               
      end if;
   end process; 
         

                  
end affine_gap_compute_v2;         