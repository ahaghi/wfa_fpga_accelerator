library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_Std.all;
use work.WFA_PKG.all;

entity affine_gap_align is
   port (
      i_clk                : in     std_logic; 
      i_rst_n              : in     std_logic; 
      i_Seq1_Mers          : in     Array_Mers_k;
      i_Seq2_Mers          : in     Array_Mers_k;
      i_patrn_len          : in     std_logic_vector((8*4)-1 downto 0);
      i_text_len           : in     std_logic_vector((8*4)-1 downto 0);
      i_seq_ID             : in     std_logic_vector((8*8)-1 downto 0);
      i_start              : in     std_logic;
      i_data_read          : in     std_logic;
      o_distance           : out    integer range 0 to c_max_seq_len;
      o_k_max              : buffer integer range 0 to c_max_seq_len;
      o_m_wavefront_ext    : out    Array_Offset;
      --Debug_s              : out    Debug_t;
      o_Dist_busy          : out    std_logic;
      o_seq_ID             : out    std_logic_vector((8*8)-1 downto 0);
      o_Dist_valid         : out    std_logic;
      o_Back_Trace         : out    std_logic_vector(63 downto 0)
   );
end affine_gap_align;

architecture affine_gap_align of affine_gap_align is

   function OR_BITS (X : Array_busy_comp) return std_logic is
     variable TMP : std_logic := '0';
   begin
     for J in X'range loop
       TMP := TMP or X(J);
     end loop; -- works for any size X
     return TMP;
   end OR_BITS;

   constant c_mismatch  : integer := 4-1;
   constant c_gap_open  : integer := 8-1;
   constant c_gap_ext   : integer := 2-1;
      
   type	States is ( st_idle,
                     st_extend,
                     st_compute,
                     st_BT,
                     st_finish
                   );
                
   signal state                  : States;
      
   signal s_valid                : std_logic;
   signal s_busy_all             : std_logic;
   signal s_score                : integer range 0 to c_max_seq_len;
   signal s_k                    : integer range -c_k_range to c_k_range;
   signal s_start_pos            : integer range -c_k_range to c_k_range;
   signal s_sec                  : integer range 0 to (2*c_k_range/c_comp_num)-1;
   signal s_sec2                 : integer range 0 to (2*c_k_range/c_comp_num)-1;
      
   signal s_busy                 : Array_busy_comp;
   signal s_k_distance           : integer range -c_k_range to c_k_range;
   signal s_Seq1_MersP           : Array_Mers_k;
   signal s_Seq1_Mers            : Array_Mers_k;
   signal s_Seq2_MersP           : Array_Mers_k;
   signal s_Seq2_Mers            : Array_Mers_k;
   signal s_patrn_lenP           : std_logic_vector((8*4)-1 downto 0);
   signal s_patrn_len            : std_logic_vector((8*4)-1 downto 0);
   signal s_text_lenP            : std_logic_vector((8*4)-1 downto 0);
   signal s_text_len             : std_logic_vector((8*4)-1 downto 0);
   signal s_seq_ID               : std_logic_vector((8*8)-1 downto 0);
      
   signal s_m_wavefront          : mwavefront;
   signal s_m_wavefront_init     : Array_Offset_comp;
   signal s_m_wavefront2         : Array_Offset_comp;
   signal s_d_wavefront2         : Array_Offset_comp;
   signal s_i_wavefront2         : Array_Offset_comp;
   signal s_i_wavefront          : iwavefront;
   signal s_d_wavefront          : dwavefront;
   signal s_m_wavefront_ext      : Array_Offset_comp;
   signal s_m_wavefront_ext_in   : Array_Offset_comp;
   signal s_m_wavefront_g1       : Array_Offset_comp2;
   signal s_m_wavefront_m1       : Array_Offset_comp2;
   signal s_d_wavefront1         : Array_Offset_comp2;
   signal s_i_wavefront1         : Array_Offset_comp2;
   signal s_m_sub_Null           : std_logic;
   signal s_m_gap_Null           : std_logic;
   signal s_i_ext_Null           : std_logic;
   signal s_d_ext_Null           : std_logic;
   signal s_first_in             : std_logic;
   signal s_m_wavefrontbk        : Wavefront_t;
   signal s_i_wavefrontbk        : Wavefront_t;
   signal s_d_wavefrontbk        : Wavefront_t;

   signal s_wr_addr              : std_logic_vector(7 downto 0);
   signal s_wr_data              : std_logic_vector(39 downto 0);
   signal s_start_BT             : std_logic;
   signal s_BT_busy              : std_logic;
   signal s_start                : std_logic;

   signal s_rd_data_m            : std_logic_vector(23 downto 0);
   signal s_rd_data_io           : std_logic_vector(23 downto 0);
   signal s_rd_data_ie           : std_logic_vector( 7 downto 0);
   signal s_rd_data_do           : std_logic_vector(23 downto 0);
   signal s_rd_data_de           : std_logic_vector( 7 downto 0);
   signal s_rd_addr_m            : std_logic_vector( 7 downto 0);
   signal s_rd_addr_io           : std_logic_vector( 7 downto 0);
   signal s_rd_addr_ie           : std_logic_vector( 7 downto 0);
   signal s_rd_addr_do           : std_logic_vector( 7 downto 0);
   signal s_rd_addr_de           : std_logic_vector( 7 downto 0);
   

begin
   
   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         
         o_Dist_busy  <= '0';
         o_distance   <=  0 ;
         o_Dist_valid <= '0';
         s_valid      <= '0';
         s_first_in   <= '1';
         s_score      <=  0 ; 
         s_k          <=  0 ;
         s_sec        <=  0 ;
         s_sec2       <=  0 ;

         s_wr_addr    <= (others => '0'); 
   
         s_k_distance <=  0 ;
         state        <= st_idle;
         o_seq_ID     <= (others => '0'); 
         s_seq_ID     <= (others => '0'); 
         s_Seq1_MersP <= (others => (others => '0')); 
         s_Seq1_Mers  <= (others => (others => '0')); 
         s_Seq2_MersP <= (others => (others => '0')); 
         s_Seq2_Mers  <= (others => (others => '0')); 
         s_patrn_lenP <= (others => '0'); 
         s_patrn_len  <= (others => '0'); 
         s_text_lenP  <= (others => '0'); 
         s_text_len   <= (others => '0'); 
         s_m_wavefront_init      <= (others => -c_max_seq_len); 
         s_m_wavefront(0).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(1).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(2).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(3).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(4).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(5).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(6).offset <= (others => -c_max_seq_len); 
         s_m_wavefront(7).offset <= (others => -c_max_seq_len); 

         s_i_wavefront(0).offset <= (others => -c_max_seq_len); 
         s_i_wavefront(1).offset <= (others => -c_max_seq_len); 

         s_d_wavefront(0).offset <= (others => -c_max_seq_len); 
         s_d_wavefront(1).offset <= (others => -c_max_seq_len); 

         s_m_wavefront(0).isNull <= '1'; 
         s_m_wavefront(1).isNull <= '1'; 
         s_m_wavefront(2).isNull <= '1'; 
         s_m_wavefront(3).isNull <= '1'; 
         s_m_wavefront(4).isNull <= '1'; 
         s_m_wavefront(5).isNull <= '1'; 
         s_m_wavefront(6).isNull <= '1'; 
         s_m_wavefront(7).isNull <= '1'; 

         s_i_wavefront(0).isNull <= '1'; 
         s_i_wavefront(1).isNull <= '1'; 

         s_d_wavefront(0).isNull <= '1'; 
         s_d_wavefront(1).isNull <= '1'; 

         s_start_pos             <= 0;
         s_start_BT              <= '0'; 
         s_start                 <= '0'; 


      elsif(rising_edge(i_clk)) then

         s_valid      <= '0';
         o_Dist_valid <= '0';
         s_start_BT   <= '0'; 

         s_seq_ID     <= i_seq_ID;  
         s_Seq1_MersP <= i_Seq1_Mers; 
         s_Seq2_MersP <= i_Seq2_Mers;
         s_patrn_lenP <= i_patrn_len;
         s_text_lenP  <= i_text_len;  
         s_start      <= i_start;

         case state is
         when st_idle    =>
            if(s_start = '1' and s_seq_ID > 0) then
               
               state        <= st_extend;
                  
               o_seq_ID     <= s_seq_ID;
               s_Seq1_Mers  <= s_Seq1_MersP;
               s_Seq2_Mers  <= s_Seq2_MersP;
               s_patrn_len  <= s_patrn_lenP;
               s_text_len   <= s_text_lenP;

               s_valid      <= '1';
               s_score      <= 0;
               s_k          <= 0;
               s_sec        <= c_k_range/c_comp_num;
               s_sec2       <= c_k_range/c_comp_num;
               s_start_pos  <= ((c_k_range/c_comp_num)*c_comp_num)-c_k_range;
               s_first_in   <= '1';

               s_wr_addr    <= (others => '0'); 

               s_m_wavefront_init     <= (others => -c_max_seq_len); 
               s_m_wavefront_init(c_idx_0_lcl_pos)  <=  0 ; 
               s_m_wavefrontbk.offset <= (others => -c_max_seq_len); 
               s_d_wavefrontbk.offset <= (others => -c_max_seq_len); 
               s_i_wavefrontbk.offset <= (others => -c_max_seq_len); 
               s_m_wavefrontbk.isNull <= '1'; 
               s_d_wavefrontbk.isNull <= '1'; 
               s_i_wavefrontbk.isNull <= '1'; 

               s_m_wavefront(0).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(1).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(2).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(3).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(4).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(5).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(6).offset <= (others => -c_max_seq_len); 
               s_m_wavefront(7).offset <= (others => -c_max_seq_len); 

               s_i_wavefront(0).offset <= (others => -c_max_seq_len); 
               s_i_wavefront(1).offset <= (others => -c_max_seq_len); 

               s_d_wavefront(0).offset <= (others => -c_max_seq_len); 
               s_d_wavefront(1).offset <= (others => -c_max_seq_len); 

               s_m_wavefront(0).isNull <= '0'; 
               s_m_wavefront(1).isNull <= '1'; 
               s_m_wavefront(2).isNull <= '1'; 
               s_m_wavefront(3).isNull <= '1'; 
               s_m_wavefront(4).isNull <= '1'; 
               s_m_wavefront(5).isNull <= '1'; 
               s_m_wavefront(6).isNull <= '1'; 
               s_m_wavefront(7).isNull <= '1'; 

               s_i_wavefront(0).isNull <= '1'; 
               s_i_wavefront(1).isNull <= '1'; 

               s_d_wavefront(0).isNull <= '1'; 
               s_d_wavefront(1).isNull <= '1'; 
               
               s_k_distance <= to_integer(signed(i_text_len - i_patrn_len));
                                 
               o_Dist_busy  <= '1';
               o_Dist_valid <= '0';
            else
               state <= st_idle;
            end if;
               
         when st_extend  =>
                                       
            if(s_valid = '1' and s_first_in = '0') then
               
               case (s_sec) is
               when 0 =>
                  for i in 0*c_comp_num-c_k_range to c_comp_num*(0+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (0*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (0*c_comp_num-c_k_range));
                  end loop;
               when 1 => --1 =>
                  for i in 1*c_comp_num-c_k_range to c_comp_num*(1+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (1*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (1*c_comp_num-c_k_range));
                  end loop;
               when 2 =>
                  for i in 2*c_comp_num-c_k_range to c_comp_num*(2+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (2*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (2*c_comp_num-c_k_range));
                  end loop;
               when 3 =>
                  for i in 3*c_comp_num-c_k_range to c_comp_num*(3+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (3*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (3*c_comp_num-c_k_range));
                  end loop;
               when 4 =>
                  for i in 4*c_comp_num-c_k_range to c_comp_num*(4+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (4*c_comp_num-c_k_range));    
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (4*c_comp_num-c_k_range));
                  end loop;
               when 5 =>
                  for i in 5*c_comp_num-c_k_range to c_comp_num*(5+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (5*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (5*c_comp_num-c_k_range));
                  end loop;
               when 6 => --6
                  for i in 6*c_comp_num-c_k_range to c_comp_num*(6+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (6*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (6*c_comp_num-c_k_range));
                  end loop;
               when others => --7 =>
                  for i in 7*c_comp_num-c_k_range to c_comp_num*(7+1)-1-c_k_range loop
                     s_i_wavefront(0).offset(i) <= s_i_wavefront2(i - (7*c_comp_num-c_k_range));
                     s_d_wavefront(0).offset(i) <= s_d_wavefront2(i - (7*c_comp_num-c_k_range));
                  end loop;
               end case;
                     
               if (s_start_pos + c_comp_num <= s_k) then
                  s_sec2       <= s_sec + 1;
               end if;
            end if;
                  
            if (s_busy_all = '0' and s_valid = '0') then        
               if (s_start_pos + c_comp_num > s_k) then
                  state       <= st_compute;
                  s_first_in  <= '0';
                  s_k         <= s_k + 1;
                  s_sec       <=   (c_k_range - (s_k + 1))/c_comp_num;
                  s_sec2      <=   (c_k_range - (s_k + 1))/c_comp_num;
                  s_start_pos <= (((c_k_range - (s_k + 1))/c_comp_num)*c_comp_num)-c_k_range;
                  s_score     <= s_score + 1;
                  if (s_k >= c_k_range) then
                     state       <= st_finish; 
                     --o_Back_Trace<= (others => '0');
                     o_k_max     <= s_k;
                     o_distance  <= 0;
                  end if; 
                  s_m_wavefront(1).offset <= s_m_wavefrontbk.offset;
                  s_i_wavefront(1).offset <= s_i_wavefrontbk.offset;
                  s_d_wavefront(1).offset <= s_d_wavefrontbk.offset;
                  s_m_wavefront(1).isNull <= s_m_wavefrontbk.isNull;
                  s_i_wavefront(1).isNull <= s_i_wavefrontbk.isNull;
                  s_d_wavefront(1).isNull <= s_d_wavefrontbk.isNull;
                  for idx in 2 to 7 loop
                      s_m_wavefront(idx).offset <= s_m_wavefront(idx-1).offset;
                      s_m_wavefront(idx).isNull <= s_m_wavefront(idx-1).isNull;
                  end loop;
                  for idx in 2 to 1 loop
                     s_i_wavefront(idx).offset <= s_i_wavefront(idx-1).offset;
                     s_d_wavefront(idx).offset <= s_d_wavefront(idx-1).offset;
                     s_i_wavefront(idx).isNull <= s_i_wavefront(idx-1).isNull;
                     s_d_wavefront(idx).isNull <= s_d_wavefront(idx-1).isNull;
                  end loop;
               else
                  s_sec       <= s_sec + 1;
                  s_start_pos <= s_start_pos + c_comp_num;
                  s_valid     <= '1';
                  s_wr_addr   <= s_wr_addr + 1;
                  state       <= st_extend;
               end if;
                  
               case (s_sec) is
               when 0 =>
                  for i in 0*c_comp_num-c_k_range to c_comp_num*(0+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (0*c_comp_num-c_k_range));
                  end loop;
               when 1 => --1 =>
                  for i in 1*c_comp_num-c_k_range to c_comp_num*(1+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (1*c_comp_num-c_k_range));
                  end loop;
               when 2 =>
                  for i in 2*c_comp_num-c_k_range to c_comp_num*(2+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (2*c_comp_num-c_k_range));
                  end loop;
               when 3 =>
                  for i in 3*c_comp_num-c_k_range to c_comp_num*(3+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (3*c_comp_num-c_k_range));
                  end loop;
               when 4 =>
                  for i in 4*c_comp_num-c_k_range to c_comp_num*(4+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (4*c_comp_num-c_k_range));
                  end loop;
               when 5 =>
                  for i in 5*c_comp_num-c_k_range to c_comp_num*(5+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (5*c_comp_num-c_k_range));
                  end loop;
               when 6 =>
                  for i in 6*c_comp_num-c_k_range to c_comp_num*(6+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (6*c_comp_num-c_k_range));
                  end loop;
               when others => --7 =>
                  for i in 7*c_comp_num-c_k_range to c_comp_num*(7+1)-1-c_k_range loop
                     s_m_wavefront(0).offset(i) <= s_m_wavefront_ext(i - (7*c_comp_num-c_k_range));
                   end loop;

               end case;               
                  
               for k in 0 to c_comp_num-1 loop
                  if (s_k_distance = s_start_pos+k and s_m_wavefront_ext(k) >= s_text_len) then
                     state       <= st_BT;
                     s_start_BT  <= '1'; 
                     o_k_max     <= s_k;
                     o_distance  <= s_score;
                  end if;
               end loop;
                                          
            end if;    
               
         when st_compute =>
                    
            state    <= st_compute;

            if (s_m_gap_Null = '0' or s_d_ext_Null = '0') then
               s_d_wavefront(0).isNull <= '0';  
            else
               s_d_wavefront(0).isNull <= '1';
            end if;
               
            if (s_m_gap_Null = '0' or s_i_ext_Null = '0') then
               s_i_wavefront(0).isNull <= '0';  
            else
               s_i_wavefront(0).isNull <= '1';
            end if;
               
            if (s_m_sub_Null = '0' or s_m_gap_Null = '0' or s_i_ext_Null = '0' or s_d_ext_Null = '0') then
               s_m_wavefront(0).isNull <= '0';
               s_valid     <= '1';
               s_wr_addr   <= s_wr_addr + 1;
               state       <= st_extend;  
               s_m_wavefrontbk.offset <= s_m_wavefront(0).offset;
               s_i_wavefrontbk.offset <= s_i_wavefront(0).offset;
               s_d_wavefrontbk.offset <= s_d_wavefront(0).offset;
               s_i_wavefrontbk.isNull <= s_i_wavefront(0).isNull;
               s_d_wavefrontbk.isNull <= s_d_wavefront(0).isNull;
               s_m_wavefrontbk.isNull <= s_m_wavefront(0).isNull;
            else             
               -- Initializing wavefronts ===========================================   
               s_m_wavefront(0).offset <= (others => -c_max_seq_len);   
               s_i_wavefront(0).offset <= (others => -c_max_seq_len);   
               s_d_wavefront(0).offset <= (others => -c_max_seq_len);   
               s_m_wavefront(0).isNull <= '1';   

               -- Shifting wavefronts ===============================================
               --for idx in 1 to 7 loop
               for idx in 1 to 7 loop
                   s_m_wavefront(idx).offset <= s_m_wavefront(idx-1).offset;
                   s_m_wavefront(idx).isNull <= s_m_wavefront(idx-1).isNull;
               end loop;
               for idx in 1 to 1 loop
                  s_i_wavefront(idx).offset <= s_i_wavefront(idx-1).offset;
                  s_d_wavefront(idx).offset <= s_d_wavefront(idx-1).offset;
                  s_i_wavefront(idx).isNull <= s_i_wavefront(idx-1).isNull;
                  s_d_wavefront(idx).isNull <= s_d_wavefront(idx-1).isNull;
               end loop;

               s_score   <= s_score + 1;
            end if;

         when st_BT      =>
            if (s_start_BT = '0' and  s_BT_busy = '0') then
               state        <= st_finish;
            else
               state        <= st_BT;  
            end if;
                
         when st_finish  =>
            if (i_data_read = '1') then
               o_Dist_busy  <= '0';
               o_Dist_valid <= '1';
               state        <= st_idle;
            else
               state        <= st_finish;
            end if;

         when others     =>
            null;
        end case;
           
      end if;
   end process; 

   u_ram_m: entity work.RAM_XPM_24x256_Simple_Dual_Port 
      generic map (G_MEMORY_PRIMITIVE => "ultra")
      port map (
         clka              => i_clk,
         ena               => s_valid,--s_wr_en,
         wea               => "1",
         addra             => s_wr_addr,
         dina              => s_wr_data(23 downto 0),
         clkb              => i_clk,
         enb               => '1',
         addrb             => s_rd_addr_m,
         doutb             => s_rd_data_m
      );

   u_ram_io: entity work.RAM_XPM_24x256_Simple_Dual_Port 
      generic map (G_MEMORY_PRIMITIVE => "ultra")
      port map (
         clka              => i_clk,
         ena               => s_valid,--s_wr_en,
         wea               => "1",
         addra             => s_wr_addr,
         dina              => s_wr_data(23 downto 0),
         clkb              => i_clk,
         enb               => '1',
         addrb             => s_rd_addr_io,
         doutb             => s_rd_data_io
      );
      
   u_ram_do: entity work.RAM_XPM_24x256_Simple_Dual_Port 
      generic map (G_MEMORY_PRIMITIVE => "ultra")
      port map (
         clka              => i_clk,
         ena               => s_valid,--s_wr_en,
         wea               => "1",
         addra             => s_wr_addr,
         dina              => s_wr_data(23 downto 0),
         clkb              => i_clk,
         enb               => '1',
         addrb             => s_rd_addr_do,
         doutb             => s_rd_data_do
      );
      
   u_ram_ie: entity work.RAM_XPM_8x256_Simple_Dual_Port 
      generic map (G_MEMORY_PRIMITIVE => "block")
      port map (
         clka              => i_clk,
         ena               => s_valid,--s_wr_en,
         wea               => "1",
         addra             => s_wr_addr,
         dina              => s_wr_data(39 downto 32),
         clkb              => i_clk,
         enb               => '1',
         addrb             => s_rd_addr_ie,
         doutb             => s_rd_data_ie
      );
      
   u_ram_de: entity work.RAM_XPM_8x256_Simple_Dual_Port 
      generic map (G_MEMORY_PRIMITIVE => "block")
      port map (
         clka              => i_clk,
         ena               => s_valid,--s_wr_en,
         wea               => "1",
         addra             => s_wr_addr,
         dina              => s_wr_data(31 downto 24),
         clkb              => i_clk,
         enb               => '1',
         addrb             => s_rd_addr_de,
         doutb             => s_rd_data_de
      );     
      
   u_affine_gap_backtrace: entity work.affine_gap_backtrace_v2
      port map(
         i_clk             => i_clk,          
         i_rst_n           => i_rst_n,                     
         i_start           => s_start_BT,                      
         i_kmax            => o_k_max,            
         i_kdits           => s_k_distance,               
         i_final_addr      => s_wr_addr,               
         i_rd_data_m       => s_rd_data_m ,         
         i_rd_data_io      => s_rd_data_io,         
         i_rd_data_ie      => s_rd_data_ie,         
         i_rd_data_do      => s_rd_data_do,         
         i_rd_data_de      => s_rd_data_de,         
         o_rd_addr_m       => s_rd_addr_m , 
         o_rd_addr_io      => s_rd_addr_io, 
         o_rd_addr_ie      => s_rd_addr_ie, 
         o_rd_addr_do      => s_rd_addr_do, 
         o_rd_addr_de      => s_rd_addr_de, 
         o_Back_Trace      => o_Back_Trace,               
         o_busy            => s_BT_busy         
      );
   
   u_affine_gap_compute: entity work.affine_gap_compute_v2
      port map(
         i_clk             => i_clk,
         i_rst_n           => i_rst_n,  
         s_k               => s_k,   
         s_m_sub_Null      => s_m_sub_Null,
         s_m_gap_Null      => s_m_gap_Null,
         s_i_ext_Null      => s_i_ext_Null,
         s_d_ext_Null      => s_d_ext_Null,
         i_m_wavefront_g   => s_m_wavefront_g1, 
         i_m_wavefront_m   => s_m_wavefront_m1, 
         i_d_wavefront     => s_d_wavefront1  , 
         i_i_wavefront     => s_i_wavefront1  , 
         o_m_wavefront     => s_m_wavefront2,
         o_d_wavefront     => s_d_wavefront2,
         o_i_wavefront     => s_i_wavefront2,  
         o_m_bt            => s_wr_data  
      );
   
   s_m_wavefront_ext_in <= s_m_wavefront_init when (s_first_in = '1') else s_m_wavefront2;
      
   GEN_Comp_128: 
   for i in 0 to c_comp_num-1 generate

      u_affine_gap_extend:entity work.affine_gap_extend_v3
         generic map (G_ID => i)
         port map(
            i_clk          => i_clk,
            i_rst_n        => i_rst_n,  
            i_Seq1_Mers    => s_Seq1_Mers, 
            i_Seq2_Mers    => s_Seq2_Mers,
            i_text_len     => to_integer(unsigned(s_text_len)),
            i_pattern_len  => to_integer(unsigned(s_patrn_len)),
            i_valid        => s_valid  and (not s_start_BT),  
            i_start_pos    => s_start_pos,
            i_offset       => s_m_wavefront_ext_in(i),
            o_New_Offset   => s_m_wavefront_ext(i),   
            o_busy         => s_busy(i)   
         );    
         
   end generate GEN_Comp_128;      
         
   process(s_sec2,s_m_wavefront(c_gap_open),s_m_wavefront(c_mismatch),s_d_wavefront(c_gap_ext),s_i_wavefront(c_gap_ext)) is
   begin
      case (s_sec2) is
      when 0 =>
         for i in 0*c_comp_num-c_k_range-1 to c_comp_num*(0+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (0*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (0*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (0*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (0*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);    
         end loop;
      when 1 => --1
         for i in 1*c_comp_num-c_k_range-1 to c_comp_num*(1+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (1*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (1*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (1*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (1*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);
         end loop;
      when 2 =>
         for i in 2*c_comp_num-c_k_range-1 to c_comp_num*(2+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (2*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (2*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (2*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (2*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);  
         end loop;
      when 3 =>
         for i in 3*c_comp_num-c_k_range-1 to c_comp_num*(3+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (3*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (3*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (3*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (3*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);
         end loop;
      when 4 =>
         for i in 4*c_comp_num-c_k_range-1 to c_comp_num*(4+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (4*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (4*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (4*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (4*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);
         end loop;
      when 5 =>
         for i in 5*c_comp_num-c_k_range-1 to c_comp_num*(5+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (5*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (5*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (5*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (5*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);
         end loop;
      when 6 => 
         for i in 6*c_comp_num-c_k_range-1 to c_comp_num*(6+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (6*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (6*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (6*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (6*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i);
         end loop;
      when others => --7 =>
         for i in 7*c_comp_num-c_k_range-1 to c_comp_num*(7+1)-1-c_k_range+1 loop
            s_m_wavefront_g1(i - (7*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_gap_open).offset(i);
            s_m_wavefront_m1(i - (7*c_comp_num-c_k_range-1))   <= s_m_wavefront(c_mismatch).offset(i);
            s_d_wavefront1(  i - (7*c_comp_num-c_k_range-1))   <= s_d_wavefront(c_gap_ext).offset(i);
            s_i_wavefront1(  i - (7*c_comp_num-c_k_range-1))   <= s_i_wavefront(c_gap_ext).offset(i); 
         end loop;

      end case;
   end process;   
         

   s_busy_all <= OR_BITS(s_busy);
   
   s_m_sub_Null <= s_m_wavefront(c_mismatch).isNull;      -- m_sub -> 0, 1, 2, 3, 4, 5, 6, 7 -> penalty_mismatch       = 4 = 3 fixed for now  
   s_m_gap_Null <= s_m_wavefront(c_gap_open).isNull;      -- m_gap -> 0, 1, 2, 3, 4, 5, 6, 7 -> penalty_gap open + ext = 8 = 7 fixed for now 
   s_i_ext_Null <= s_i_wavefront(c_gap_ext).isNull;       -- i_ext -> 0, 1                   -> penalty_gap ext        = 2 = 1 fixed for now 
   s_d_ext_Null <= s_d_wavefront(c_gap_ext).isNull;       -- d_ext -> 0, 1                   -> penalty_gap ext        = 2 = 1 fixed for now 
                  
end affine_gap_align;         