library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.WFA_PKG.all;

entity Controller_v2 is
   port (
      i_clk                : in        std_logic; 
      i_rst_n              : in        std_logic; 
      i_busy               : in        Array_1bit;
      o_data_read          : buffer    Array_1bit;
      i_kmers_finished     : in        std_logic;
      i_distance           : in        Array_distance;
      i_k_max              : in        Array_distance;
      i_dist_valid         : in        Array_1bit;
      i_Back_Trace         : in        Array_64bit;
      i_seq_ID             : in        Array_ID;
      i_fifo_near_full     : in        std_logic;
      o_fifo_wr_rq         : buffer    std_logic;
      o_fifo_Din           : out       std_logic_vector(1023 downto 0);
      o_finished           : out       std_logic;
      o_return_data_size   : out       std_logic_vector(63 downto 0)
   );
end Controller_v2;

architecture Controller_v2 of Controller_v2 is
   function OR_BITS (X : Array_1bit) return std_logic is
     variable TMP : std_logic := '0';
   begin
     for J in X'range loop
       TMP := TMP or X(J);
     end loop; 
     return TMP;
   end OR_BITS;

   function AND_BITS (X : Array_1bit) return std_logic is
     variable TMP : std_logic := '1';
   begin
     for J in X'range loop
       TMP := TMP and X(J);
     end loop; 
     return TMP;
   end AND_BITS;

   signal s_cntr         : integer range 0 to 7;
   signal s_priority     : integer range 0 to 7;
   signal s_data_cntr    : std_logic_vector(63 downto 0);
   signal s_busy_all     : std_logic;
   signal s_data_read_all: std_logic;
   signal s_data_mid_all : std_logic;
   signal s_Data_f0      : Array_128bit;
   signal s_Data_f1      : Array_128bit;
   signal s_Data_f2      : Array_128bit;
   signal s_Data_f3      : Array_128bit;
   signal s_Data_f4      : Array_128bit;
   signal s_Data_f5      : Array_128bit;
   signal s_Data_f6      : Array_128bit;
   signal s_Data_f7      : Array_128bit;
   signal s_Data_in      : Array_128bit;
   signal s_Data_f       : Array_1024bit;
   signal s_valid_f      : Array_1bit;
   signal s_valid_m      : Array_1bit;
   signal s_dist_valid   : Array_1bit;
   signal s_busy         : Array_1bit;
   signal s_free_space   : Array_cntr7;
   constant c_padding    : integer := 0;

begin
           
   GEN_REG: 
   for i in 0 to c_cores_num-1 generate
      s_Data_f(i) <= s_Data_f0(i) & s_Data_f1(i) & s_Data_f2(i) & s_Data_f3(i) & s_Data_f4(i) & s_Data_f5(i) & s_Data_f6(i) & s_Data_f7(i);
   end generate GEN_REG;
   
   process(i_clk, i_rst_n) is
   begin
      
      if(i_rst_n = '0') then
         o_fifo_wr_rq          <= '0';
         o_finished            <= '0';
         --s_busy_all            <= '0';
         o_fifo_Din            <= (others => '0');
         s_cntr                <= 0;
         s_priority            <= 0;
         s_data_cntr           <= (others => '0');
         o_return_data_size    <= (others => '0');
         s_Data_f0             <= (others => (others => '0'));
         s_Data_f1             <= (others => (others => '0'));
         s_Data_f2             <= (others => (others => '0'));
         s_Data_f3             <= (others => (others => '0'));
         s_Data_f4             <= (others => (others => '0'));
         s_Data_f5             <= (others => (others => '0'));
         s_Data_f6             <= (others => (others => '0'));
         s_Data_f7             <= (others => (others => '0'));
         o_data_read           <= (others => '1');
         s_valid_f             <= (others => '0');
         s_valid_m             <= (others => '0');
         s_free_space          <= (others => 0);
         s_dist_valid          <= (others => '0');
         s_busy                <= (others => '0'); 
         s_Data_in             <= (others => (others => '0'));

      elsif(rising_edge(i_clk)) then
         
         for i in 0 to c_cores_num-1 loop
            
            s_dist_valid(i) <= i_dist_valid(i);
            s_busy(i)       <= i_busy(i);
            s_Data_in(i)    <= i_Back_Trace(i) & conv_std_logic_vector(i_k_max(i),8*1) & conv_std_logic_vector(i_distance(i),8*3) & i_seq_ID(i)((8*4)-1 downto 0);
            
            if (s_dist_valid(i) = '1') then
               s_valid_m(i)         <= '1';
               case s_free_space(i) is
               when 0 =>
                  s_free_space(i)   <= 1;
                  s_Data_f0(i)      <= s_Data_in(i);
               when 1 =>
                  s_free_space(i)   <= 2;
                  s_Data_f1(i)      <= s_Data_in(i);               
               when 2 =>
                  s_free_space(i)   <= 3;
                  s_Data_f2(i)      <= s_Data_in(i);
               when 3 =>
                  s_free_space(i)   <= 4;
                  s_Data_f3(i)      <= s_Data_in(i);
               when 4 =>
                  s_free_space(i)   <= 5;
                  s_Data_f4(i)      <= s_Data_in(i);
               when 5 =>
                  s_free_space(i)   <= 6;
                  s_Data_f5(i)      <= s_Data_in(i);
               when 6 =>
                  s_free_space(i)   <= 7;
                  s_Data_f6(i)      <= s_Data_in(i);
               when others => --7 =>
                  s_free_space(i)   <= 0;
                  s_Data_f7(i)      <= s_Data_in(i);
                  s_valid_f(i)      <= '1';
                  s_valid_m(i)      <= '0';
               end case;
            end if;
               
            o_data_read(i) <= not s_valid_f(i);
         
         end loop;
            
         o_fifo_wr_rq <= '1';
            
         case s_priority is
         when 0 =>
            s_priority <= 1;
            if (s_valid_f(0) = '1') then
               s_valid_f(0)  <= '0'; 
               o_fifo_Din    <= s_Data_f(0);
            elsif (s_valid_f(1) = '1') then
               s_valid_f(1)  <= '0'; 
               o_fifo_Din    <= s_Data_f(1);
            elsif (s_valid_f(2) = '1') then
               s_valid_f(2)  <= '0'; 
               o_fifo_Din    <= s_Data_f(2);
            elsif (s_valid_f(3) = '1') then
               s_valid_f(3)  <= '0'; 
               o_fifo_Din    <= s_Data_f(3); 
            elsif (s_valid_f(4) = '1') then
               s_valid_f(4)  <= '0'; 
               o_fifo_Din    <= s_Data_f(4);  
            elsif (s_valid_f(5) = '1') then
               s_valid_f(5)  <= '0'; 
               o_fifo_Din    <= s_Data_f(5);
            elsif (s_valid_f(6) = '1') then
               s_valid_f(6)  <= '0'; 
               o_fifo_Din    <= s_Data_f(6); 
            elsif (s_valid_f(7) = '1') then
               s_valid_f(7)  <= '0'; 
               o_fifo_Din    <= s_Data_f(7);
            elsif (s_valid_f(8) = '1') then
               s_valid_f(8)  <= '0'; 
               o_fifo_Din    <= s_Data_f(8);
            elsif (s_valid_f(9) = '1') then
               s_valid_f(9)  <= '0'; 
               o_fifo_Din    <= s_Data_f(9);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         when 1 =>
            s_priority <= 2;
            if (s_valid_f(10) = '1') then
               s_valid_f(10) <= '0'; 
               o_fifo_Din    <= s_Data_f(10); 
            elsif (s_valid_f(11) = '1') then
               s_valid_f(11) <= '0'; 
               o_fifo_Din    <= s_Data_f(11); 
            elsif (s_valid_f(12) = '1') then
               s_valid_f(12) <= '0'; 
               o_fifo_Din    <= s_Data_f(12); 
            elsif (s_valid_f(13) = '1') then
               s_valid_f(13) <= '0'; 
               o_fifo_Din    <= s_Data_f(13);
            elsif (s_valid_f(14) = '1') then
               s_valid_f(14) <= '0'; 
               o_fifo_Din    <= s_Data_f(14);
            elsif (s_valid_f(15) = '1') then
               s_valid_f(15) <= '0'; 
               o_fifo_Din    <= s_Data_f(15);
            elsif (s_valid_f(16) = '1') then
               s_valid_f(16) <= '0'; 
               o_fifo_Din    <= s_Data_f(16);
            elsif (s_valid_f(17) = '1') then
               s_valid_f(17) <= '0'; 
               o_fifo_Din    <= s_Data_f(17);
            elsif (s_valid_f(18) = '1') then
               s_valid_f(18) <= '0'; 
               o_fifo_Din    <= s_Data_f(18);
            elsif (s_valid_f(19) = '1') then
               s_valid_f(19) <= '0'; 
               o_fifo_Din    <= s_Data_f(19);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         when 2 =>
            s_priority <= 3;
            if (s_valid_f(20) = '1') then
               s_valid_f(20) <= '0'; 
               o_fifo_Din    <= s_Data_f(20);
            elsif (s_valid_f(21) = '1') then
               s_valid_f(21) <= '0'; 
               o_fifo_Din    <= s_Data_f(21);
            elsif (s_valid_f(22) = '1') then
               s_valid_f(22) <= '0'; 
               o_fifo_Din    <= s_Data_f(22);
            elsif (s_valid_f(23) = '1') then
               s_valid_f(23) <= '0'; 
               o_fifo_Din    <= s_Data_f(23);
            elsif (s_valid_f(24) = '1') then
               s_valid_f(24) <= '0'; 
               o_fifo_Din    <= s_Data_f(24);
            elsif (s_valid_f(25) = '1') then
               s_valid_f(25) <= '0'; 
               o_fifo_Din    <= s_Data_f(25);
            elsif (s_valid_f(26) = '1') then
               s_valid_f(26) <= '0'; 
               o_fifo_Din    <= s_Data_f(26);
            elsif (s_valid_f(27) = '1') then
               s_valid_f(27) <= '0'; 
               o_fifo_Din    <= s_Data_f(27);
            elsif (s_valid_f(28) = '1') then
               s_valid_f(28) <= '0'; 
               o_fifo_Din    <= s_Data_f(28);
            elsif (s_valid_f(29) = '1') then
               s_valid_f(29) <= '0'; 
               o_fifo_Din    <= s_Data_f(29);
            elsif (s_valid_f(30) = '1') then
               s_valid_f(30) <= '0'; 
               o_fifo_Din    <= s_Data_f(30);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         when 3 =>
            s_priority <= 4;
            if (s_valid_f(31) = '1') then
               s_valid_f(31) <= '0'; 
               o_fifo_Din    <= s_Data_f(31);
            elsif (s_valid_f(32) = '1') then
               s_valid_f(32) <= '0'; 
               o_fifo_Din    <= s_Data_f(32);
            elsif (s_valid_f(33) = '1') then
               s_valid_f(33) <= '0'; 
               o_fifo_Din    <= s_Data_f(33);
            elsif (s_valid_f(34) = '1') then
               s_valid_f(34) <= '0'; 
               o_fifo_Din    <= s_Data_f(34);
            elsif (s_valid_f(35) = '1') then
               s_valid_f(35) <= '0'; 
               o_fifo_Din    <= s_Data_f(35);
            elsif (s_valid_f(36) = '1') then
               s_valid_f(36) <= '0'; 
               o_fifo_Din    <= s_Data_f(36);
            elsif (s_valid_f(37) = '1') then
               s_valid_f(37) <= '0'; 
               o_fifo_Din    <= s_Data_f(37);
            elsif (s_valid_f(38) = '1') then
               s_valid_f(38) <= '0'; 
               o_fifo_Din    <= s_Data_f(38);
            elsif (s_valid_f(39) = '1') then
               s_valid_f(39) <= '0'; 
               o_fifo_Din    <= s_Data_f(39);
            elsif (s_valid_f(40) = '1') then
               s_valid_f(40) <= '0'; 
               o_fifo_Din    <= s_Data_f(40);
            elsif (s_valid_f(41) = '1') then
               s_valid_f(41) <= '0'; 
               o_fifo_Din    <= s_Data_f(41);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         when 4 =>
            s_priority <= 5;
            if (s_valid_f(42) = '1') then
               s_valid_f(42) <= '0'; 
               o_fifo_Din    <= s_Data_f(42);
            elsif (s_valid_f(43) = '1') then
               s_valid_f(43) <= '0'; 
               o_fifo_Din    <= s_Data_f(43);
            elsif (s_valid_f(44) = '1') then
               s_valid_f(44) <= '0'; 
               o_fifo_Din    <= s_Data_f(44);
            elsif (s_valid_f(45) = '1') then
               s_valid_f(45) <= '0'; 
               o_fifo_Din    <= s_Data_f(45);
            elsif (s_valid_f(46) = '1') then
               s_valid_f(46) <= '0'; 
               o_fifo_Din    <= s_Data_f(46);
            elsif (s_valid_f(47) = '1') then
               s_valid_f(47) <= '0'; 
               o_fifo_Din    <= s_Data_f(47);
            elsif (s_valid_f(48) = '1') then
               s_valid_f(48) <= '0'; 
               o_fifo_Din    <= s_Data_f(48);
            elsif (s_valid_f(49) = '1') then
               s_valid_f(49) <= '0'; 
               o_fifo_Din    <= s_Data_f(49);
            elsif (s_valid_f(50) = '1') then
               s_valid_f(50) <= '0'; 
               o_fifo_Din    <= s_Data_f(50);
            elsif (s_valid_f(51) = '1') then
               s_valid_f(51) <= '0'; 
               o_fifo_Din    <= s_Data_f(51);
            elsif (s_valid_f(52) = '1') then
               s_valid_f(52) <= '0'; 
               o_fifo_Din    <= s_Data_f(52);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         when others => --5 =>
            s_priority <= 0;
            if (s_valid_f(53) = '1') then
               s_valid_f(53) <= '0'; 
               o_fifo_Din    <= s_Data_f(53);
            elsif (s_valid_f(54) = '1') then
               s_valid_f(54) <= '0'; 
               o_fifo_Din    <= s_Data_f(54);
            elsif (s_valid_f(55) = '1') then
               s_valid_f(55) <= '0'; 
               o_fifo_Din    <= s_Data_f(55);
            elsif (s_valid_f(56) = '1') then
               s_valid_f(56) <= '0'; 
               o_fifo_Din    <= s_Data_f(56);
            elsif (s_valid_f(57) = '1') then
               s_valid_f(57) <= '0'; 
               o_fifo_Din    <= s_Data_f(57);
            elsif (s_valid_f(58) = '1') then
               s_valid_f(58) <= '0'; 
               o_fifo_Din    <= s_Data_f(58);
            elsif (s_valid_f(59) = '1') then
               s_valid_f(59) <= '0'; 
               o_fifo_Din    <= s_Data_f(59);
            elsif (s_valid_f(60) = '1') then
               s_valid_f(60) <= '0'; 
               o_fifo_Din    <= s_Data_f(60);
            elsif (s_valid_f(61) = '1') then
               s_valid_f(61) <= '0'; 
               o_fifo_Din    <= s_Data_f(61);
            elsif (s_valid_f(62) = '1') then
               s_valid_f(62) <= '0'; 
               o_fifo_Din    <= s_Data_f(62);
            elsif (s_valid_f(63) = '1') then
               s_valid_f(63) <= '0'; 
               o_fifo_Din    <= s_Data_f(63);
            else
               o_fifo_wr_rq  <= '0';    
            end if;
         end case;
            
         
         if (o_fifo_wr_rq = '1') then
            s_data_cntr <= s_data_cntr + 1;
         end if;      

         if (i_kmers_finished = '1' and s_busy_all = '0' and s_data_read_all = '0') then
            o_fifo_wr_rq <= '1';
            case s_priority is
            when 0 =>
               s_priority <= 1;
               if (s_valid_m(0) = '1') then
                  s_valid_m(0)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(0);
               elsif (s_valid_m(1) = '1') then
                  s_valid_m(1)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(1);
               elsif (s_valid_m(2) = '1') then
                  s_valid_m(2)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(2);
               elsif (s_valid_m(3) = '1') then
                  s_valid_m(3)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(3); 
               elsif (s_valid_m(4) = '1') then
                  s_valid_m(4)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(4);  
               elsif (s_valid_m(5) = '1') then
                  s_valid_m(5)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(5);
               elsif (s_valid_m(6) = '1') then
                  s_valid_m(6)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(6); 
               elsif (s_valid_m(7) = '1') then
                  s_valid_m(7)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(7);
               elsif (s_valid_m(8) = '1') then
                  s_valid_m(8)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(8);
               elsif (s_valid_m(9) = '1') then
                  s_valid_m(9)  <= '0'; 
                  o_fifo_Din    <= s_Data_f(9);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            when 1 =>
               s_priority <= 2;
               if (s_valid_m(10) = '1') then
                  s_valid_m(10) <= '0'; 
                  o_fifo_Din    <= s_Data_f(10); 
               elsif (s_valid_m(11) = '1') then
                  s_valid_m(11) <= '0'; 
                  o_fifo_Din    <= s_Data_f(11); 
               elsif (s_valid_m(12) = '1') then
                  s_valid_m(12) <= '0'; 
                  o_fifo_Din    <= s_Data_f(12); 
               elsif (s_valid_m(13) = '1') then
                  s_valid_m(13) <= '0'; 
                  o_fifo_Din    <= s_Data_f(13);
               elsif (s_valid_m(14) = '1') then
                  s_valid_m(14) <= '0'; 
                  o_fifo_Din    <= s_Data_f(14);
               elsif (s_valid_m(15) = '1') then
                  s_valid_m(15) <= '0'; 
                  o_fifo_Din    <= s_Data_f(15);
               elsif (s_valid_m(16) = '1') then
                  s_valid_m(16) <= '0'; 
                  o_fifo_Din    <= s_Data_f(16);
               elsif (s_valid_m(17) = '1') then
                  s_valid_m(17) <= '0'; 
                  o_fifo_Din    <= s_Data_f(17);
               elsif (s_valid_m(18) = '1') then
                  s_valid_m(18) <= '0'; 
                  o_fifo_Din    <= s_Data_f(18);
               elsif (s_valid_m(19) = '1') then
                  s_valid_m(19) <= '0'; 
                  o_fifo_Din    <= s_Data_f(19);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            when 2 =>
               s_priority <= 3;
               if (s_valid_m(20) = '1') then
                  s_valid_m(20) <= '0'; 
                  o_fifo_Din    <= s_Data_f(20);
               elsif (s_valid_m(21) = '1') then
                  s_valid_m(21) <= '0'; 
                  o_fifo_Din    <= s_Data_f(21);
               elsif (s_valid_m(22) = '1') then
                  s_valid_m(22) <= '0'; 
                  o_fifo_Din    <= s_Data_f(22);
               elsif (s_valid_m(23) = '1') then
                  s_valid_m(23) <= '0'; 
                  o_fifo_Din    <= s_Data_f(23);
               elsif (s_valid_m(24) = '1') then
                  s_valid_m(24) <= '0'; 
                  o_fifo_Din    <= s_Data_f(24);
               elsif (s_valid_m(25) = '1') then
                  s_valid_m(25) <= '0'; 
                  o_fifo_Din    <= s_Data_f(25);
               elsif (s_valid_m(26) = '1') then
                  s_valid_m(26) <= '0'; 
                  o_fifo_Din    <= s_Data_f(26);
               elsif (s_valid_m(27) = '1') then
                  s_valid_m(27) <= '0'; 
                  o_fifo_Din    <= s_Data_f(27);
               elsif (s_valid_m(28) = '1') then
                  s_valid_m(28) <= '0'; 
                  o_fifo_Din    <= s_Data_f(28);
               elsif (s_valid_m(29) = '1') then
                  s_valid_m(29) <= '0'; 
                  o_fifo_Din    <= s_Data_f(29);
               elsif (s_valid_m(30) = '1') then
                  s_valid_m(30) <= '0'; 
                  o_fifo_Din    <= s_Data_f(30);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            when 3 =>
               s_priority <= 4;
               if (s_valid_m(31) = '1') then
                  s_valid_m(31) <= '0'; 
                  o_fifo_Din    <= s_Data_f(31);
               elsif (s_valid_m(32) = '1') then
                  s_valid_m(32) <= '0'; 
                  o_fifo_Din    <= s_Data_f(32);
               elsif (s_valid_m(33) = '1') then
                  s_valid_m(33) <= '0'; 
                  o_fifo_Din    <= s_Data_f(33);
               elsif (s_valid_m(34) = '1') then
                  s_valid_m(34) <= '0'; 
                  o_fifo_Din    <= s_Data_f(34);
               elsif (s_valid_m(35) = '1') then
                  s_valid_m(35) <= '0'; 
                  o_fifo_Din    <= s_Data_f(35);
               elsif (s_valid_m(36) = '1') then
                  s_valid_m(36) <= '0'; 
                  o_fifo_Din    <= s_Data_f(36);
               elsif (s_valid_m(37) = '1') then
                  s_valid_m(37) <= '0'; 
                  o_fifo_Din    <= s_Data_f(37);
               elsif (s_valid_m(38) = '1') then
                  s_valid_m(38) <= '0'; 
                  o_fifo_Din    <= s_Data_f(38);
               elsif (s_valid_m(39) = '1') then
                  s_valid_m(39) <= '0'; 
                  o_fifo_Din    <= s_Data_f(39);
               elsif (s_valid_m(40) = '1') then
                  s_valid_m(40) <= '0'; 
                  o_fifo_Din    <= s_Data_f(40);
               elsif (s_valid_m(41) = '1') then
                  s_valid_m(41) <= '0'; 
                  o_fifo_Din    <= s_Data_f(41);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            when 4 =>
               s_priority <= 5;
               if (s_valid_m(42) = '1') then
                  s_valid_m(42) <= '0'; 
                  o_fifo_Din    <= s_Data_f(42);
               elsif (s_valid_m(43) = '1') then
                  s_valid_m(43) <= '0'; 
                  o_fifo_Din    <= s_Data_f(43);
               elsif (s_valid_m(44) = '1') then
                  s_valid_m(44) <= '0'; 
                  o_fifo_Din    <= s_Data_f(44);
               elsif (s_valid_m(45) = '1') then
                  s_valid_m(45) <= '0'; 
                  o_fifo_Din    <= s_Data_f(45);
               elsif (s_valid_m(46) = '1') then
                  s_valid_m(46) <= '0'; 
                  o_fifo_Din    <= s_Data_f(46);
               elsif (s_valid_m(47) = '1') then
                  s_valid_m(47) <= '0'; 
                  o_fifo_Din    <= s_Data_f(47);
               elsif (s_valid_m(48) = '1') then
                  s_valid_m(48) <= '0'; 
                  o_fifo_Din    <= s_Data_f(48);
               elsif (s_valid_m(49) = '1') then
                  s_valid_m(49) <= '0'; 
                  o_fifo_Din    <= s_Data_f(49);
               elsif (s_valid_m(50) = '1') then
                  s_valid_m(50) <= '0'; 
                  o_fifo_Din    <= s_Data_f(50);
               elsif (s_valid_m(51) = '1') then
                  s_valid_m(51) <= '0'; 
                  o_fifo_Din    <= s_Data_f(51);
               elsif (s_valid_m(52) = '1') then
                  s_valid_m(52) <= '0'; 
                  o_fifo_Din    <= s_Data_f(52);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            when others => --5 =>
               s_priority <= 0;
               if (s_valid_m(53) = '1') then
                  s_valid_m(53) <= '0'; 
                  o_fifo_Din    <= s_Data_f(53);
               elsif (s_valid_m(54) = '1') then
                  s_valid_m(54) <= '0'; 
                  o_fifo_Din    <= s_Data_f(54);
               elsif (s_valid_m(55) = '1') then
                  s_valid_m(55) <= '0'; 
                  o_fifo_Din    <= s_Data_f(55);
               elsif (s_valid_m(56) = '1') then
                  s_valid_m(56) <= '0'; 
                  o_fifo_Din    <= s_Data_f(56);
               elsif (s_valid_m(57) = '1') then
                  s_valid_m(57) <= '0'; 
                  o_fifo_Din    <= s_Data_f(57);
               elsif (s_valid_m(58) = '1') then
                  s_valid_m(58) <= '0'; 
                  o_fifo_Din    <= s_Data_f(58);
               elsif (s_valid_m(59) = '1') then
                  s_valid_m(59) <= '0'; 
                  o_fifo_Din    <= s_Data_f(59);
               elsif (s_valid_m(60) = '1') then
                  s_valid_m(60) <= '0'; 
                  o_fifo_Din    <= s_Data_f(60);
               elsif (s_valid_m(61) = '1') then
                  s_valid_m(61) <= '0'; 
                  o_fifo_Din    <= s_Data_f(61);
               elsif (s_valid_m(62) = '1') then
                  s_valid_m(62) <= '0'; 
                  o_fifo_Din    <= s_Data_f(62);
               elsif (s_valid_m(63) = '1') then
                  s_valid_m(63) <= '0'; 
                  o_fifo_Din    <= s_Data_f(63);
               else
                  o_fifo_wr_rq  <= '0';    
               end if;
            end case;          

         end if;
                      
         case s_cntr is
         when 0 =>
            if (i_kmers_finished = '1') then
               s_cntr   <= 1;
            end if;
         when 1 =>
            if (s_busy_all = '0') then
               s_cntr   <= 2;
            end if;  
         when 2 =>
            if (s_data_read_all = '0') then
               s_cntr   <= 3;
            end if;   
         when 3 =>
            if (s_data_mid_all = '0') then
               s_cntr   <= 4;
            end if;   
         when 4 =>
            s_cntr   <= 5;
         when 5 =>
            s_cntr   <= 6;
         when 6 =>
            s_cntr   <= 7;
         when others => --7
            if (s_busy_all = '0' and s_data_read_all = '0' and s_data_mid_all = '0') then
               o_finished         <= '1';
               o_return_data_size <= s_data_cntr;
            else
               s_cntr   <=  0;
            end if;
         end case;               
         
               
      end if;
   end process; 
        
   s_busy_all        <= OR_BITS(s_busy);      
   s_data_read_all   <= OR_BITS(s_valid_f);      
   s_data_mid_all    <= OR_BITS(s_valid_m);      
         
end Controller_v2;         